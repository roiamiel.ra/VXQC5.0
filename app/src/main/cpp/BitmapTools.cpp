#include <jni.h>

int argbToRGB(int ARGB);
unsigned int alpha(int color);
int compositeAlpha(int foregroundAlpha, int backgroundAlpha);
int compositeComponent(int fgC, int fgA, int bgC, int bgA, int a);
int red(int color);
int green(int color);
int blue(int color);
int argb(int alpha, int red, int green, int blue);

extern "C"
JNIEXPORT jintArray JNICALL
Java_vx_1qc_visionix_vxqc50_Tools_FunnelPDF_convertBitmapTo(JNIEnv *env, jobject instance,
                                                            jintArray PixelsArray_,
                                                            jint ArraySize) {
    jintArray newArray = env->NewIntArray(ArraySize);

    jint* oArray = env->GetIntArrayElements(PixelsArray_, NULL);
    jint* nArray = env->GetIntArrayElements(newArray, NULL);

    for (int i = 0; i < ArraySize; i++) {
        nArray[i] = argbToRGB(oArray[i]);
    }

    env->ReleaseIntArrayElements(newArray, nArray, NULL);
    env->ReleaseIntArrayElements(PixelsArray_, oArray, NULL);

    return newArray;

}

int argbToRGB(int ARGB)
{
    int bgAlpha = alpha(0xFFFFFFFF);
    int fgAlpha = alpha(ARGB);
    int a = compositeAlpha(fgAlpha, bgAlpha);

    int r = compositeComponent(red(ARGB), fgAlpha,
                               red(0xFFFFFFFF), bgAlpha, a);
    int g = compositeComponent(green(ARGB), fgAlpha,
                               green(0xFFFFFFFF), bgAlpha, a);
    int b = compositeComponent(blue(ARGB), fgAlpha,
                               blue(0xFFFFFFFF), bgAlpha, a);

    return argb(a, r, g, b);
}

unsigned int alpha(int color)
{
    return (unsigned int) color >> 24;
}

int compositeAlpha(int foregroundAlpha, int backgroundAlpha)
{
    return 0xFF - (((0xFF - backgroundAlpha) * (0xFF - foregroundAlpha)) / 0xFF);
}

int compositeComponent(int fgC, int fgA, int bgC, int bgA, int a)
{
    if (a == 0) return 0;
    return ((0xFF * fgC * fgA) + (bgC * bgA * (0xFF - fgA))) / (a * 0xFF);
}

int red(int color)
{
    return (color >> 16) & 0xFF;
}

int green(int color)
{
    return (color >> 8) & 0xFF;
}

int blue(int color)
{
    return color & 0xFF;
}

int argb(int alpha, int red, int green, int blue)
{
    return (alpha << 24) | (red << 16) | (green << 8) | blue;
}

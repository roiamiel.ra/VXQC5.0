package com.visionix.vxqc.KAKA.Activitysa.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

public class AlertButtonsDialog extends Dialog {

    private String mTitle;
    private String mSubTitle;
    private String mPossitiveButtonText;
    private String mNegativeButonText;

    private View.OnClickListener mPossitiveButtonListener;
    private OnClickListener mOnNegativeClick;

    public AlertButtonsDialog(Context context, String Title, String SubTitle, String PossitiveButtonText, View.OnClickListener PossitiveButtonListener) {
        super(context);

        mTitle = Title;
        mSubTitle = SubTitle;
        mPossitiveButtonText = PossitiveButtonText;
        mNegativeButonText = null;

        mPossitiveButtonListener = PossitiveButtonListener;

        mOnNegativeClick = null;

        return;
    }

    public AlertButtonsDialog(Context context, String Title, String SubTitle, String PossitiveButtonText, String NegativeButtonText, View.OnClickListener PossitiveButtonListener) {
        super(context);

        mTitle = Title;
        mSubTitle = SubTitle;
        mPossitiveButtonText = PossitiveButtonText;
        mNegativeButonText = NegativeButtonText;

        mPossitiveButtonListener = PossitiveButtonListener;

        mOnNegativeClick = null;

        return;
    }

    public AlertButtonsDialog(Context context, String Title, String SubTitle, String PossitiveButtonText, String NegativeButtonText, View.OnClickListener PossitiveButtonListener, OnClickListener OnNegativeClick) {
        super(context);

        mTitle = Title;
        mSubTitle = SubTitle;
        mPossitiveButtonText = PossitiveButtonText;
        mNegativeButonText = NegativeButtonText;

        mPossitiveButtonListener = PossitiveButtonListener;
        mOnNegativeClick = OnNegativeClick;

        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.alertbuttonsdialog);

        TextView titleTextView = findViewById(R.id.Title);
        TextView subTitleTextView = findViewById(R.id.SubTitle);

        Button positiveButton = findViewById(R.id.PossitiveButton);
        Button negativeButton = findViewById(R.id.NegativeButton);

        titleTextView.setText(mTitle);
        subTitleTextView.setText(mSubTitle);

        positiveButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);
        positiveButton.setText(mPossitiveButtonText);
        positiveButton.setOnClickListener(mPossitiveButtonListener);

        if(mNegativeButonText != null) {
            negativeButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);
            negativeButton.setText(mNegativeButonText);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnNegativeClick != null) {
                        mOnNegativeClick.onClick(AlertButtonsDialog.this, 0);
                    } else {
                        cancel();
                    }
                }
            });
        } else {
            negativeButton.setVisibility(View.GONE);
        }

        return;
    }
}

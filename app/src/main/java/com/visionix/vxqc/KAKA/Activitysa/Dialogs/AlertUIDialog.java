package com.visionix.vxqc.KAKA.Activitysa.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

import java.util.ArrayList;

public class AlertUIDialog extends Dialog {

    private Context mContext;

    private TextView mTitleTextView;
    private TextView mSubTitleTextView;
    private Button mOKButton;
    private ListView mListView;

    private ArrayList<String> mItemsList;

    private String mTitle, mSubTitle;

    public AlertUIDialog(Context context, String Title, String SubTitle) {
        super(context);
        mContext = context;

        mTitle = Title;
        mSubTitle = SubTitle;

        mItemsList = null;

        return;
    }

    public AlertUIDialog(Context context, String Title, String SubTitle, ArrayList<String> ItemsList) {
        super(context);
        mContext = context;

        mTitle = Title;
        mSubTitle = SubTitle;

        mItemsList = ItemsList;

        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.alertdialog);

        mTitleTextView = findViewById(R.id.title);
        mSubTitleTextView = findViewById(R.id.subtitle);
        mOKButton = findViewById(R.id.okbutton);
        mListView = findViewById(R.id.ListView);

        mTitleTextView.setText(mTitle);
        mSubTitleTextView.setText(mSubTitle);

        mOKButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        mOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });

        if(mItemsList != null){
            mListView.setVisibility(View.VISIBLE);
            mListView.setAdapter(new ArrayAdapter<String>(mContext, R.layout.basiclistobject, mItemsList));
            mListView.setClickable(false);
            mListView.setEnabled(false);

        } else {
            mListView.setVisibility(View.GONE);
        }

        return;
    }
}

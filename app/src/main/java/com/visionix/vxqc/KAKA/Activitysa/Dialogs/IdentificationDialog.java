package com.visionix.vxqc.KAKA.Activitysa.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.visionix.vxqc.server.objects.DocumentSource;
import com.visionix.vxqc.server.objects.Identification;
import com.visionix.vxqc.gui.CirculatImageView.CircularImageView;
import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

import java.util.ArrayList;
import java.util.HashMap;

public class IdentificationDialog extends Dialog {

    private static boolean mAlreadyLaunched = false;

    private TextView mTextView;
    private GridLayout mGridLayou;
    private Button mOKButton;
    private TextView mParamsTextView;
    private ProgressBar mProgressBar;

    private CircularImageView mCircularImageView;

    private DocumentSource mDocumentSource;
    private ReturnResualt mReturnResualt;

    private int mID = -1;

    private Context mContext;

    private boolean mCreateNewDocument;

    private boolean mEditable;

    public IdentificationDialog(Context context, DocumentSource DocumentSource, boolean CreateNewDocument, boolean Editable, ReturnResualt ReturnResualt) {
        super(context);

        mEditable = Editable;

        mContext =  context;

        mCreateNewDocument = CreateNewDocument;

        mReturnResualt = ReturnResualt;
        mDocumentSource = DocumentSource;

        return;
    }

    public IdentificationDialog(Context context, DocumentSource DocumentSource, boolean CreateNewDocument, int ID, boolean Editable,  ReturnResualt ReturnResualt) {
        super(context);

        mEditable = Editable;

        mID = ID;
        mContext =  context;

        mCreateNewDocument = CreateNewDocument;

        mReturnResualt = ReturnResualt;
        mDocumentSource = DocumentSource;

        return;
    }

    @Override
    public void show() {
        if(!mAlreadyLaunched) {
            super.show();
            mAlreadyLaunched = true;
        }
    }

    @Override
    public void cancel() {
        super.cancel();
        mAlreadyLaunched = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAlreadyLaunched = false;
    }

    private ArrayList<IdentificationGrid> mIdentificationGridArrayList = new ArrayList<IdentificationGrid>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if(mCreateNewDocument){
            setContentView(LayoutInflater.from(mContext).inflate(R.layout.identificationnew_dialog, null, false));
        } else {
            setContentView(LayoutInflater.from(mContext).inflate(R.layout.identification_dialog, null, false));
        }

        Install();

        Identification[] identifications = mDocumentSource.getIdentificationsList();
        for(int i = 0; i < identifications.length; i++){
            IdentificationGrid identificationGrid = new IdentificationGrid();

            Identification identification = identifications[i];

            identificationGrid.mName = identification.getName();

            TextView textView = new TextView(mContext);
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams(GridLayout.spec(i), GridLayout.spec(0));
            layoutParams.setGravity(Gravity.LEFT);
            layoutParams.rightMargin = 15;
            textView.setLayoutParams(layoutParams);
            textView.setTextSize(18);
            textView.setText(identification.getName());
            mGridLayou.addView(textView);

            identificationGrid.mEditText = new EditText(mContext);
            layoutParams = new GridLayout.LayoutParams(GridLayout.spec(i), GridLayout.spec(1));
            layoutParams.rightMargin = 15;
            identificationGrid.mEditText.setLayoutParams(layoutParams);
            identificationGrid.mEditText.setEms(10);
            identificationGrid.mEditText.setGravity(Gravity.LEFT);
            identificationGrid.mEditText.setMaxLines(1);
            identificationGrid.mEditText.setMaxWidth(10);
            identificationGrid.mEditText.setSingleLine(true);
            identificationGrid.mEditText.setHint(identification.getName());
            identificationGrid.mEditText.setText(identification.getValue() != null ? identification.getValue() : "");

            identificationGrid.mEditText.setEnabled(mEditable);

            mGridLayou.addView(identificationGrid.mEditText);

            mIdentificationGridArrayList.add(identificationGrid);
        }

        mOKButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        mOKButton.setOnClickListener(v -> {

            if(mEditable) {
                new Handler().post(() -> {
                    for (int i = 0; i < mIdentificationGridArrayList.size(); i++) {
                        IdentificationGrid identificationGrid = mIdentificationGridArrayList.get(i);
                        identificationGrid.mEditText.setEnabled(false);
                    }
                });

                for (int i = 0; i < mIdentificationGridArrayList.size(); i++) {
                    IdentificationGrid identificationGrid = mIdentificationGridArrayList.get(i);

                    Identification identification = identifications[i];

                    identification.setValue(identificationGrid.mValue = identificationGrid.mEditText.getText().toString());
                    identificationGrid.mUnique = identification.isUnique();
                }

                new CheckForDuplicates(mIdentificationGridArrayList, mDocumentSource, mID).execute();

            } else {
                cancel();

            }
        });

        return;
    }

    private void Install(){
        mTextView = findViewById(R.id.Title);
        mGridLayou = findViewById(R.id.params_layout);
        mOKButton = findViewById(R.id.ok_button);
        mProgressBar = findViewById(R.id.ok_progressbar);

        if(mCreateNewDocument){
            mCircularImageView = findViewById(R.id.CircularImageView);
            mCircularImageView.setImageBitmap(mDocumentSource.getImage());

            mTextView.setText(mDocumentSource.getProduct() + " " + mDocumentSource.getType());

            mOKButton.setText("צור/י");

        } else {
            mParamsTextView = findViewById(R.id.ParamsTextView);

            mTextView.setText("הגדר מסמך "  + mDocumentSource.getProduct() + " " + mDocumentSource.getType());
            mOKButton.setText("סיימתי");

            mParamsTextView.setText(mDocumentSource.getType() + " " + mDocumentSource.getProduct() + " | P.N: " + mDocumentSource.getPartNumber() + " | Rev: " + mDocumentSource.getRevision());

        }

        mOKButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        mProgressBar.getIndeterminateDrawable().setColorFilter(UiUtiles.getAppTheme().mColorAccent, android.graphics.PorterDuff.Mode.SRC_IN);
        mProgressBar.setVisibility(View.GONE);

        return;
    }

    public interface ReturnResualt{
        void onReturn();
    }

    class CheckForDuplicates extends AsyncTask<Void, Void, Void> {
        private DocumentSource mDocumentSource;
        private ArrayList<IdentificationGrid> mIdentificationGridArrayList;
        private HashMap<String, Boolean> mDuplicatesList;
        private int mID;

        public CheckForDuplicates(ArrayList<IdentificationGrid> IdentificationGridArrayList, DocumentSource DocumentSource, int ID){
            mIdentificationGridArrayList = IdentificationGridArrayList;
            mDocumentSource = DocumentSource;
            mID = ID;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(mProgressBar != null){
                mProgressBar.setVisibility(View.VISIBLE);
            }
            if(mOKButton != null){
                mOKButton.setVisibility(View.GONE);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            //mDuplicatesList = DuplicatesIdentifications.SearchForDuplicatesIdentifications(mIdentificationGridArrayList, mDocumentSource, mID);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < mIdentificationGridArrayList.size(); i++) {
                        IdentificationGrid identificationGrid = mIdentificationGridArrayList.get(i);
                        identificationGrid.mEditText.setEnabled(true);
                    }
                }
            });

            if(mProgressBar != null){
                mProgressBar.setVisibility(View.GONE);
            }
            if(mOKButton != null){
                mOKButton.setVisibility(View.VISIBLE);
            }

            boolean requestFocus = false;
            int Size = mDuplicatesList.size();

            if(Size == 0){
                IdentificationDialog.this.cancel();
                mReturnResualt.onReturn();
                return;
            }

            for (int i = 0; i < mIdentificationGridArrayList.size(); i++) {
                IdentificationGrid identificationGrid = mIdentificationGridArrayList.get(i);

                if(identificationGrid.mUnique) {
                    if(mDuplicatesList.get(identificationGrid.mName) != null){
                        identificationGrid.mEditText.setError("ערך זהה קיים כבר במסמך אחר מסוג זה");

                        if(!requestFocus) {
                            identificationGrid.mEditText.requestFocus();
                            requestFocus = true;
                        }
                    } else {
                        identificationGrid.mEditText.setError(null);

                    }
                }
            }
        }
    }

    public class IdentificationGrid {
        public EditText mEditText;
        public String mName;
        public String mValue;
        public boolean mUnique;
    }
}

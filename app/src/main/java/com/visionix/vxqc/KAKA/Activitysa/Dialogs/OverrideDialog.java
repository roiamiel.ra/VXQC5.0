package com.visionix.vxqc.KAKA.Activitysa.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.visionix.vxqc.server.objects.User;
import com.visionix.vxqc.KAKA.Parser.Objects.Error;
import com.visionix.vxqc.KAKA.Parser.Objects.Object;
import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

import java.util.ArrayList;

public class OverrideDialog extends Dialog implements TextWatcher {

    private Context mContext;

    private ArrayList<Error> mErrorsList;
    private OnReturn mOnReturn;

    private LinearLayout mShowErrorsLayout;
    private LinearLayout mOverrideLayout;

    private Button mOverride;
    private AutoCompleteTextView mUserName;
    private EditText mPassword;

    private ListView mErrorsListView;
    private Button mCloseButton;
    private Button mSwitchToOverrideButton;

    private boolean mJustOverride = false;
    private boolean mInOverrideMode = false;

    private TextView mTitleTextView;
    private TextView mSubTitleTextView;

    private String mTitle;
    private String mSubTitle;

    public OverrideDialog(Context Context, ArrayList<Error> ErrorsList, OnReturn OnReturn) {
        super(Context);

        mContext = Context;

        mErrorsList = ErrorsList;
        mOnReturn = OnReturn;

        mJustOverride = false;

        return;
    }

    public OverrideDialog(Context Context, String Title, String SubTitle, OnReturn OnReturn) {
        super(Context);

        mContext = Context;

        mOnReturn = OnReturn;

        mTitle = Title;
        mSubTitle = SubTitle;

        mJustOverride = true;

        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.overridedialog);

        mShowErrorsLayout = findViewById(R.id.ErrorsListLinearLayout);
        mOverrideLayout = findViewById(R.id.ManagerOverrideRelativeLayout);

        mTitleTextView = findViewById(R.id.Title);
        mSubTitleTextView = findViewById(R.id.SubTitle1);

        mOverride = findViewById(R.id.Override);
        mUserName = findViewById(R.id.UserNameAutoComplete);
        mPassword = findViewById(R.id.Password);

        mErrorsListView = findViewById(R.id.ErrorsListView);
        mCloseButton = findViewById(R.id.Close);
        mSwitchToOverrideButton = findViewById(R.id.SwitchToOverride);

        mShowErrorsLayout.setVisibility(View.VISIBLE);
        mOverrideLayout.setVisibility(View.GONE);

        mUserName.addTextChangedListener(this);

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                mOnReturn.OnReturn(false, null);
            }
        });

        mOverride.setTextColor(UiUtiles.getAppTheme().mColorAccent);
        mCloseButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);
        mSwitchToOverrideButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        mUserName.setThreshold(1);
        mUserName.setAdapter(new ArrayAdapter<>(mContext, R.layout.support_simple_spinner_dropdown_item, User.getUsersNames()));


        if(mJustOverride){
            SwitchToOverrideMode();
        } else {
            mSwitchToOverrideButton.setOnClickListener(v -> {
                if (!User.getConnectUser().isManagerPermission()) {
                    SwitchToOverrideMode();
                } else {
                    OverrideSuccess();
                }

            });

            mErrorsListView.setAdapter(new ErrorsAdapter(mErrorsList, mOnReturn));

        }
        return;
    }

    @Override
    public void onBackPressed() {
        if(mInOverrideMode && !mJustOverride){
            SwitchToListMode();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void cancel() {
        if(mInOverrideMode && !mJustOverride){
            SwitchToListMode();
        } else {
            super.cancel();
            mOnReturn.OnReturn(false, null);
        }
    }

    private void SwitchToOverrideMode(){
        mOverrideLayout.setVisibility(View.VISIBLE);
        mShowErrorsLayout.setVisibility(View.GONE);

        if(mJustOverride){
            mTitleTextView.setText(mTitle);
            mSubTitleTextView.setText(mSubTitle);
        }

        mOverride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserName.setError(null);

                String userName = mUserName.getText().toString();
                String password = mPassword.getText().toString();

                if(userName.equals("")){
                    mUserName.setError("אתה חייב להזין שם משתמש");
                    return;
                }

                User user = User.getUser(userName, password);
                if(user == null){
                    mUserName.setError("שם המשתמש או הסיסמא שגויים");
                    return;
                }

                if(!user.isManagerPermission()){
                    mUserName.setError("למשתמש זה אין הרשאת מנהל");
                    return;
                }

                OverrideSuccess();
            }
        });

        mInOverrideMode = true;
    }

    private void OverrideSuccess(){
        cancel();
        mOnReturn.OnReturn(true, null);

        return;
    }

    private void SwitchToListMode(){
        mShowErrorsLayout.setVisibility(View.VISIBLE);
        mOverrideLayout.setVisibility(View.GONE);

        mOverride.setOnClickListener(null);

        mInOverrideMode = false;
        return;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(User.isUserHasPassword(mUserName.getText().toString())){
            mPassword.setText("");
            mPassword.setVisibility(View.VISIBLE);

        } else {
            mPassword.setText("");
            mPassword.setVisibility(View.GONE);

        }

    }

    class ErrorsAdapter extends BaseAdapter {

        private ArrayList<Error> mErrordList;
        private OnReturn mOnReturn;

        public ErrorsAdapter(ArrayList<Error> ErrordList, OnReturn OnReturn) {
            mErrordList = ErrordList;
            mOnReturn = OnReturn;

        }

        @Override
        public int getCount() {
            return mErrordList.size();
        }

        @Override
        public java.lang.Object getItem(int i) {
            return mErrordList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View layout = getLayoutInflater().inflate(R.layout.erroritem, viewGroup, false);

            Error error = mErrordList.get(i);

            TextView textView = layout.findViewById(R.id.Text);
            Button button = layout.findViewById(R.id.Button);

            textView.setText("השדה " + error.mObject.mObejctName + " " + error.mMassage);

            final Object ThisObject = error.mObject;
            button.setOnClickListener(view1 -> {
                if(isShowing()){
                    cancel();
                    mOnReturn.OnReturn(false, ThisObject);
                }
            });

            return layout;
        }
    }

    public interface OnReturn {
        void OnReturn(boolean OverrideSuccess, Object ScrollTo);
    }

}

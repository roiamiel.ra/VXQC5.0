package com.visionix.vxqc.KAKA.Activitysa.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.visionix.vxqc.server.objects.Document;
import com.visionix.vxqc.gui.Signatrue.views.SignaturePad;
import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

public class SignatureDialog extends Dialog {

    private Document mDocument;
    private OnReturn mOnReturn;

    private Context mContext;

    public SignatureDialog(Context context, Document Document, OnReturn OnReturn) {
        super(context);

        mContext = context;

        mDocument = Document;
        mOnReturn = OnReturn;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signatruedialog);

        setCancelable(false);

        Button okButton = findViewById(R.id.OKButton);
        Button clearButton = findViewById(R.id.ClearButton);
        final SignaturePad signaturePad = findViewById(R.id.SignaturePad);

        if(mDocument.getSignature() != null){
            signaturePad.setSignatureBitmap(mDocument.getSignature());
        }

        okButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);
        clearButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(signaturePad.isEmpty()){
                    Toast.makeText(getContext(), "את/ה חייב/ת לחתום כדי להמשיך.", Toast.LENGTH_SHORT).show();
                } else {
                    cancel();
                    mDocument.setSignature(signaturePad.getSignatureBitmap());
                    mOnReturn.OnReturn();
                }
            }
        });
    }

    public interface OnReturn {
        void OnReturn();
    }
}

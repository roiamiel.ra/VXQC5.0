package com.visionix.vxqc.KAKA.Activitysa;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.visionix.vxqc.server.objects.Document;
import com.visionix.vxqc.gui.CustomScrollView.CustomScrollView;
import com.visionix.vxqc.gui.FAB.FloatingActionButton;
import com.visionix.vxqc.gui.FAB.FloatingActionsMenu;
import com.visionix.vxqc.KAKA.JSONDocumentParser.JSONDocumentBuilder;
import com.visionix.vxqc.KAKA.Parser.Objects.DocumentStructure;
import com.visionix.vxqc.KAKA.Tools.FunnelPDF;

public class DocumentEditorActivity extends AppCompatActivity /*implements OnClickListener, Runnable*/ {

    //Finals
    private static final int AUTO_SAVE_TIME = 20000; //20sec

    //Runtime params
    private static Document mDocumentToEdit = null;

    private static boolean mIsCanEdit = false;
    private static boolean mIsNewDocument = false;

    //Flags
    private boolean mIsDocumentDone = false;
    private int     mPageHight = -1;

    //Views
    private FloatingActionsMenu mMenuFAM = null;
    private FloatingActionButton mDoneFAB = null;
    private FloatingActionButton mSaveFAB = null;
    private FloatingActionButton mAddFailureFAB = null;
    private FloatingActionButton mFailuresFAB = null;
    private FloatingActionButton mOptionsFAB = null;

    private LinearLayout mDocumentEditorLayout = null;
    private CustomScrollView mScrollViewLayout = null;

    //Class
    private FunnelPDF mFunnelPDF = null;
    private JSONDocumentBuilder mJSONDocumentParser = null;
    private DocumentStructure mDocumentStructure = null;

    //Tools
    private Thread mAutoSaveThread = null;

    /*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.documenteditor);

        //Set animation
        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        //Install class and params
        this.mJSONDocumentParser = new JSONDocumentBuilder(DocumentEditorActivity.this);
        this.mIsDocumentDone = false;

        //Find Views by id
        this.mDocumentEditorLayout = findViewById(R.id.DocumentEditorLayout);
        this.mScrollViewLayout = findViewById(R.id.ScrollViewLayout);

        this.mMenuFAM = findViewById(R.id.MenuFAM);
        this.mDoneFAB = findViewById(R.id.DoneFAB);
        this.mSaveFAB = findViewById(R.id.SaveFAB);
        this.mAddFailureFAB = findViewById(R.id.AddFailureFAB);
        this.mFailuresFAB = findViewById(R.id.FailuresFAB);
        this.mOptionsFAB = findViewById(R.id.OptionsFAB);

        //Set colors
        int pressedColorFABs = UiUtiles.colorLighter(
                UiUtiles.getAppTheme().mPrimaryColor,
                0.25f
        );

        UiUtiles.setLayoutMaterialColor(this, null);

        this.mDoneFAB.setColorNormal(UiUtiles.getAppTheme().mPrimaryColor);
        this.mSaveFAB.setColorNormal(UiUtiles.getAppTheme().mPrimaryColor);
        this.mAddFailureFAB.setColorNormal(UiUtiles.getAppTheme().mPrimaryColor);
        this.mFailuresFAB.setColorNormal(UiUtiles.getAppTheme().mPrimaryColor);
        this.mOptionsFAB.setColorNormal(UiUtiles.getAppTheme().mPrimaryColor);

        this.mDoneFAB.setColorPressed(pressedColorFABs);
        this.mSaveFAB.setColorPressed(pressedColorFABs);
        this.mAddFailureFAB.setColorPressed(pressedColorFABs);
        this.mFailuresFAB.setColorPressed(pressedColorFABs);
        this.mOptionsFAB.setColorPressed(pressedColorFABs);

        this.mDocumentEditorLayout.setBackgroundColor(UiUtiles.getAppTheme().mText);

        //Set Listeners
        this.mDoneFAB.setOnClickListener(this);
        this.mSaveFAB.setOnClickListener(this);
        this.mAddFailureFAB.setOnClickListener(this);
        this.mFailuresFAB.setOnClickListener(this);
        this.mOptionsFAB.setOnClickListener(this);

        this.mMenuFAM.setOnClickListener(
                view -> this.mMenuFAM.setCollapse(this.mMenuFAM.isExpanded())
        );

        //Other params
        if (mIsCanEdit) {
            mDoneFAB.setEnabled(true);
            mSaveFAB.setEnabled(true);
            mAddFailureFAB.setEnabled(true);

        } else {
            mDoneFAB.setVisibility(View.GONE);
            mSaveFAB.setVisibility(View.GONE);
            mAddFailureFAB.setVisibility(View.GONE);
        }

        //loadURLs the document to the screen
        new LoadDocument().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Delete the document reference
        mDocumentToEdit = null;
    }

    @Override
    public void finish() {
        //Stop the autosave
        stopAutoSave();

        //Finish and add animation
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }


     * Function to calculate the page high
     * (16 * 2) -> the padding (top and bottom)
     *

    private void LoadPageHight() {
        this.mPageHight = this.mScrollViewLayout.getHeight() - 16 * 2;
    }


     * Class to load all the document params and content to the screen
     *

    private class LoadDocument extends AsyncTask<Void, Void, Void> {

        //Views
        private ProgressDialog mProgressDialog = null;

        //Object
        private LinearLayout mDocumentContentLinearLayout = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Set the progress dialog
            this.mProgressDialog = new ProgressDialog(DocumentEditorActivity.this);
            this.mProgressDialog.setMessage("בונה את המסמך");
            this.mProgressDialog.setCancelable(false);
            this.mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //Check if the document content loaded
            if(!mDocumentToEdit.isDocumentContentLoaded()) {
                //loadURLs it
                mDocumentToEdit.loadDocumentContent();
            }

            //Create the document structure via the parser
            mDocumentStructure =
                    mJSONDocumentParser.buildDocumentStructure(
                            mDocumentToEdit.getDocumentContent(),
                            mDocumentToEdit.getProduct()
                    );

            //Install the document linear layout to the content
            this.mDocumentContentLinearLayout = new LinearLayout(DocumentEditorActivity.this);

            //Put the document structure data into the layout
            mJSONDocumentParser.setLinesToLayout(
                    mDocumentStructure,
                    new LayoutView<>(
                            this.mDocumentContentLinearLayout
                    ),
                    mIsCanEdit
            );

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //Add the document layout to the main layout
            mDocumentEditorLayout.addView(this.mDocumentContentLinearLayout);

            //loadURLs the page hight when the document linear layout is post
            this.mDocumentContentLinearLayout.post(DocumentEditorActivity.this::LoadPageHight);

            //Start the auto save
            startAutoSave();

            //Close the progress dialog
            this.mProgressDialog.cancel();
        }
    }


     * Auto save thread
     *

    @Override
    public void run() {
        //Auto save thread
        //Save loop
        while(true) {
            //Check if the document
            if(!this.mIsDocumentDone) {
                //Save and update
                UpdateTempSaveAndGetSuccess();
            } else {
                //Exit the loop
                break;
            }

            //Wait until the nex save
            TimeUtiles.wait(AUTO_SAVE_TIME);
        }
    }

    /**
     * Function to install and run the auto save thread
     *

    private synchronized void startAutoSave() {
        //Check valid
        if(!mIsCanEdit) {
            return;
        }

        //Install and start the thread
        this.mAutoSaveThread = new Thread(this);
        this.mAutoSaveThread.start();
    }


    /**
     * Function to stop the auto save thread
     *

    private synchronized void stopAutoSave() {
        //Check valid
        if(this.mAutoSaveThread == null) {
            return;
        }

        //Stop the thread
        this.mAutoSaveThread.stop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.SaveFAB:
                save();
                break;

            case R.id.AddFailureFAB:
                addFailure();
                break;

            case R.id.OptionsFAB:
                options();
                break;

            case R.id.FailuresFAB:
                failures();
                break;

            case R.id.DoneFAB:
                done();
                break;
        }

        this.mMenuFAM.collapse();
    }

    /**
     * Function to save the document
     *

    private void save() {
        //Cehck save valid
        if (!mIsCanEdit) {
            return;
        }

        //Save and update the document
        new Thread(() -> {
            if (UpdateTempSaveAndGetSuccess()) {
                //Success
                runOnUiThread(() -> Toast.makeText(DocumentEditorActivity.this, "שמירת המסמך הצליחה (:", Toast.LENGTH_SHORT).show());

            } else {
                //Faild
                runOnUiThread(() -> new AlertUIDialog(DocumentEditorActivity.this, "משהו השתבש ):", "שמירת המסמך נכשלה").show());

            }
        }).start();
    }

    private void addFailure() {
        Intent intent = new Intent(DocumentEditorActivity.this, FailureEditor.class);
        FailureEditor.mFailure = null;
        FailureEditor.mDocument = mDocumentToEdit;
        FailureEditor.mEditFailure = false;
        startActivity(intent);
    }

    private void options() {
        int id = -1;
        if (mDocumentToEdit.getDocumentID() != DynamicObject.INTEGER_NULL_VALUE) {
            id = mDocumentToEdit.getDocumentID();
        }

        IdentificationDialog identificationDialog = new IdentificationDialog(DocumentEditorActivity.this, mDocumentToEdit, false, id, mIsCanEdit, new IdentificationDialog.ReturnResualt() {
            @Override
            public void onReturn() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(mIsCanEdit) {
                            UpdateTempSaveAndGetSuccess();
                        }
                    }
                }).start();
            }
        });

        identificationDialog.setCancelable(false);
        identificationDialog.show();

    }

    private void failures() {
        if (mDocumentToEdit.getFailureListSize() == 0) {
            if(mIsCanEdit){
                Toast.makeText(DocumentEditorActivity.this, "לא נוספו עדיין תקלות...", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(DocumentEditorActivity.this, "למסמך זה אין תקלות...", Toast.LENGTH_SHORT).show();

            }

        } else {
            Intent intent = new Intent(DocumentEditorActivity.this, FailureEditor.class);
            FailureEditor.mFailure = null;
            FailureEditor.mDocument = mDocumentToEdit;
            FailureEditor.mEditFailure = true;
            FailureEditor.mEditable = mIsCanEdit;
            startActivity(intent);

        }
    }

    private void done() {
        mDocumentToEdit.setEndTime(TimeUtiles.getDate());
        mDocumentToEdit.setTotalTime(TimeUtiles.getDateDifferent(mDocumentToEdit.getEndTime(), mDocumentToEdit.getStartTime()));

        if(mDocumentToEdit.getSignature() == null){
            SignatureDialog signatureDialog = new SignatureDialog(DocumentEditorActivity.this, mDocumentToEdit, new SignatureDialog.OnReturn() {
                @Override
                public void OnReturn() {
                    UpdateDocument();
                }
            });
            signatureDialog.show();

        } else {
            UpdateDocument();
        }

    }

    //Save And Update
    private synchronized boolean UpdateTempSaveAndGetSuccess() {
        mJSONDocumentParser.UpdateValuesAndGetLegalityFeedback(mDocumentStructure, false);

        return InsertDocumentToServer(false);
    }


    private synchronized ArrayList<Error> InstallDocumentAndGetErrors(boolean Available, boolean CheckLegality, boolean MakeStatistic, OnGetDocumentError OnGetDocumentError) {
        if (CheckLegality || Available) {
            ArrayList<String> errorsList = new ArrayList<String>();

            errorsList.addAll(CheckIfAllIdentificationsHaveValue());
            errorsList.addAll(CheckAllFailuresColsed());

            if (errorsList.size() > 0) {
                OnGetDocumentError.onGetErrors(errorsList);
                return null;
            }
        }

        ArrayList<Error> errorArrayList;

        if (MakeStatistic) {
            errorArrayList = mJSONDocumentParser.UpdateAllDocumentValuesAndGetLegalityFedback(mDocumentStructure, mDocumentToEdit, CheckLegality);

        } else {
            errorArrayList = mJSONDocumentParser.UpdateValuesAndGetLegalityFeedback(mDocumentStructure, CheckLegality).mErrorsArrayList;
        }

        return errorArrayList;
    }

    public synchronized boolean InsertDocumentToServer(boolean Available) {
        return UpdateDocumentAndGetSuccess(mDocumentToEdit, Available);
    }


    private float mTimeLimit = 3600000; //ms

    private ArrayList<TimeToFix> GetTimeFixConfigArray(){
        ArrayList<TimeToFix> mFixTimeList = new ArrayList<>();

        for (int i = 0; i < mDocumentToEdit.getFailureListSize(); i++) {
            Failure failure =  mDocumentToEdit.getFailure(i);

            if(failure.getStartWorkTime() == null || failure.getEndWorkTime() == null){
                TimeToFix timeToFix = new TimeToFix();

                /*
                failure.setStartWorkTime(new Date(0L);
                failure.mEndWorkTime = new Date(0L);


                timeToFix.mStartDate = failure.getStartWorkTime();
                timeToFix.mEndDate = failure.getEndWorkTime();
                timeToFix.mTotalTime = failure.getTotalWorkTime();

                timeToFix.mText = "תקלה מסוג " + failure.getMain() + " " + failure.getSub() + " נמצאה ללא זמן תיקון, האם אתה בטוח?";

                mFixTimeList.add(timeToFix);

            } else if(Math.abs(failure.getStartWorkTime().getTime() - failure.getEndWorkTime().getTime()) >= mTimeLimit){
                TimeToFix timeToFix = new TimeToFix();

                timeToFix.mStartDate = failure.getStartWorkTime();
                timeToFix.mEndDate = failure.getEndWorkTime();
                timeToFix.mTotalTime = failure.getTotalWorkTime();

                timeToFix.mText = "זמן התיקון של התקלה מסוג " + failure.getMain() + " " + failure.getSub() + " חורג מזמן התיקון הנורמאלי " + failure.getTotalWorkTime() + " האם אתה בטוח?";

                mFixTimeList.add(timeToFix);
            }

            if(Math.abs(failure.getStartTime().getTime() - failure.getEndTime().getTime()) >= mTimeLimit){
                TimeToFix timeToFix = new TimeToFix();

                timeToFix.mStartDate = failure.getStartTime();
                timeToFix.mEndDate = failure.getEndTime();
                timeToFix.mTotalTime = failure.getTotalTime();

                timeToFix.mText = "האם אתה בטוח התקלה היה פתוחה במשך  " + timeToFix.mTotalTime;
                timeToFix.mText = "תקלה מסוג " + failure.getMain() + " " + failure.getSub() + " הייתה פתוחה במשך " + failure.getTotalTime() + " האם אתה בטוח?";

                mFixTimeList.add(timeToFix);
            }
        }

        return mFixTimeList;
    }

    class TimeToFix {
        public Date mStartDate;
        public Date mEndDate;
        public String mTotalTime;

        public String mText;

    }

    public class TimeConfigAdapter extends BaseAdapter {

        private ArrayList<TimeToFix> mTimeToFixObjects;

        private LayoutInflater mLayoutInflater;

        private OnListEmpty mOnListEmpty;

        public TimeConfigAdapter(ArrayList<TimeToFix> TimeToFixObjects, LayoutInflater LayoutInflater){
            mTimeToFixObjects = TimeToFixObjects;
            mLayoutInflater = LayoutInflater;

        }

        @Override
        public int getCount() {
            return mTimeToFixObjects.size();
        }

        @Override
        public TimeToFix getItem(int position) {
            return mTimeToFixObjects.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View layout = mLayoutInflater.inflate(R.layout.timeconfigitem, null);

            final TimeToFix timeToFix = mTimeToFixObjects.get(position);

            Button changeDate = layout.findViewById(R.id.Button);
            TextView text = layout.findViewById(R.id.Text);

            text.setText(timeToFix.mText);

            changeDate.setOnClickListener(new OnClickListener() {
                int objectPosition = position;

                Date startDate = timeToFix.mStartDate;
                Date endDate = timeToFix.mEndDate;

                String totalTime = timeToFix.mTotalTime;

                @Override
                public void onClick(View v) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(DocumentEditorActivity.this, (view, hourOfDay, minute) -> {
                        totalTime = Integer.toDateTimeString(hourOfDay) + ":" + Integer.toDateTimeString(minute);

                        long endTime = TimeUtiles.getTime();

                        endDate.setTime(endTime);
                        startDate.setTime(endTime - ((hourOfDay * 3600000) + (minute * 60000)));

                        mTimeToFixObjects.remove(objectPosition);
                        TimeConfigAdapter.this.notifyDataSetChanged();

                    }, 0, 0, true);
                    timePickerDialog.show();
                }
            });

            return layout;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
            if(mOnListEmpty != null && mTimeToFixObjects.size() == 0){
                mOnListEmpty.OnListEmpty();
            }
        }

        public void setOnListEmptyListener(OnListEmpty OnListEmpty){
            mOnListEmpty = OnListEmpty;

        }
    }

    public interface OnListEmpty {
        void OnListEmpty();
    }

    /**************************************************************************************


    //Update Document

    private ArrayList<String> CheckIfAllIdentificationsHaveValue() {
        ArrayList<String> errorsList = new ArrayList<String>();

        for (Identification identification : mDocumentToEdit.getIdentificationsList()) {
            if (identification.getValue().equals("")) {
                errorsList.add("אתה חייב להזין את השדה " + identification.getName() + " בהגדרות המסמך");
            }
        }

        return errorsList;
    }

    private ArrayList<String> CheckAllFailuresColsed() {
        ArrayList<String> errorsList = new ArrayList<String>();

        for (int i = 0; i < mDocumentToEdit.getFailureListSize(); i++) {
            Failure failure = mDocumentToEdit.getFailure(i);

            if (!failure.isApproval()) {
                errorsList.add("תקלה מסוג " + failure.getMain() + " אינה סגורה");

                if(failure.getStartWorkTime() == null || failure.getEndTime() == null){
                    errorsList.add("תקלה מסוג " + failure.getMain() + " אניה מכילה זמנים");

                }

            }

            if (failure.getStartWorkTime() != null && failure.getEndWorkTime() == null) {
                errorsList.add("תקלה מסוג " + failure.getMain() + " נמצאת עדין בטיפול");
            }
        }

        return errorsList;
    }

    public void CreateFailureQAL(Document Document){
        String failureQAL = "";

        for (int i = 0; i < Document.getFailureListSize(); i++) {
            Failure failure = Document.getFailure(i);

            failureQAL += failure.getMain() + "\n";
            failureQAL += failure.getSub() + "\n";
            failureQAL += failure.getExpl() + "\n";
            failureQAL += failure.getTotalTime() + "\n";
            failureQAL += failure.getTotalWorkTime() + "\n";
        }

        Document.setFailuresQAL(failureQAL);
    }

    public synchronized void UpdateDocument() {

        new AsyncTask<Void, Void, Void>(){

            ProgressDialog mProgressDialog = new ProgressDialog(DocumentEditorActivity.this);

            ArrayList<Error> mErrorsList;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog.setMessage("מזין ערכי מסמך...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();

            }

            @Override
            protected Void doInBackground(Void... params) {

                CreateFailureQAL(mDocumentToEdit);
                mErrorsList = InstallDocumentAndGetErrors(true, true, true, new OnGetDocumentError() {
                    @Override
                    public void onGetErrors(final ArrayList<String> ErrorsList) {
                        runOnUiThread(() -> {
                            AlertUIDialog alertUIDialog = new AlertUIDialog(DocumentEditorActivity.this, "המסמך אינו שלם", "אנא פתור את כל התקלות כדי להמשיך", ErrorsList);
                            alertUIDialog.show();
                        });

                    }
                });

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                mProgressDialog.cancel();

                if(mErrorsList != null) {
                    ArrayList<TimeToFix> timeToFixList = GetTimeFixConfigArray();

                    if(timeToFixList.size() != 0) {
                        final ChangeTimeDialog changeTimeDialog = new ChangeTimeDialog(DocumentEditorActivity.this, () -> continueUpdateAfterTimeConfig(mErrorsList), new TimeConfigAdapter(timeToFixList, getLayoutInflater()));
                        changeTimeDialog.show();
                    } else {
                        continueUpdateAfterTimeConfig(mErrorsList);
                    }

                } else {
                    return;

                }

            }
        }.execute();

    }

    public void continueUpdateAfterTimeConfig(ArrayList<Error> ErrorsList){
        if (ErrorsList.size() == 0) {
            OnDocumentUpdateFinish();

        } else {
            OverrideDialog overrideDialog = new OverrideDialog(DocumentEditorActivity.this, ErrorsList, (OverrideSuccess, ScrollTo) -> {
                if (OverrideSuccess) {
                    OnDocumentUpdateFinish();
                    return;
                }

                if (ScrollTo != null) {
                    mScrollViewLayout.smoothScrollTo(0, ((View) ScrollTo.mLineViewGroup.mObject).getTop() + ((View) ScrollTo.mObjectView.mView).getTop());
                    ((View) ScrollTo.mObjectView.mView).requestFocus();
                }

            });
            overrideDialog.show();
        }
    }

    public void OnDocumentUpdateFinish() {

        if(OnDone != null){
            OnDone.OnDocumentUpdateDone();
        }

        final ProgressDialog progressDialog = new ProgressDialog(DocumentEditorActivity.this);

        final boolean[] dialogShowen = {false};
        new Handler(Looper.getMainLooper()).post(() -> {
            mFunnelPDF = new FunnelPDF(DocumentEditorActivity.this, mDocumentToEdit, mDocumentStructure, mDocumentEditorLayout);
            mFunnelPDF.Install();

            dialogShowen[0] = true;
        });

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog.setCancelable(false);
                progressDialog.setMessage("מזין את המסמך לשרת, ויוצר PDF...");
                progressDialog.show();

            }

            @Override
            protected Void doInBackground(Void... params) {
                while(!dialogShowen[0]);

                final boolean[] updateDone = {false, false};
                new Thread(() -> {
                    UpdateDocumentAndGetSuccess(mDocumentToEdit, true);

                    mIsDocumentDone = true;

                    sendUpdateDocumentListToAllDevices();

                    updateDone[0] = true;
                }).start();

                final int documentID = mDocumentToEdit.getDocumentID();
                mDocumentEditorLayout.post(() -> mFunnelPDF.GetPDF(PrintedPdfDocument -> {
                    new Thread(() -> {
                        try {
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

                            PrintedPdfDocument.writeTo(byteArrayOutputStream);
                            UpdatePDFFile(byteArrayOutputStream.toByteArray(), Integer.toDateTimeString(documentID));

                        } catch (Exception e) {
                            e.printStackTrace();
                            throw new RuntimeException("Fail To Create PDF");
                        }
                    }).start();

                    updateDone[1] = true;
                }));

                TimeUtiles.wait(2000);

                while(!updateDone[0] || !updateDone[1]);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressDialog.cancel();

                finish();

            }
        }.execute();
    }

    public void UpdatePDFFile(final byte[] ByteArray, final String DocumentID) {

        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("ID", DocumentID));

        VXQCApplication.mServerConnection.makeHttpRequest(URLs.mUpdatePDF, ByteArray, params);

        //TODO fix
    }

    private interface OnGetValuesError {
        boolean onGetErrorsKeepInSave(ArrayList<Error> ErrorsList);
    }

    private interface OnGetDocumentError {
        void onGetErrors(ArrayList<String> ErrorsList);
    }

    public interface OnDocumentLoadedListener {
        void documentIsLoaded();
    }

    public interface OnDone {
        void OnDocumentUpdateDone();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        OnSaveFAB(false);
    }

    */

    /**
     * Function to get a local static param
     *
     */
    public static Document getStaticDocuemnt() {
        return mDocumentToEdit;
    }

    /**
     * Function to get a local static param
     *
     */
    public static boolean isCanEdit() {
        return mIsCanEdit;
    }

    /**
     * Function to get a local static param
     *
     */
    public static boolean isNewDocument() {
        return mIsNewDocument;
    }

    /**
     * Create a intent to start the class
     * @param Context The context to create
     * @param Document The document to edit\show
     * @param IsNewDocument The New document flag
     * @param IsCanEdit The can edit falg
     * @return the intent
     *
     */
    public static Intent getIntent(
            Context Context,
            Document Document,
            boolean IsNewDocument,
            boolean IsCanEdit
    ) {
        //Check document valid
        if(Document == null) {
            throw new RuntimeException("DocumentEditorActivity: error while getIntent," +
                    "document param can't be null");
        }

        //Set the params
        DocumentEditorActivity.mDocumentToEdit = Document;
        DocumentEditorActivity.mIsNewDocument = IsNewDocument;
        DocumentEditorActivity.mIsCanEdit = IsCanEdit;

        //Create new intent and return it
        return new Intent(Context, DocumentEditorActivity.class);
    }

}

package com.visionix.vxqc.KAKA.Activitysa;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.visionix.vxqc.server.objects.Document;

import java.util.ArrayList;

public class EditDocument extends AppCompatActivity /* implements TextWatcher */ {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private EditText mSearchEditText;

    private RelativeLayout mBackgroundLayout;

    private static ArrayList<Document> mDocumentsArrayList = new ArrayList<Document>();
    private static ArrayList<Document> mDocumentsAdapterList = new ArrayList<Document>();
    //private static Adapter mAdapter;

    private static int mExpandedPosition;

    private static Context mContext;

    public static final int mCallBackTimeOut = 5000; //1500ms = 1.5sec
    public static final int mTransferCallBackTimeOut = 10000; //10000ms = 10sec

    private ArrayList<CountDownTimer> mTimersCountDown;

    private boolean mAlreadyLoad = false;

    /*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editdocument);

        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        mTimersCountDown = new ArrayList<>();

        mContext = this;

        mAlreadyLoad = false;

        mExpandedPosition = -1;

        Install();

    }

    @Override
    protected void onDestroy() {
        mOnDocumentsListUpdateEvent = null;

        for (int i = 0; i < mTimersCountDown.size(); i++) {
            CountDownTimer countDownTimer = mTimersCountDown.get(i);
            if(countDownTimer != null){
                try {

                } catch (Exception e){
                    countDownTimer.cancel();
                }
            }
        }

        super.onDestroy();
    }

    public static void SendSocketToUpdateList(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try { SocketHandler.SentJSONToAll(new JSONObject().put("Command", "DocumentsListUpdate"), ""); } catch (JSONException e) { e.printStackTrace(); }
            }
        }).start();

        return;
    }

    private void Install(){
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefreshLayout);
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mSearchEditText = (EditText) findViewById(R.id.SearchEditText);

        mBackgroundLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        UiUtiles.setLayoutMaterialColor(this, mBackgroundLayout);

        mSwipeRefreshLayout.setEnabled(false);

        mSearchEditText.addTextChangedListener(this);

        TextView SubTitle = (TextView) findViewById(R.id.SubTitle);

        ((AppBarLayout) findViewById(R.id.AppBarLayout)).addOnOffsetChangedListener(new OnOffsetChangedAlpha(50, SubTitle));

        mAdapter = new Adapter(getLayoutInflater());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(VXQCApplication.mBaseContext));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        return;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mContext = this;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });

        mOnDocumentsListUpdateEvent = new MessageManeger.OnDocumentsListUpdateEvent() {
            @Override
            public void OnDocumentsListUpdate() {
                new Load().execute();
            }
        };
        MessageManeger.mOnDocumentsListUpdateEventsList.add(mOnDocumentsListUpdateEvent);

        new Load().execute();
    }

    public void OpenDoucmnet(Document Document){
        new OpenDocumentAndGetSocketCallBack(Integer.toDateTimeString(Document.mID)).execute();
    }

    class OpenDocumentAndGetSocketCallBack extends AsyncTask<Void, Void, Void> {

        private String mID;
        private ProgressDialog mProgressDialog;

        public OpenDocumentAndGetSocketCallBack(String ID){
            mID = ID;
            mProgressDialog = new ProgressDialog(mContext);

            return;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("מבקש גישה למסמך");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            return;
        }

        private String mAnswer;
        private int mThreadsCounter;
        private int mReturnThreadsCounter;

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> paramsList = new ArrayList<NameValuePair>();
            paramsList.add(new BasicNameValuePair("AnswerIP", NetworkControl.mLastIP));

            JSONObject jsonObject = VXQCApplication.mServerConnection.makeHttpRequest(URLs.mDidYouEditThis, paramsList);

            try {
                JSONArray arrayIPs = jsonObject.getJSONArray("IPs");

                mThreadsCounter = 0;
                mReturnThreadsCounter = 0;

                mAnswer = "";

                JSONObject messageJSONObject = new JSONObject().put("Command", "DidYouEditThis").put("DocumentID", mID);
                final String message = messageJSONObject.toDateTimeString();

                for(int i = 0; i < arrayIPs.length(); i++){
                    try {
                        final String ip = arrayIPs.getString(i);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Socket socket = null;
                                try {
                                    socket = new Socket();

                                    socket.setSoTimeout(mCallBackTimeOut);

                                    socket.connect(new InetSocketAddress(ip, ServerConnection.mDefaultSocketPort), mCallBackTimeOut);

                                    SocketHandler.SentJSONBySocket(socket, message);

                                    Scanner scanner = new Scanner(socket.getInputStream());
                                    String answer = scanner.nextLine();

                                    if(!answer.equals("No") && !answer.equals("")){
                                        mAnswer = answer;
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if(socket != null && socket.isConnected()){
                                    try {
                                        socket.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                                mReturnThreadsCounter++;
                            }
                        }).start();

                        mThreadsCounter++;

                    } catch (Exception e) {

                    }
                }

                while(mThreadsCounter > mReturnThreadsCounter && mAnswer.equals(""));

            } catch (Exception e) {
                Report.SendLog("Error EditDocument x2", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mProgressDialog.cancel();

            if(mAnswer.equals("")){
                mScoketCallBack = null;
                ShowDocument(mID);

            } else {
                String IP = "";
                String UserName = "";

                try {
                    JSONObject jsonObject = new JSONObject(mAnswer);

                    IP = jsonObject.getString("AnswerIP");
                    UserName = jsonObject.getString("Questioner");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mScoketCallBack = new SocketCallBack(UserName, IP);

                ShowSomeoneEditDialog(mID);

            }

            return;
        }
    }

    private static CountDownTimer mCountDownTimer;
    private static AlertButtonsDialog mAlertButtonsDialog;
    public void ShowSomeoneEditDialog(final String ID){
        mAlertButtonsDialog = new AlertButtonsDialog(
                EditDocument.this,
                "עריכה כפולה!",
                "המסמך שבחרת נערך עכשיו על-ידי " + mScoketCallBack.mUserName + " תוכל לבקש ממנו להעביר את המסמך אליך.",
                "בקש מ " + mScoketCallBack.mUserName + " לערוך את המסמך במקומו",
                "חזור",
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final long startTime = TimeUtiles.GetTime();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                TransferDocument(ID, startTime);
                            }
                        }).start();

                        mCountDownTimer = new CountDownTimer(mTransferCallBackTimeOut, mTransferCallBackTimeOut){

                            @Override
                            public void onTick(long millisUntilFinished) {
                                return;
                            }

                            @Override
                            public void onFinish() {
                                mAlertButtonsDialog.cancel();
                                Toast.makeText(EditDocument.this, "ל " + mScoketCallBack.mUserName + " לוקח יותר מידי זמן להגיב", Toast.LENGTH_SHORT).show();
                            }
                        }.start();
                    }
                }
        );

        mAlertButtonsDialog.setCanceledOnTouchOutside(false);

        mAlertButtonsDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                CloseDialogs(null);
            }
        });

        mAlertButtonsDialog.show();
    }

    public void TransferDocument(String ID, long StartTime) {
        try {
            SocketHandler.SentJSONBySocket(mScoketCallBack.mIP, new JSONObject()
                    .put("Command", "AskForTransfer")
                    .put("Questioner", User.getConnectUser().mUserName)
                    .put("DocumentID", ID)
                    .put("AnswerIP", NetworkControl.mLastIP)
                    .put("StartTime", StartTime)
                    .put("TimeOut", mTransferCallBackTimeOut));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return;
    }

    public static void ShowDocument(final String ID){
        CloseDialogs(null);

        new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                new ShowDocument(ID).execute();
            }
        });


        return;
    }

    static class ShowDocument extends AsyncTask<Void, Void, Void> {

        private ProgressDialog mProgressDialog;

        private String mID;

        private Document mDocument;

        public ShowDocument(String ID){
            mID = ID;

            return;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("מוריד את המסמך");
            mProgressDialog.show();

            return;
        }

        @Override
        protected Void doInBackground(Void... params1) {

            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("ID", mID));

            JSONObject jsonObject = VXQCApplication.mServerConnection.makeHttpRequest(URLs.mGetEditDocument, params);

            mDocument = new Document();

            try {
                jsonObject = jsonObject.getJSONObject("Document");

                mDocument.mDocumentSource.mID = Integer.parseInt(jsonObject.getString("DocumentSourceID"));
                mDocument.mSourceDocumentID = mDocument.mDocumentSource.mID;

                mDocument.mDocumentSource.mProduct = jsonObject.getString("Product");
                mDocument.mDocumentSource.mType = jsonObject.getString("Type");
                mDocument.mDocumentSource.mRevision = jsonObject.getString("Revision");

                mDocument.mDocumentSource.mDate = jsonObject.getString("UpdateAt");
                mDocument.mDocumentSource.mPN = jsonObject.getString("PN");
                mDocument.mDocumentSource.mDocumentName = jsonObject.getString("DocumentName");
                mDocument.mDocumentSource.mPartNumber = jsonObject.getString("PartNumber");
                mDocument.mDocumentSource.mDescription = jsonObject.getString("Description");
                mDocument.mDocumentSource.mPurpose = jsonObject.getString("Purpose");

                mDocument.mID = Integer.parseInt(jsonObject.getString("ID"));
                mDocument.mStartTime = TimeUtiles.mSimpleDateFormat.parse(jsonObject.getString("StartTime"));
                mDocument.mEndTime = TimeUtiles.mSimpleDateFormat.parse(jsonObject.getString("EndTime"));
                mDocument.mTotalTime = jsonObject.getString("TotalTime");
                mDocument.mTester = jsonObject.getString("Tester");

                mDocument.mDocumentJSONArray = jsonObject.getJSONArray("DocumentJSONArray");
                mDocument.mFailuresJSONArray = jsonObject.getJSONArray("FailuresJSONArray");

                mDocument.mFailuresList = new ArrayList<Failure>();
                for(int i = 0; i < mDocument.mFailuresJSONArray.length(); i++){
                    JSONObject failureJSON = mDocument.mFailuresJSONArray.getJSONObject(i);

                    Failure failure = new Failure();

                    failure.mFailureSource.mMain = failureJSON.getString("Main");
                    failure.mFailureSource.mSub = failureJSON.getString("Sub");
                    failure.mFailureSource.mExpl = failureJSON.getString("Expl");

                    failure.mCorrectiveAction = failureJSON.getString("CorrectiveAction");
                    failure.mDescription = failureJSON.getString("Description");
                    failure.mTechnician = failureJSON.getString("Technician");

                    if(failureJSON.has("StartTime")) { failure.mStartTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("StartTime")); }
                    if(failureJSON.has("EndTime")) { failure.mEndTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("EndTime")); }
                    if(failureJSON.has("StartWorkTime")) { failure.mStartWorkTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("StartWorkTime")); }
                    if(failureJSON.has("EndWorkTime")) { failure.mEndWorkTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("EndWorkTime")); }
                    if(failureJSON.has("TotalTime")) { failure.mCorrectiveAction = failureJSON.getString("TotalTime"); }
                    if(failureJSON.has("TotalWorkTime")) { failure.mCorrectiveAction = failureJSON.getString("TotalWorkTime"); }

                    failure.mApproval = failureJSON.has("Approval") && failureJSON.getBoolean("Approval");

                    mDocument.mFailuresList.add(failure);
                }

                mDocument.mAvailable = jsonObject.getString("Available").equals("1");

                JSONObject identificationsJSONObject = jsonObject.getJSONObject("Identifications");
                JSONArray names = identificationsJSONObject.names();
                for(int j = 0; j < names.length(); j++){
                    Identification identification = new Identification();

                    identification.mName = names.getString(j);
                    identification.mValue = identificationsJSONObject.getString(identification.mName);

                    mDocument.mDocumentSource.mIdentificationList.add(identification);
                }

            } catch (Exception e){
                e.printStackTrace();

                mDocument = null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.cancel();

            if(mDocument == null){
                Toast.makeText(VXQCApplication.mBaseContext, "הורדת המסמך נכשלה", Toast.LENGTH_SHORT).show();
            } else {
                Intent i = new Intent(mContext, DocumentEditorActivity.class);
                DocumentEditorActivity.mDocument = mDocument;
                DocumentEditorActivity.mCanEdit = true;
                DocumentEditorActivity.mNewDocument = true;

                DocumentEditorActivity.mOnDone = new DocumentEditorActivity.OnDone() {
                    @Override
                    public void OnDocumentUpdateDone() {

                        int id = Integer.parseInt(mID);
                        for(int i = 0; i < mDocumentsAdapterList.size(); i++){
                            Document thisDocument = mDocumentsAdapterList.get(i);

                            if(thisDocument.mID == id){
                                mDocumentsAdapterList.remove(i);
                                mAdapter.notifyItemRemoved(i);

                                return;
                            }
                        }

                        for(int i = 0; i < mDocumentsArrayList.size(); i++){
                            Document thisDocument = mDocumentsArrayList.get(i);

                            if(thisDocument.mID == id){
                                mDocumentsArrayList.remove(i);

                                return;
                            }
                        }


                    }
                };

                mContext.startActivity(i);
            }

            return;
        }
    }

    public static void CloseDialogs(final String Message){
        if(mCountDownTimer != null){
            mCountDownTimer.cancel();

        }

        if(mAlertButtonsDialog != null){
            mAlertButtonsDialog.cancel();

        }

        if(Message != null){
            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(VXQCApplication.mBaseContext, Message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        mAdapter.Filter();

    }


    class Load extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(!mAlreadyLoad) {
                mSwipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(true);
                    }
                });
            }

            mDocumentsArrayList.clear();
            mDocumentsAdapterList.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                LoadList();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!mAlreadyLoad || !mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });

                mAlreadyLoad = true;
            }

            mAdapter.notifyDataSetChanged();
        }
    }

    private synchronized void LoadList() throws JSONException, ParseException {
        JSONObject jsonObject = VXQCApplication.mServerConnection.makeHttpRequest(URLs.mGetEditDocumentsList, null);
        JSONArray jsonArray = jsonObject.getJSONArray("Documents");

        if(jsonObject.has("HaveMore")){
            if(jsonObject.getBoolean("HaveMore")){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "מציג את ה50 מסמכים האחרונים", Toast.LENGTH_SHORT).show();
                    }
                });

                Report.SendLog("יש יותר מ50 מסמכים פתוחים בשרת", null);
            }
        }

        mDocumentsArrayList.clear();
        for(int i = 0; i < jsonArray.length(); i++){
            Document document = new Document();

            JSONObject object = jsonArray.getJSONObject(i);

            document.mID = object.getInt("ID");
            document.mTester = object.getString("Tester");
            document.mStartTime = TimeUtiles.mSimpleDateFormat.parse(object.getString("StartTime"));
            document.mDocumentSource.mProduct = object.getString("Product");
            document.mDocumentSource.mType = object.getString("Type");
            document.mDocumentSource.mRevision = object.getString("Revision");
            document.mDocumentSource.mImage = VXQCApplication.mSourceList.mDocumentImagesList.get(document.mDocumentSource.mProduct + document.mDocumentSource.mType);

            if(document.mDocumentSource.mImage == null){
                document.mDocumentSource.mImage = Image.GetBitmapFromDrawable(VXQCApplication.mBaseContext, R.drawable.imagenotfound);
            }

            JSONObject identificationsJSONObject = object.getJSONObject("Identifications");
            JSONArray names = identificationsJSONObject.names();
            for(int j = 0; j < names.length(); j++){
                Identification identification = new Identification();

                identification.mName = names.getString(j);
                identification.mValue = identificationsJSONObject.getString(identification.mName);

                document.mDocumentSource.mIdentificationList.add(identification);
            }

            mDocumentsArrayList.add(document);
        }

        mDocumentsAdapterList.clear();
        mDocumentsAdapterList.addAll(mDocumentsArrayList);
    }

    public void Delete(final Document Document, final int Position){
        if(User.getConnectUser().mManagerPermission){
            FinalDelete(Document, Position);
        } else {
            OverrideDialog overrideDialog = new OverrideDialog(
                    EditDocument.this,
                    "מחיקת מסמך לא גמור", "אנא התחבר כמנהל על מנת לאשר את מחיקת מסמך מסוג " + Document.mDocumentSource.mProduct + " " + Document.mDocumentSource.mType,
                    new OverrideDialog.OnReturn() {
                        @Override
                        public void OnReturn(boolean OverrideSuccess, Object ScrollTo) {
                            if (OverrideSuccess) {
                                FinalDelete(Document, Position);
                        }
                }
            });
            overrideDialog.show();
        }

        return;
    }

    public void FinalDelete(final Document Document, final int Position){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("ID", Integer.toDateTimeString(Document.mID)));

                if(VXQCApplication.mServerConnection.makeHttpRequest(URLs.mDeleteDocumentByID, params) == null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "שגיאה במהלך המחיקה", Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "נמחק בהצלחה", Toast.LENGTH_SHORT).show();

                        }
                    });

                    mDocumentsAdapterList.remove(Position);
                    mAdapter.notifyItemRemoved(Position);

                    SendSocketToUpdateList();
                }

                //mAdapter.notifyDataSetChanged();

                //new loadURLs().execute();
            }
        }).start();
    }

    class Adapter extends RecyclerView.Adapter<Holder> {

        private LayoutInflater mLayoutInflater;

        public Adapter(LayoutInflater LayoutInflater){
            mLayoutInflater = LayoutInflater;

            return;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = mLayoutInflater.inflate(R.layout.editdocumentsitem, parent, false);

            Holder holder = new Holder(layout);

            holder.mImageView = (CircularImageView) layout.findViewById(R.id.CircularImageView);
            holder.mTitleTextView = (TextView) layout.findViewById(R.id.Title);
            holder.mSubTitleTextView = (TextView) layout.findViewById(R.id.SubTitle);
            holder.mTimeTextView = (TimeTestView) layout.findViewById(R.id.TimeUtiles);
            holder.mExpendRelativeLayout = (RelativeLayout) layout.findViewById(R.id.ExpendRelativeLayout);
            holder.mExpendTextView = (TextView) layout.findViewById(R.id.TextView);

            return holder;
        }

        @Override
        public void onBindViewHolder(final Holder holder, final int position) {
            final Document document = mDocumentsAdapterList.get(position);

            String text = "";
            String firstIdentificationWithValue = "ללא סיריאלי";
            for (Identification identification : document.mDocumentSource.mIdentificationList) {
                if(!identification.mValue.equals("")) {
                    text += identification.mName + " - " + identification.mValue + "\n";

                    if(identification.mName.equals("Machine SN")){
                        firstIdentificationWithValue = identification.mValue;
                    }
                }
            }

            if(text.equals("")){
                text += "כל הערכים ריקים";
            }

            holder.mTitleTextView.setText(document.mDocumentSource.mProduct + " - " + document.mDocumentSource.mType + " " + firstIdentificationWithValue);
            holder.mSubTitleTextView.setText("נוצר על-ידי " + document.mTester);
            holder.mImageView.setImageBitmap(document.mDocumentSource.mImage);

            holder.mTimeTextView.Start(document.mStartTime.getTime());

            holder.mExpendRelativeLayout.setVisibility(View.GONE);

            holder.mTimeTextView.setTextColor(UiUtiles.getAppTheme().mPrimaryColor);

            Button openButton = (Button) holder.itemView.findViewById(R.id.OpenButton);

            openButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    OpenDoucmnet(document);
                }
            });

            holder.mExpendTextView.setText(text);

            holder.itemView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    final int thisPosition = holder.getPosition();

                    if (mExpandedPosition == thisPosition) {
                        notifyItemChanged(thisPosition);

                        mExpandedPosition = -1;
                    } else {
                        notifyItemChanged(mExpandedPosition);

                        mExpandedPosition = thisPosition;

                        notifyItemChanged(mExpandedPosition);

                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.smoothScrollToPosition(thisPosition);
                            }
                        });

                    }
                }
            });

            holder.itemView.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Delete(document, position);

                    return true;
                }
            });

            if (position == mExpandedPosition) {
                holder.mExpendRelativeLayout.setVisibility(View.VISIBLE);

            } else {
                holder.mExpendRelativeLayout.setVisibility(View.GONE);

            }

            return;
        }

        @Override
        public int getItemCount() {
            return mDocumentsAdapterList.size();
        }

        public void Filter(){
            String text = mSearchEditText.getText().toDateTimeString();
            if(text != null && text.length() > 0){
                ArrayList<Document> filterList = new ArrayList<Document>();

                String upperCaseConstraint = text.toDateTimeString().toUpperCase();

                int size = mDocumentsArrayList.size();

                for(int i = 0; i < size; i++){
                    Document document = mDocumentsArrayList.get(i);

                    String tempTester = document.mTester.toUpperCase();
                    String tempProduct = document.mDocumentSource.mProduct.toUpperCase();
                    String tempType = document.mDocumentSource.mType.toUpperCase();

                    if(tempTester.contains(upperCaseConstraint) || tempProduct.contains(upperCaseConstraint) || tempType.contains(upperCaseConstraint)){
                        filterList.add(document);
                    } else {
                        for (int j = 0; j < document.mDocumentSource.mIdentificationList.size(); j++) {
                            if (document.mDocumentSource.mIdentificationList.get(j).mValue.toUpperCase().contains(upperCaseConstraint)) {
                                filterList.add(document);
                                break;
                            }
                        }
                    }
                }

                mDocumentsAdapterList.clear();
                mDocumentsAdapterList.addAll(filterList);

                mAdapter.notifyDataSetChanged();
            } else {
                mDocumentsAdapterList.clear();
                mDocumentsAdapterList.addAll(mDocumentsArrayList);

                mAdapter.notifyDataSetChanged();
            }
        }

    }

    class Holder extends RecyclerView.ViewHolder {

        public TextView mTitleTextView;
        public TextView mSubTitleTextView;
        public TimeTestView mTimeTextView;
        public CircularImageView mImageView;

        public TextView mExpendTextView;
        public RelativeLayout mExpendRelativeLayout;

        public Holder(View itemView) {
            super(itemView);

            return;
        }
    }
    */

}


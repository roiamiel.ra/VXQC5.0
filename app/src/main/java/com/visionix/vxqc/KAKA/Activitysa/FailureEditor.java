package com.visionix.vxqc.KAKA.Activitysa;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.visionix.vxqc.server.objects.Document;
import com.visionix.vxqc.server.objects.Failure;

import java.util.ArrayList;
import java.util.HashMap;

public class FailureEditor extends AppCompatActivity /* implements OnItemClickListener, OnCheckedChangeListener, OnClickListener */ {

    //Global
    public static Failure mFailure = null;
    public static Document mDocument = null;
    public static boolean mEditFailure = false;

    public static boolean mEditable = true;

    /*****************************************************************/


    //Local
    private EditText                mDescriptionEditText;
    private EditText                mCorrectiveActionEditText;
    private EditText                mTechincianEditText;

    private FloatingActionButton    mCreateFAB;

    private ListView                mFailuresListView;

    private CheckBox                mApprovalCheckBox;

    private TextView                mTitleTextView;
    private TextView                mSubTitleTextView;

    private RelativeLayout          mMainLayout;
    private RelativeLayout          mFloatingLayout;
    private RelativeLayout          mSelectFailure;
    private RelativeLayout          mFailureEditor;

    private Button                  mTimerButton;

    //private Adapter                mAdapter;
    //private FailureCreatedAdapter  mFailuresFromDocumentAdapter;

    private HashMap<String, HashMap<String, HashMap<String, Void>>> mFailuresList;

    private boolean mCreateNew = false;

    private boolean mSelectedFailureFromDocumentAndNowInEdit = false;
    private ArrayList<Failure> mFailuresFromDocumentList;

    /*****************************************************************/


    /*

    //Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.failureeditor);

        if(mDocument == null){
            AlertUIDialog alertUIDialog = new AlertUIDialog(FailureEditor.this, "אופס, משהו השתבש ):", "משהו השתבש במהלך בניית התקלה");
            alertUIDialog.show();

            finish();

            return;
        }

        overridePendingTransition(R.anim.right_in, R.anim.left_out);

        Install();

    }

    public void Install() {

        mDescriptionEditText         = (EditText)                findViewById(R.id.DescriptionEditText);
        mCorrectiveActionEditText    = (EditText)                findViewById(R.id.CorrectiveActionEditText);
        mTechincianEditText          = (EditText)                findViewById(R.id.TechincianEditText);

        mCreateFAB                   = (FloatingActionButton)    findViewById(R.id.CreateFailureFAB);

        mFailuresListView            = (ListView)                findViewById(R.id.FailuresList);

        mApprovalCheckBox            = (CheckBox)                findViewById(R.id.ApprovalCheckBox);

        mTitleTextView               = (TextView)                findViewById(R.id.Title);
        mSubTitleTextView            = (TextView)                findViewById(R.id.SubTitle);

        mMainLayout                  = (RelativeLayout)          findViewById(R.id.MainLayout);
        mFloatingLayout              = (RelativeLayout)          findViewById(R.id.FloatLayout);
        mSelectFailure               = (RelativeLayout)          findViewById(R.id.SelectFailure);
        mFailureEditor               = (RelativeLayout)          findViewById(R.id.FailureEditor);

        mTimerButton                 = (Button)                 findViewById(R.id.TimerButton);

        UiUtiles.setLayoutMaterialColor(FailureEditor.this, mMainLayout);

        mCreateFAB.setBackgroundTintList(ColorStateList.valueOf(UiUtiles.getAppTheme().mColorAccent));
        mCreateFAB.setOnClickListener(this);

        mApprovalCheckBox.setOnCheckedChangeListener(this);

        if(mFailure == null || mEditFailure) {

            mCreateFAB.setVisibility(View.GONE);
            mFailureEditor.setVisibility(View.GONE);
            mSelectFailure.setVisibility(View.VISIBLE);

            mFailuresListView.setOnItemClickListener(this);

        }

        if(mFailure == null && !mEditFailure){
            mCreateNew = true;

            mFailure = new Failure(new FailureSource());

            mFailuresList = VXQCApplication.mSourceList.mFailuresList;

            mAdapter = new Adapter(mThisFailuresList, getLayoutInflater());
            mFailuresListView.setAdapter(mAdapter);

            new Thread(() -> mFailure.setStartTime(TimeUtiles.getDate())).start();

            LoadNextList();

        } else if(mEditFailure) {
            mTitleTextView.setText("תקלות שנוצרו");
            mSubTitleTextView.setText("לחץ על תקלה שנוצרה כדי לערוך אותה");

            mFailuresFromDocumentList = mDocument.mFailuresList;

            mFailuresFromDocumentAdapter = new FailureCreatedAdapter(mFailuresFromDocumentList);
            mFailuresListView.setAdapter(mFailuresFromDocumentAdapter);

        } else {
            mCreateNew = false;
            ShowEditor();

        }

        return;
    }

    //Build Lists
    ArrayList<String> mThisFailuresList = new ArrayList<String>();

    private synchronized void LoadNextList(){
        UpdateTitlesToCreateMode();

        if(mFailure.mFailureSource.mMain == null){
            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.keySet());

            mAdapter.notifyDataSetChanged();

        } else if(mFailure.mFailureSource.mSub == null){
            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.get(mFailure.mFailureSource.mMain).keySet());

            mAdapter.notifyDataSetChanged();

        } else if(mFailure.mFailureSource.mExpl == null){
            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.get(mFailure.mFailureSource.mMain).get(mFailure.mFailureSource.mSub).keySet());

            mAdapter.notifyDataSetChanged();

        } else {
            ShowEditor();
        }

        return;
    }

    private synchronized void ReturnBack(){
        if(!mCreateNew){
            onClick(null);
        }

        UpdateTitlesToCreateMode();

        if(mFailure.mFailureSource.mExpl != null){
            mFailure.mFailureSource.mExpl = null;

            HideEditor();
            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.get(mFailure.mFailureSource.mMain).get(mFailure.mFailureSource.mSub).keySet());

            mAdapter.notifyDataSetChanged();

        } else if(mFailure.mFailureSource.mSub != null){
            mFailure.mFailureSource.mSub = null;

            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.get(mFailure.mFailureSource.mMain).keySet());

            mAdapter.notifyDataSetChanged();

        } else if(mFailure.mFailureSource.mMain != null) {
            mFailure.mFailureSource.mMain = null;

            mThisFailuresList.clear();
            mThisFailuresList.addAll(mFailuresList.keySet());

            mAdapter.notifyDataSetChanged();

        } else {
            super.onBackPressed();
        }

        return;
    }

    private void UpdateTitlesToCreateMode(){
        mSubTitleTextView.setText("בחר את סוג התקלה");
        mTitleTextView.setText("יצירת תקלה חדשה");

        return;
    }

    //Build Editor

    public void ShowEditor(){
        final int state = mFailure.mStartWorkTime != null ? (mFailure.mEndWorkTime == null ? 2 : 3) : 1;

        OnClickListener onClickListener = new OnClickListener() {
            private int mState = state;

            @Override
            public void onClick(View view) {
                if(mState == 1){
                    mTimerButton.setText("תהליך התיקון התחיל");

                    mState = 2;

                    mFailure.mEndWorkTime = null;
                    mFailure.mStartWorkTime = null;
                    mFailure.mTotalWorkTime.mString = null;

                } else if(mState == 2){
                    mTimerButton.setText("התקלה תוקנה");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mFailure.mStartWorkTime = TimeUtiles.GetDateInDate();
                        }
                    }).start();
                    mFailure.mEndWorkTime = null;
                    mFailure.mTotalWorkTime.mString = null;

                    mState = 3;
                } else {
                    mTimerButton.setText("התחל שוב");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mFailure.mEndWorkTime = TimeUtiles.GetDateInDate();
                            mFailure.mTotalWorkTime.mString = TimeUtiles.GetDifferentialOfDate(mFailure.mEndWorkTime, mFailure.mStartWorkTime);

                        }
                    }).start();

                    mState = 2;
                }
            }
        };

        mTimerButton.setOnClickListener(onClickListener);
        onClickListener.onClick(null);

        if(mCreateNew){
            mSubTitleTextView.setText("צור תקלה חדשה מסוג " + mFailure.mFailureSource.mMain + " - " + mFailure.mFailureSource.mSub + " - " + mFailure.mFailureSource.mExpl);
            mTitleTextView.setText("יצירת תקלה חדשה");

            mCreateFAB.setImageResource(R.drawable.next);

        } else {
            mSubTitleTextView.setText("ערוך תקלה מסוג " + mFailure.mFailureSource.mMain + " - " + mFailure.mFailureSource.mSub + " - " + mFailure.mFailureSource.mExpl);
            mTitleTextView.setText("עריכת תקלה");
            mCreateFAB.setImageResource(R.drawable.back);

            mTimerButton.setEnabled(mEditable);

            mDescriptionEditText.setText(mFailure.mDescription);
            mDescriptionEditText.setEnabled(mEditable);

            mCorrectiveActionEditText.setText(mFailure.mCorrectiveAction);
            mCorrectiveActionEditText.setEnabled(mEditable);

            mTechincianEditText.setText(mFailure.mTechnician);
            mTechincianEditText.setEnabled(mEditable);

            mApprovalCheckBox.setChecked(mFailure.mApproval);
            mApprovalCheckBox.setEnabled(mEditable);
        }

        mCreateFAB.setVisibility(View.VISIBLE);
        mFailureEditor.setVisibility(View.VISIBLE);
        mSelectFailure.setVisibility(View.GONE);

    }

    public void HideEditor(){
        mCreateFAB.setVisibility(View.GONE);
        mFailureEditor.setVisibility(View.GONE);
        mSelectFailure.setVisibility(View.VISIBLE);

        if(mEditFailure){
            mTitleTextView.setText("תקלות שנוצרו");
            mSubTitleTextView.setText("לחץ על תקלה שנוצרה כדי לערוך אותה");
        } else {
            mDescriptionEditText.setText("");
            mCorrectiveActionEditText.setText("");
            mTechincianEditText.setText("");

            mApprovalCheckBox.setChecked(false);

            mFailure.mStartTime = null;
            mFailure.mEndTime = null;
            mFailure.mStartWorkTime = null;
            mFailure.mEndWorkTime = null;
        }

        return;
    }

    //Events

     @Override
    public void onClick(View v) {

         if(mEditable) {

             mCorrectiveActionEditText.setError(null);

             String description = mDescriptionEditText.getText().toDateTimeString();
             String correctiveAction = mCorrectiveActionEditText.getText().toDateTimeString();
             String techincian = mTechincianEditText.getText().toDateTimeString();

             if (correctiveAction.equals("")) {
                 mCorrectiveActionEditText.setError("אתה חייב להזין פעולה מתקנת");
                 mCorrectiveActionEditText.requestFocus();

                 return;
             }

             mFailure.mDescription = description;
             mFailure.mCorrectiveAction = correctiveAction;
             mFailure.mTechnician = techincian;
             mFailure.mApproval = mApprovalCheckBox.isChecked();

             if (mCreateNew) {
                 DocumentEditorActivity.mDocument.mFailuresList.add(mFailure);
                 Toast.makeText(FailureEditor.this, "התקלה נוצרה בהצלחה :)", Toast.LENGTH_SHORT).show();

             } else {
                 Toast.makeText(FailureEditor.this, "התקלה עודכנה בהצלחה :)", Toast.LENGTH_SHORT).show();

             }

         }

         if(mSelectedFailureFromDocumentAndNowInEdit){
             HideEditor();
             onResume();

             mSelectedFailureFromDocumentAndNowInEdit = false;

         } else {
             finish();
         }

         return;
    }

    @Override
    public void finish() {
        super.finish();
        
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        
        return;
    }

    @Override
    public void onBackPressed() {
        if(!mEditFailure) {
            ReturnBack();

        } else if(mSelectedFailureFromDocumentAndNowInEdit){
            onClick(null);
        } else {
            super.onBackPressed();

        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mEditFailure){
            mFailuresFromDocumentAdapter.notifyDataSetChanged();
        }

        return;
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(mFailure != null){
                    if(isChecked) {
                        mFailure.mEndTime = TimeUtiles.GetDateInDate();
                        if(mFailure.mStartTime == null){
                            mFailure.mStartTime = mFailure.mEndTime;
                            Report.SendLog("FailureEditor class error: 777", null);
                        }

                        mFailure.mTotalTime.mString = TimeUtiles.GetDifferentialOfDate(mFailure.mEndTime, mFailure.mStartTime);
                    } else {
                        mFailure.mEndTime = null;
                        mFailure.mTotalTime.mString = null;
                    }
                }

                return;
            }
        }).start();

        return;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(mCreateNew){
            if(mFailure.mFailureSource.mMain == null){
                mFailure.mFailureSource.mMain = mThisFailuresList.get(i);

            } else if(mFailure.mFailureSource.mSub == null){
                mFailure.mFailureSource.mSub = mThisFailuresList.get(i);

            } else if(mFailure.mFailureSource.mExpl == null){
                mFailure.mFailureSource.mExpl = mThisFailuresList.get(i);

            } else {
                Toast.makeText(FailureEditor.this, "נכשל במהלך הרכבת התקלה :(", Toast.LENGTH_SHORT).show();

                mFailure.mFailureSource.mMain = null;
                mFailure.mFailureSource.mSub = null;
                mFailure.mFailureSource.mExpl = null;

            }

            LoadNextList();

        } else if(mEditFailure){
            mSelectedFailureFromDocumentAndNowInEdit = true;
            mFailure = mFailuresFromDocumentList.get(i);
            ShowEditor();
        }

        return;
    }

    private class FailureCreatedAdapter extends BaseAdapter {

        ArrayList<Failure> mFailuresList;

        public FailureCreatedAdapter(ArrayList<Failure> FailuresList){
            mFailuresList = FailuresList;
        }

        @Override
        public int getCount() {
            return mFailuresList.size();
        }

        @Override
        public Object getItem(int i) {
            return mFailuresList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View layout = getLayoutInflater().inflate(R.layout.basiclistobject, viewGroup, false);

            TextView textView = (TextView) layout;

            Failure failure = mFailuresList.get(i);

            boolean timerOn = (failure.mStartWorkTime != null && failure.mEndWorkTime == null);

            textView.setText("תקלה מסוג " + failure.mFailureSource.mMain + " " + failure.mFailureSource.mSub + (timerOn ? " - בתהליך תיקון" : "") + (failure.mApproval ? "" : " - לא סגור"));

            if(timerOn || !failure.mApproval){
                textView.setTextColor(android.graphics.Color.RED);
            }

            return layout;
        }
    }

    class Adapter extends BaseAdapter {

        LayoutInflater mLayoutInflater;
        ArrayList<String> mArrayList;

        public Adapter(ArrayList<String> ArrayList, LayoutInflater LayoutInflater) {
            mLayoutInflater = LayoutInflater;
            mArrayList = ArrayList;
        }

        @Override
        public int getCount() {
            return mArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return mArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TextView textView = (TextView) mLayoutInflater.inflate(R.layout.failureitem, null);

            textView.setText(mArrayList.get(position));

            textView.startAnimation(android.view.animation.AnimationUtils.loadAnimation(FailureEditor.this, android.R.anim.fade_in));

            return textView;
        }
    }

    */

}

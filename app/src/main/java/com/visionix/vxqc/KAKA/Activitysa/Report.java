package com.visionix.vxqc.KAKA.Activitysa;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

import java.util.ArrayList;

public class Report extends AppCompatActivity implements OnClickListener {

    private RelativeLayout          mMainLayout;
    private RelativeLayout          mFloatLayout;

    private FloatingActionButton    mSendFAB;

    private EditText                mTitleEditText;
    private EditText                mMessageEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report);

        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        install();

    }

    private void install() {
        mMainLayout     = findViewById(R.id.MainLayout);
        mFloatLayout    = findViewById(R.id.FloatLayout);

        mSendFAB        = findViewById(R.id.SendFAB);

        mTitleEditText  = findViewById(R.id.TitleEditText);
        mMessageEditText  = findViewById(R.id.MessageEditText);

        mSendFAB.setBackgroundTintList(ColorStateList.valueOf(UiUtiles.getAppTheme().mColorAccent));
        mSendFAB.setOnClickListener(this);

        UiUtiles.setLayoutMaterialColor(this, mMainLayout);
    }

    @Override
    public void onClick(View v) {
        mTitleEditText.setError(null);
        mMessageEditText.setError(null);

        String title = mTitleEditText.getText().toString();

        if(title.equals("")){
            mTitleEditText.setError("שדה זה לא יכול להיות ריק");
            mTitleEditText.requestFocus();
            return;
        }

        if(title.length() < 3){
            mTitleEditText.setError("שדה זה חייב להכיל לפחות 3 תווים");
            mTitleEditText.requestFocus();
            return;
        }

        String message = mMessageEditText.getText().toString();

        if(message.equals("")){
            mMessageEditText.setError("שדה זה לא יכול להיות ריק");
            mMessageEditText.requestFocus();
            return;
        }

        if(message.length() < 8){
            mMessageEditText.setError("שדה זה חייב להכיל לפחות 8 תווים");
            mMessageEditText.requestFocus();
            return;
        }

        //new mSendReportTask(title, message, null, User.getConnectUser().mUserName.replaceAll(" ", "-")).execute();

        Toast.makeText(this, "נשלח בהצלחה!", Toast.LENGTH_SHORT).show();

        finish();
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.right_in, R.anim.left_out);

    }

    public static class mSendReportTask extends AsyncTask<Void, Void, Void> {

        private String mTitle;
        private String mMessage;
        private String mNote;
        private String mUserName;

        public mSendReportTask(String Title, String Message, String Note, String UserName){
            mTitle = Title;
            mMessage = Message;
            mNote = Note;
            mUserName = UserName;

        }

        @Override
        protected Void doInBackground(Void... params) {
            if(mNote != null && !mNote.equals("")){
                mMessage += "\nNote:\n\t" + mNote;
            }

            /*
            ArrayList<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("Title", mTitle));
            param.add(new BasicNameValuePair("Message", mMessage));
            param.add(new BasicNameValuePair("UserName", mUserName));

            VXQCApplication.mServerConnection.makeHttpRequest(URLs.mSendReport, false, param);

*/

            return null;
        }
    }


    private static final int mListMaxSize = 50;
    private static ArrayList<LogObject> mLogsList = new ArrayList<>(mListMaxSize);
    public static synchronized void sendLog(String Message, String Note){
        sendLog(Message, Note, false);

    }

    public static synchronized void sendLog(String Message, String Note, boolean UserLog){
        Log.d("VXQC5.0 Application", Message + " - " + Note);

        if(UserLog){
            PushLog(Message, Note);
            return;
        }

        int size = mLogsList.size();
        boolean alredySend = false;
        for (int i = 0; i < size; i++) {
            LogObject logObject = mLogsList.get(i);

            if(((logObject.mMessage == null && Message == null) || (Message != null && (Message.equals(logObject.mMessage)))) && ((logObject.mNote == null && Note == null) || (Note != null && (Note.equals(logObject.mNote))))){
                alredySend = true;
                break;
            }
        }

        if(alredySend){
            return;
        }

        if(size >= mListMaxSize){
            mLogsList.clear();
        }

        mLogsList.add(new LogObject(Message, Note));

        PushLog(Message, Note);
    }

    private static void PushLog(String Message, String Note){
        if(Message == null){
            Message = "Empty";

        } else if(Message.equals("")) {
            Message = "Empty";

        }

        //FirebaseCrash.report(new RuntimeException(Message + " : " + Note));

        //new mSendReportTask("Log", Message, Note, (User.getConnectUser() != null ? User.getConnectUser().mUserName.replaceAll(" ", "-") : "SystemNullUser")).execute();

    }

    private static class LogObject {
        public String mMessage;
        public String mNote;

        public LogObject(String Message, String Note){
            mMessage = Message;
            mNote = Note;

        }

    }

}

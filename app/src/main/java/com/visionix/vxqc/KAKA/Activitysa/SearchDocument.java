package com.visionix.vxqc.KAKA.Activitysa;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.visionix.vxqc.server.objects.Document;
import com.visionix.vxqc.gui.ContactsCompletionView;

import java.util.ArrayList;

public class SearchDocument extends AppCompatActivity {

    private static Context mContext;

    private RelativeLayout          mMainLayout;
    private Button                  mSearchButton;
    private RelativeLayout          mSearchLayout;
    private ContactsCompletionView mSearchEditText;
    private SwipeRefreshLayout      mSwipeRefreshLayout;
    private RecyclerView            mRecyclerView;
    private AppBarLayout            mAppBarLayout;

    private static int mExpandedPosition;

    private static ArrayList<Document> mDocumentsAdapterList = new ArrayList<Document>();
    //private Adapter mAdapter;

    /*

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_document);

        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        mExpandedPosition = -1;

        Install();

        return;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mContext = SearchDocument.this;

        mAppBarLayout.setExpanded(true);

        mDocumentsAdapterList.clear();
        mAdapter.notifyDataSetChanged();

    }

    public void Install() {
        mMainLayout         = (RelativeLayout)          findViewById(R.id.mainLayout);
        mSearchEditText     = (ContactsCompletionView)  findViewById(R.id.editText);
        mSwipeRefreshLayout = (SwipeRefreshLayout)      findViewById(R.id.SwipeRefreshLayout);
        mRecyclerView       = (RecyclerView)            findViewById(R.id.RecyclerView);
        mSearchLayout       = (RelativeLayout)          findViewById(R.id.SearchLayout);
        mSearchButton       = (Button)                  findViewById(R.id.SearchButton);
        mAppBarLayout       = (AppBarLayout)            findViewById(R.id.AppBarLayout);

        ((AppBarLayout) findViewById(R.id.AppBarLayout)).addOnOffsetChangedListener(new OnOffsetChangedAlpha(150, mSearchLayout));

        UiUtiles.setLayoutMaterialColor(this, mMainLayout);

        mSwipeRefreshLayout.setEnabled(false);

        mSearchButton.setTextColor(UiUtiles.getAppTheme().mColorAccent);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchEditText.setError(null);

                mDocumentsAdapterList.clear();
                mAdapter.notifyDataSetChanged();

                mSearchEditText.handleDone();

                ArrayList<SmartSearchObject> objectsList = new ArrayList<SmartSearchObject>(mSearchEditText.getObjects());

                int size = objectsList.size();

                if(size == 0){
                    mSearchEditText.setError("את/ה חייב/ת להזין ערכים לחיפוש");
                    return;
                }

                if(size < 2){
                    mSearchEditText.setError("את/ה חייב/ת להזין לפחות 2 ערכים לחיפוש");
                    return;
                }

                new Search(objectsList).execute();

            }
        });

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mDocumentsAdapterList.size() != 0) {
                    mDocumentsAdapterList.clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mAdapter = new Adapter(getLayoutInflater());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(VXQCApplication.mBaseContext));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    class Search extends AsyncTask<Void, Void, Void>{

        private boolean mFoundDocuments = false;
        private boolean mFoundTooMuch = false;

        private ArrayList<SmartSearchObject> mObjectsList;

        public Search(ArrayList<SmartSearchObject> ObjectsList){
            mObjectsList = ObjectsList;

            return;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mSwipeRefreshLayout.setRefreshing(true);
            mSearchEditText.setEnabled(false);
            mSearchButton.setEnabled(false);

        }

        @Override
        protected Void doInBackground(Void... param) {

            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Keys", getJSONArray(mObjectsList).toDateTimeString()));

            JSONObject jsonObject = VXQCApplication.mServerConnection.makeHttpRequest(URLs.mSearchDocument, params);

            if(jsonObject.has("DocumentsFound")) {
                mFoundDocuments = jsonObject.getBoolean("DocumentsFound");
            } else {
                mFoundDocuments = false;
                return null;
            }

            if(jsonObject.has("FoundTooMuch")) {
                mFoundTooMuch = jsonObject.getBoolean("FoundTooMuch");
            } else {
                mFoundTooMuch = false;
                return null;
            }

            mDocumentsAdapterList.clear();

            if(jsonObject.has("Documents")) {
                JSONArray jsonArray = jsonObject.getJSONArray("Documents");

                for(int i = 0; i < jsonArray.length(); i++){
                    Document documentObject = new Document();
                    JSONObject documentJSONObject = jsonArray.getJSONObject(i);

                    documentObject.mID = documentJSONObject.getInt("ID");
                    documentObject.mTester = documentJSONObject.getString("Tester");
                    try { documentObject.mStartTime = TimeUtiles.mSimpleDateFormat.parse(documentJSONObject.getString("StartTime")); } catch (ParseException e) { e.printStackTrace(); }
                    documentObject.mDocumentSource.mProduct = documentJSONObject.getString("Product");
                    documentObject.mDocumentSource.mType = documentJSONObject.getString("Type");
                    documentObject.mDocumentSource.mRevision = documentJSONObject.getString("Revision");
                    documentObject.mDocumentSource.mImage = VXQCApplication.mSourceList.mDocumentImagesList.get(documentObject.mDocumentSource.mProduct + documentObject.mDocumentSource.mType);

                    if(documentObject.mDocumentSource.mImage == null){
                        documentObject.mDocumentSource.mImage = Image.GetBitmapFromDrawable(VXQCApplication.mBaseContext, R.drawable.imagenotfound);
                    }

                    JSONObject identificationsJSONObject = documentJSONObject.getJSONObject("Identifications");
                    JSONArray names = identificationsJSONObject.names();
                    for(int j = 0; j < names.length(); j++){
                        Identification identification = new Identification();

                        identification.mName = names.getString(j);
                        identification.mValue = identificationsJSONObject.getString(identification.mName);

                        documentObject.mDocumentSource.mIdentificationList.add(identification);
                    }

                    mDocumentsAdapterList.add(documentObject);
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mAppBarLayout.setExpanded(!(mFoundDocuments && !mFoundTooMuch));

            mSwipeRefreshLayout.setRefreshing(false);
            mSearchEditText.setEnabled(true);
            mSearchButton.setEnabled(true);

            mAdapter.notifyDataSetChanged();

            if(!mFoundDocuments){
                Toast.makeText(SearchDocument.this, "לא נמצא מסמך מתאים...", Toast.LENGTH_SHORT).show();

                return;
            }

            if(mFoundTooMuch){
                AlertUIDialog alertUIDialog = new AlertUIDialog(SearchDocument.this, "יותר מידי תוצאות!", "נמצאו יותר מידי מסמכים, נסה למקד את החיפוש...");
                alertUIDialog.show();

                return;
            }

        }

        private JSONArray getJSONArray(ArrayList<SmartSearchObject> ObjectsList){
            JSONArray jsonArray = new JSONArray();

            for(int i = 0; i < ObjectsList.size(); i++){
                try { jsonArray.put(i, ObjectsList.get(i).getContent()); } catch (JSONException e) { }
            }

            return jsonArray;
        }

    }

    class Adapter extends RecyclerView.Adapter<Holder> {

        private LayoutInflater mLayoutInflater;

        public Adapter(LayoutInflater LayoutInflater){
            mLayoutInflater = LayoutInflater;

            return;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = mLayoutInflater.inflate(R.layout.editdocumentsitem, parent, false);

            Holder holder = new Holder(layout);

            holder.mImageView = (CircularImageView) layout.findViewById(R.id.CircularImageView);
            holder.mTitleTextView = (TextView) layout.findViewById(R.id.Title);
            holder.mSubTitleTextView = (TextView) layout.findViewById(R.id.SubTitle);
            holder.mTimeTextView = (TimeTestView) layout.findViewById(R.id.TimeUtiles);
            holder.mExpendRelativeLayout = (RelativeLayout) layout.findViewById(R.id.ExpendRelativeLayout);
            holder.mExpendTextView = (TextView) layout.findViewById(R.id.TextView);

            return holder;
        }

        @Override
        public void onBindViewHolder(final Holder holder, final int position) {
            final Document document = mDocumentsAdapterList.get(position);

            String text = "";
            String firstIdentificationWithValue = "";
            for (Identification identification : document.mDocumentSource.mIdentificationList) {
                if(!identification.mValue.equals("")) {
                    text += identification.mName + " - " + identification.mValue + "\n";

                    if(firstIdentificationWithValue.equals("")){
                        firstIdentificationWithValue = identification.mValue;
                    }
                }
            }

            if(text.equals("")){
                text += "כל הערכים ריקים";
            }

            holder.mTitleTextView.setText(document.mDocumentSource.mProduct + " - " + document.mDocumentSource.mType + " " + firstIdentificationWithValue);
            holder.mSubTitleTextView.setText("נוצר על-ידי " + document.mTester);
            holder.mImageView.setImageBitmap(document.mDocumentSource.mImage);

            holder.mTimeTextView.Start(document.mStartTime.getTime());

            holder.mExpendRelativeLayout.setVisibility(View.GONE);

            holder.mTimeTextView.setTextColor(UiUtiles.getAppTheme().mPrimaryColor);

            Button openButton = (Button) holder.itemView.findViewById(R.id.OpenButton);

            openButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ShowDocument(Integer.toDateTimeString(document.mID)).execute();
                }
            });

            holder.mExpendTextView.setText(text);

            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final int thisPosition = holder.getPosition();

                    if (mExpandedPosition == thisPosition) {
                        notifyItemChanged(thisPosition);

                        mExpandedPosition = -1;
                    } else {
                        notifyItemChanged(mExpandedPosition);

                        mExpandedPosition = thisPosition;

                        notifyItemChanged(mExpandedPosition);

                        mRecyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.smoothScrollToPosition(thisPosition);
                            }
                        });

                    }
                }
            });

            if (position == mExpandedPosition) {
                holder.mExpendRelativeLayout.setVisibility(View.VISIBLE);

            } else {
                holder.mExpendRelativeLayout.setVisibility(View.GONE);

            }

            return;
        }

        @Override
        public int getItemCount() {
            return mDocumentsAdapterList.size();
        }

    }

    class Holder extends RecyclerView.ViewHolder {

        public TextView mTitleTextView;
        public TextView mSubTitleTextView;
        public TimeTestView mTimeTextView;
        public CircularImageView mImageView;

        public TextView mExpendTextView;
        public RelativeLayout mExpendRelativeLayout;

        public Holder(View itemView) {
            super(itemView);

            return;
        }
    }

    static class ShowDocument extends AsyncTask<Void, Void, Void> {

        private ProgressDialog mProgressDialog;

        private String mID;

        private Document mDocument;

        public ShowDocument(String ID){
            mID = ID;

            return;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("מוריד את המסמך");
            mProgressDialog.show();

            return;
        }

        @Override
        protected Void doInBackground(Void... params1) {

            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("ID", mID));

            JSONObject jsonObject = VXQCApplication.mServerConnection.makeHttpRequest(URLs.mGetEditDocument, params);

            mDocument = new Document();

            try {

                jsonObject = jsonObject.getJSONObject("Document");

                mDocument.mDocumentSource.mID = Integer.parseInt(jsonObject.getString("DocumentSourceID"));
                mDocument.mSourceDocumentID = mDocument.mDocumentSource.mID;

                mDocument.mDocumentSource.mProduct = jsonObject.getString("Product");
                mDocument.mDocumentSource.mType = jsonObject.getString("Type");
                mDocument.mDocumentSource.mRevision = jsonObject.getString("Revision");

                mDocument.mID = Integer.parseInt(jsonObject.getString("ID"));
                mDocument.mStartTime = TimeUtiles.mSimpleDateFormat.parse(jsonObject.getString("StartTime"));
                mDocument.mEndTime = TimeUtiles.mSimpleDateFormat.parse(jsonObject.getString("EndTime"));
                mDocument.mTotalTime = jsonObject.getString("TotalTime");
                mDocument.mTester = jsonObject.getString("Tester");

                mDocument.mDocumentJSONArray = jsonObject.getJSONArray("DocumentJSONArray");
                mDocument.mFailuresJSONArray = jsonObject.getJSONArray("FailuresJSONArray");

                mDocument.mFailuresList = new ArrayList<Failure>();
                for(int i = 0; i < mDocument.mFailuresJSONArray.length(); i++){
                    JSONObject failureJSON = mDocument.mFailuresJSONArray.getJSONObject(i);

                    Failure failure = new Failure();

                    failure.mFailureSource.mMain = failureJSON.getString("Main");
                    failure.mFailureSource.mSub = failureJSON.getString("Sub");
                    failure.mFailureSource.mExpl = failureJSON.getString("Expl");

                    failure.mCorrectiveAction = failureJSON.getString("CorrectiveAction");
                    failure.mDescription = failureJSON.getString("Description");
                    failure.mTechnician = failureJSON.getString("Technician");

                    if(failureJSON.has("StartTime")) { failure.mStartTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("StartTime")); }
                    if(failureJSON.has("EndTime")) { failure.mEndTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("EndTime")); }
                    if(failureJSON.has("StartWorkTime")) { failure.mStartWorkTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("StartWorkTime")); }
                    if(failureJSON.has("EndWorkTime")) { failure.mEndWorkTime = TimeUtiles.mSimpleDateFormat.parse(failureJSON.getString("EndWorkTime")); }
                    if(failureJSON.has("TotalTime")) { failure.mCorrectiveAction = failureJSON.getString("TotalTime"); }
                    if(failureJSON.has("TotalWorkTime")) { failure.mCorrectiveAction = failureJSON.getString("TotalWorkTime"); }

                    failure.mApproval = failureJSON.has("Approval") && failureJSON.getBoolean("Approval");

                    mDocument.mFailuresList.add(failure);
                }

                mDocument.mAvailable = jsonObject.getString("Available").equals("1");

                JSONObject identificationsJSONObject = jsonObject.getJSONObject("Identifications");
                JSONArray names = identificationsJSONObject.names();
                for(int j = 0; j < names.length(); j++){
                    Identification identification = new Identification();

                    identification.mName = names.getString(j);
                    identification.mValue = identificationsJSONObject.getString(identification.mName);

                    mDocument.mDocumentSource.mIdentificationList.add(identification);
                }

            } catch (Exception e){
                e.printStackTrace();

                mDocument = null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.cancel();

            if(mDocument == null){
                Toast.makeText(VXQCApplication.mBaseContext, "הורדת המסמך נכשלה", Toast.LENGTH_SHORT).show();
            } else {
                Intent i = new Intent(mContext, DocumentEditorActivity.class);
                DocumentEditorActivity.mDocument = mDocument;
                DocumentEditorActivity.mCanEdit = false;
                DocumentEditorActivity.mNewDocument = false;
                mContext.startActivity(i);
            }

            return;
        }
    }

*/

}

package com.visionix.vxqc.KAKA.Activitysa.model;

public class SmartSearchObject {

    private String mContent;

    public SmartSearchObject(String Content){
        mContent = Content.toUpperCase();
    }

    public String getContent(){
        return mContent;
    }

}

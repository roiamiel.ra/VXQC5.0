package com.visionix.vxqc.KAKA.JSONDocumentParser;

import android.view.View;
import android.view.ViewGroup;

import com.visionix.vxqc.KAKA.Parser.JSONDocumentParser;

/**
 * version 0.91 (beta)
 * author Roi
 */
public class JSONDocumentBuilder extends JSONDocumentParser<View, ViewGroup> {

    /*

    private static final String mStatisticTab = "░";

    private Activity mContext;

    private int mMarkErrorColor = App.mAppColorTheme.mColorAccent;

    public JSONDocumentBuilder(Activity Activity){
        mContext = Activity;

    }

    @Override
    public void setVertical(LayoutView<ViewGroup> Layout) {
        ((LinearLayout) Layout.mObject).setOrientation(LinearLayout.VERTICAL);
    }

    @Override
    public LayoutView createLine(Line Line) {
        ViewGroup lineLayoutViewGroup;

        switch(Line.mLineKind){
            case LINEKIND_LINEAR:
                lineLayoutViewGroup = new LinearLayout(mContext);

                ((LinearLayout) lineLayoutViewGroup).setOrientation(Line.mOrientation == OBJECTORIENTATION_HORIZONTAL ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);

                break;
            case LINEKIND_TABLE:
                lineLayoutViewGroup = new GridLayout(mContext);

                break;
            case LINEKIND_XY:
                lineLayoutViewGroup = new RelativeLayout(mContext);

                break;
            default:
                throw new RuntimeException("LineKind Of Some Line Has Undefine value");
        }

        final int padding = 16;
        if(Line.mPaddingLeft != OBJECT_NODEFINEINTRGERVALUE_CODE){
            lineLayoutViewGroup.setPadding(padding + Line.mPaddingLeft, padding, padding, padding);
        }

        return Line.mViewGroup = new LayoutView(lineLayoutViewGroup);
    }

    @Override
    public void createTextView(Object Object) {
        Object.mObjectView = new ViewObject<>(new TextView(mContext));
        ((TextView) Object.mObjectView.mView).setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoTextView(Object);
    }

    @Override
    public void createCheckBox(Object Object, boolean Editable) {
        Object.mObjectView = new ViewObject<>(new CheckBox(mContext));
        ((CheckBox) Object.mObjectView.mView).setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoCheckBox(Object, Editable);
    }

    @Override
    public void createEditText(Object Object, boolean Editable) {
        Object.mObjectView = new ViewObject<>(new EditText(mContext));
        ((EditText) Object.mObjectView.mView).setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoEditText(Object, Editable);
    }

    @Override
    public void createImageView(Object Object) {
        /*
                Button button = new Button(mContext);
        button.setLayoutParams(GetViewWithParams(Object));

        if(Object.mObjectText != null && !Object.mObjectText.equals(com.jsondocumentparser.Objects.Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
            button.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != com.jsondocumentparser.Objects.Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != com.jsondocumentparser.Objects.Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            button.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD);

        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(mContext);
                dialog.setTitle("Image");
                dialog.setContentView(R.layout.image_dialog);

                ImageView imageView = (ImageView) dialog.findViewById(R.id.imageView);

                Bitmap b = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(b);

                c.drawColor(Color.GREEN);

                imageView.setImageBitmap(b);

                dialog.show();
            }
        });

        return Object.mObjectView = button;

        *
    }

    @Override
    public void addObjectToLayout(Object Object, Line Line) {
        ((ViewGroup) Line.mViewGroup.mObject).addView(((View) Object.mObjectView.mView));
    }

    @Override
    public void addLineToLayout(LayoutView<ViewGroup> Layout, Line Line) {
        Layout.mObject.addView(((ViewGroup) Line.mViewGroup.mObject), Line.mLinePosition);
    }


    //Insert Object Into View

    private synchronized void InsertObjectIntoTextView(Object Object){

        TextView textView = (TextView) Object.mObjectView.mView;

        if(Object.mObjectText != null && !Object.mObjectText.equals(OBJECT_NODEFINESTRINGVALUE_CODE)){
            textView.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != OBJECT_NODEFINEINTRGERVALUE_CODE){
            textView.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != OBJECT_NODEFINEINTRGERVALUE_CODE){
            textView.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            textView.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            textView.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            textView.setTypeface(null, Typeface.BOLD);

        } else {
            textView.setTypeface(null, Typeface.NORMAL);
        }

    }

    private synchronized void InsertObjectIntoCheckBox(final Object Object, final boolean Editable){
        CheckBox checkBox = (CheckBox) Object.mObjectView.mView;

        checkBox.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) checkBox.getLayoutParams();
        layoutParams.bottomMargin += 10;
        checkBox.setLayoutParams(layoutParams);

        if(Object.mObjectText != OBJECT_NODEFINESTRINGVALUE_CODE && Object.mObjectText != null){
            checkBox.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != OBJECT_NODEFINEINTRGERVALUE_CODE){
            checkBox.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != OBJECT_NODEFINEINTRGERVALUE_CODE){
            checkBox.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            checkBox.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            checkBox.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            checkBox.setTypeface(null, Typeface.BOLD);

        } else {
            checkBox.setTypeface(null, Typeface.NORMAL);
        }

        new Handler(Looper.getMainLooper()).post(() -> {
            checkBox.setEnabled(Editable);
            checkBox.setChecked(Object.mBooleanValue);
        });
    }

    public synchronized void InsertObjectIntoEditText(final Object Object, final boolean Editable){
        EditText editText = (EditText) Object.mObjectView.mView;

        editText.setMinWidth(Object.mObjectXSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : 80);

        if(Object.mObjectTextSize != OBJECT_NODEFINEINTRGERVALUE_CODE){
            editText.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != OBJECT_NODEFINEINTRGERVALUE_CODE){
            editText.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            editText.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            editText.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            editText.setTypeface(null, Typeface.BOLD);

        } else {
            editText.setTypeface(null, Typeface.NORMAL);
        }

        if(Object.mTextValue != null){
            editText.setText(Object.mTextValue);

        }

        if(Object.mHint != null){
            editText.setHint(Object.mHint);

        }

        if(Object.mObjectNumbersKeyboard){
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        }

        if(Object.mHaveLimits || Object.mUpperLimit != Object.mLowerLimit){
            TextWatcher textWatcher = new TextWatcher() {
                final EditText ThisEditText = editText;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editText.setError(GetEditTextToleranceExceed(Object, ThisEditText.getText().toDateTimeString()));
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };
            editText.addTextChangedListener(textWatcher);

        }

        new Handler(Looper.getMainLooper()).post(() -> editText.setEnabled(Editable));
    }

    private synchronized String GetEditTextToleranceExceed(Object Object, String Text){
        double value;

        if(Object.mHaveLimits) {
            if(Text == null || Text.equals("")){
                return "שדה זה לא יכול להיות ריק";
            } else {

                try {
                    value = Double.parseDouble(Text);

                } catch (Exception e) {
                    return "ערך לא תקני";
                }

                if (value > Object.mLowerLimit && value > Object.mUpperLimit) {
                    return "הערך גבוהה מהמותר";

                } else if (value < Object.mLowerLimit && value < Object.mUpperLimit) {
                    return "הערך נמוך מהמותר";

                }
            }
        }

        return null;
    }


    //Create View Params

    public synchronized ViewGroup.LayoutParams GetViewWithParams(Object Object){

        switch (Object.mLineKind){
            case LINEKIND_TABLE:
                return GetGridLayoutParams(Object);

            case LINEKIND_XY:
                return GetRelativeLayoutParams(Object);

            case LINEKIND_LINEAR:
                return GetLinearLayoutParams(Object);

            default:
                throw new RuntimeException("LineKind undefine value");
        }
    }

    //Create Layout Params

    private synchronized ViewGroup.LayoutParams GetGridLayoutParams(Object Object){
        GridLayout.LayoutParams gridLayoutLayoutParams = new GridLayout.LayoutParams();

        gridLayoutLayoutParams.height = Object.mObjectYSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : GridLayout .LayoutParams.WRAP_CONTENT;
        gridLayoutLayoutParams.width = Object.mObjectXSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : GridLayout.LayoutParams.WRAP_CONTENT;

        gridLayoutLayoutParams.setMargins(
                Object.mMarginLeft != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginLeft : 0,
                Object.mMarginTop != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginTop : 0,
                Object.mMarginRight != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginRight : 0,
                Object.mMarginBottom != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginBottom : 0
        );

        if(Object.mObjectY == OBJECT_NODEFINEINTRGERVALUE_CODE || Object.mObjectX == OBJECT_NODEFINEINTRGERVALUE_CODE)
            throw new RuntimeException("In Table You Most define x and y");

        gridLayoutLayoutParams.columnSpec = GridLayout.spec(Object.mObjectX);
        gridLayoutLayoutParams.rowSpec = GridLayout.spec(Object.mObjectY);

        return gridLayoutLayoutParams;
    }

    private synchronized ViewGroup.LayoutParams GetRelativeLayoutParams(Object Object){
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(
                (Object.mObjectXSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : ViewGroup.LayoutParams.WRAP_CONTENT),
                (Object.mObjectYSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : ViewGroup.LayoutParams.WRAP_CONTENT));

        relativeLayoutParams.setMargins(
                Object.mObjectX != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectX : 0,
                Object.mObjectY != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectY : 0,
                Object.mMarginRight != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginRight : 0,
                Object.mMarginBottom != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginBottom : 0
        );

        return relativeLayoutParams;
    }

    private synchronized ViewGroup.LayoutParams GetLinearLayoutParams(Object Object){
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                (Object.mObjectXSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : ViewGroup.LayoutParams.WRAP_CONTENT),
                (Object.mObjectYSize != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : ViewGroup.LayoutParams.WRAP_CONTENT));

        linearLayoutParams.setMargins(
                Object.mMarginLeft != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginLeft : 0,
                Object.mMarginTop != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginTop : 0,
                Object.mMarginRight != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginRight : 0,
                Object.mMarginBottom != OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mMarginBottom : 0
        );

        return linearLayoutParams;
    }

    //Get Values

    public synchronized ErrorsAndHashMap UpdateValuesAndGetLegalityFeedback(DocumentStructure DocumentStructure, boolean MarkErrors){
        HashMap<String, Boolean> checkBoxsKeyAndValueList = new HashMap<>();
        HashMap<String, String> editTextsKeyAndValueList = new HashMap<>();
        ArrayList<Error> errorsList = new ArrayList<>();

        checkBoxsKeyAndValueList.clear();
        editTextsKeyAndValueList.clear();
        errorsList.clear();
        for (int i = 0; i < DocumentStructure.mObjectsArrayList.size(); i++) {
            try {

                Object object = DocumentStructure.mObjectsArrayList.get(i);
                String objectName = object.mObejctName;

                if ((object.mObjectKind == OBJECTKIND_CHECKBOX ? checkBoxsKeyAndValueList.get(objectName) : editTextsKeyAndValueList.get(objectName)) != null) {
                    mContext.runOnUiThread(() -> {
                        AlertUIDialog alertUIDialog = new AlertUIDialog(mContext, "משהו נכשל ):", "קיימים שני אובייקטים או יותר עם אותו השם");
                        alertUIDialog.show();
                    });

                    return null;
                }

                if (object.mObjectKind == OBJECTKIND_CHECKBOX) {
                    CheckBox checkBox = (CheckBox) object.mObjectView.mView;
                    boolean value = checkBox.isChecked();
                    if (!value) {
                        if (MarkErrors && !object.mHasErrorMarking) {
                            object.mColorBeforeMarking = checkBox.getCurrentTextColor();
                            object.mHasErrorMarking = true;

                            mContext.runOnUiThread(() -> checkBox.setTextColor(mMarkErrorColor));

                            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                                if (object.mHasErrorMarking && b) {
                                    compoundButton.setTextColor(object.mColorBeforeMarking);
                                    object.mHasErrorMarking = false;
                                    checkBox.setOnCheckedChangeListener(null);
                                }
                            });
                        }

                        Error error = new Error();
                        error.mObject = object;
                        error.mMassage = "לא מסומן";
                        errorsList.add(error);
                    }

                    object.mJSONObject.put(JSONOBJECT_OBJECTBOOLEANVALUE_KEY, value);
                    checkBoxsKeyAndValueList.put(objectName, value);

                } else if (object.mObjectKind == OBJECTKIND_EDITTEXT) {
                    EditText editText = (EditText) object.mObjectView.mView;
                    String value = editText.getText().toDateTimeString();

                    final String errorText = GetEditTextToleranceExceed(object, value);

                    if(MarkErrors){
                        mContext.runOnUiThread(() -> editText.setError(errorText));
                    }

                    if(errorText != null){
                        Error error = new Error();
                        error.mObject = object;
                        error.mMassage = errorText;
                        errorsList.add(error);
                    }

                    object.mJSONObject.put(JSONOBJECT_OBJECTSTRINGVALUE_KEY, value);
                    editTextsKeyAndValueList.put(objectName, value);

                }
            } catch (Exception e) {
                mContext.runOnUiThread(() -> {
                    AlertUIDialog alertUIDialog = new AlertUIDialog(mContext, "משהו נכשל ):", "נכל בהזנת הערכים");
                    alertUIDialog.show();
                });
                return null;
            }
        }

        ErrorsAndHashMap errorsAndHashMap = new ErrorsAndHashMap();
        errorsAndHashMap.mErrorsArrayList = errorsList;
        errorsAndHashMap.mCheckBoxsValuesAndKeyHashMap = checkBoxsKeyAndValueList;
        errorsAndHashMap.mEditTextsValuesAndKeyHashMap = editTextsKeyAndValueList;

        return errorsAndHashMap;
    }

    public synchronized ArrayList<Error> UpdateAllDocumentValuesAndGetLegalityFedback(DocumentStructure DocumentStructure, Document Document, boolean MarkErrors){
        ErrorsAndHashMap errorsAndHashMap = UpdateValuesAndGetLegalityFeedback(DocumentStructure, MarkErrors);
        ArrayList<Error> errorArrayList = errorsAndHashMap.mErrorsArrayList;
        HashMap<String, Boolean> CheckBoxsValueAndKeyHashMap = errorsAndHashMap.mCheckBoxsValuesAndKeyHashMap;
        HashMap<String, String> EditTextsValueAndKeyHashMap = errorsAndHashMap.mEditTextsValuesAndKeyHashMap;

        String topList = "", lowList = "";

        Document.clearHMCheckBoxs();
        Document.clearHMEditTexts();
        Document.clearQALCheckBoxs();
        Document.clearQALEditTexts();

        ArrayList<String> checkBoxsKeys = new ArrayList<>(CheckBoxsValueAndKeyHashMap.keySet());
        for (String key : checkBoxsKeys) {
            String value = Boolean.toDateTimeString(CheckBoxsValueAndKeyHashMap.get(key));
            Document.appendHMCheckBoxs(key + "\n" + value + "\n");
            topList += key + mStatisticTab;
            lowList += value + mStatisticTab;
        }
        Document.appendQALCheckBoxs(topList + "\n" + lowList);

        topList = ""; lowList = "";
        ArrayList<String> editTextsKeys = new ArrayList<>(EditTextsValueAndKeyHashMap.keySet());
        for (String key : editTextsKeys) {
            String value = EditTextsValueAndKeyHashMap.get(key);
            Document.appendHMEditTexts(key + "\n" + value + "\n");
            topList += key + mStatisticTab;
            lowList += value + mStatisticTab;
        }
        Document.appendQALEditTexts(topList + "\n" + lowList);

        return errorArrayList;
    }

    public class ErrorsAndHashMap {
        public ArrayList<Error> mErrorsArrayList;
        public HashMap<String, Boolean> mCheckBoxsValuesAndKeyHashMap;
        public HashMap<String, String> mEditTextsValuesAndKeyHashMap;

    }

    */

}

package com.visionix.vxqc.KAKA.Parser.Class;

public class LayoutView <E> {
    public E mObject;

    public LayoutView(){

    }

    public LayoutView(E Object){
        setObject(Object);
    }

    public void setObject(E Object) {
        this.mObject = Object;
    }
}

package com.visionix.vxqc.KAKA.Parser.Class;

public class ViewObject<E> {
    public E mView;

    public ViewObject(){

    }

    public ViewObject(E View){
        setView(View);
    }

    public void setView(E View) {
        this.mView = View;
    }
}

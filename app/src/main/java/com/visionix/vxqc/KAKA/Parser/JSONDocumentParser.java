package com.visionix.vxqc.KAKA.Parser;

public abstract class JSONDocumentParser<View, Layout> {

    /*

    private LinkedHashMap<String, Integer> mObjectsNames;

    public JSONDocumentParser(){
        mObjectsNames = new LinkedHashMap<String, Integer>();
    }

    public LinkedHashMap<String, Integer> getObjectsNamesHashMap() {
        return mObjectsNames;
    }

    public synchronized DocumentStructure buildDocumentStructure(JSONArray JSONArray, String ProductName) {
        ArrayList<Line> linesArrayList = new ArrayList<>();
        ArrayList<Object> objectsArrayList = new ArrayList<>();

        mObjectsNames = new LinkedHashMap<String, Integer>();

        int size = JSONArray.length();

        ArrayList<SortObject> objectSortArray = new ArrayList<SortObject>(size);
        for(int i = 0; i < size; i++){
            JSONObject thisLineJSONObject = JSONArray.getJSONObject(i);
            int lineNumber = thisLineJSONObject.getInt(Line.JSONOBJECT_LINE_OBJECTLINEPOSITIONAUTO_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);

            int thisSize = objectSortArray.size();
            if(lineNumber != thisSize){
                for(int j = thisSize - 1; j < lineNumber; j++){
                    objectSortArray.add(new SortObject(null));
                }
            }

            objectSortArray.add(new SortObject(thisLineJSONObject));

        }

        int objectCounter = 0;
        for(int i = 0; i < objectSortArray.size(); i++){
            JSONObject thisLineJSONObject = objectSortArray.get(i).getmJSONObject();

            if(thisLineJSONObject == null){
                continue;
            }

            Line thisLineObject = new Line();

            String productNameUpperCase = ProductName.toUpperCase();

            boolean foundThisDocumentAsUnVisible = false;
            JSONArray visibleObjectsJSONArray = thisLineJSONObject.getJSONArray(Line.JSONOBJECT_LINE_OBJECTLINENOVISIBLEPRODUCTSLIST_KEY);
            if(visibleObjectsJSONArray != null){
                thisLineObject.mProductsNoVisibleList.clear();
                for (int j = 0; j < visibleObjectsJSONArray.length(); j++) {
                    if(visibleObjectsJSONArray.getString(j).toUpperCase().equals(productNameUpperCase)){
                        foundThisDocumentAsUnVisible = true;
                        break;
                    }
                }

                if(foundThisDocumentAsUnVisible){
                    continue;
                }
            }

            thisLineObject.mLinePosition = linesArrayList.size();
            thisLineObject.mLineKind = thisLineJSONObject.getInt(Line.JSONOBJECT_LINE_LINEKIND_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisLineObject.mPaddingLeft = thisLineJSONObject.getInt(Line.JSONOBJECT_LINE_PADDINGLEFT_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);

            if (thisLineObject.mLineKind == LINEKIND_LINEAR) {
                thisLineObject.mOrientation = thisLineJSONObject.getInt(Line.JSONOBJECT_LINE_OBJECTORIENTATION_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            }

            JSONArray thisLineObjectsJSONArray = thisLineJSONObject.getJSONArray(Line.JSONOBJECT_LINE_OBJECTSARRAY_KEY);

            for (int j = 0; j < thisLineObjectsJSONArray.length(); j++) {
                Object thisObject = createObject(thisLineObjectsJSONArray.getJSONObject(j), thisLineObject, thisLineJSONObject, objectCounter);

                objectsArrayList.add(thisObject);
                thisLineObject.mInLineObjects.add(thisObject);

                objectCounter++;
            }

            linesArrayList.add(thisLineObject);
        }

        DocumentStructure documentStructure = new DocumentStructure();
        documentStructure.mLineArrayList = linesArrayList;
        documentStructure.mObjectsArrayList = objectsArrayList;

        return documentStructure;
    }

    /**
     * Create a New Object
     * @param thisObjectJSONObject Object as JSONObject
     * @param thisLineObject The Line Of The Object
     * @param thisLineJSONObject The JSONObject of Line Of The Object
     * @param objectCounter The Num iD Of the Object
     * @return new Object

    private synchronized Object createObject(JSONObject thisObjectJSONObject, Line thisLineObject, JSONObject thisLineJSONObject, int objectCounter) {
        Object thisObject = new Object();

        //Object Basic Out_Params
        thisObject.mObjectID = objectCounter;
        thisObject.mLineKind = thisLineObject.mLineKind;
        thisObject.mJSONObject = thisObjectJSONObject;

        //Object Basic In_Params
        thisObject.mObjectKind = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTKIND_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);

        //Object Layout Position
        thisObject.mObjectXSize = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTXSIZE_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mObjectYSize = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTYSIZE_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginLeft = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTMARGINLEFT_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginRight = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTMARGINREIGHT_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginTop = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTMARGINTOP_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginBottom = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTMARGINBOTTOM_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);

        //Line XY Params
        if (thisObject.mLineKind == LINEKIND_TABLE || thisObject.mLineKind == LINEKIND_XY) {
            thisObject.mObjectX = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTX_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectY = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTY_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);

        }

        //Text Params
        if (thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX || thisObject.mObjectKind == Object.OBJECTKIND_TEXTVIEW || thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT) {
            thisObject.mObjectText = thisObjectJSONObject.getString(Object.JSONOBJECT_OBJECTTEXT_KEY);
            thisObject.mObjectTextSize = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTTEXTSIZE_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectTextColor = thisObjectJSONObject.getInt(Object.JSONOBJECT_OBJECTTEXTCOLOR_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectItalic = thisObjectJSONObject.getBoolean(Object.JSONOBJECT_OBJECTITALIC_KEY, false);
            thisObject.mObjectBold = thisObjectJSONObject.getBoolean(Object.JSONOBJECT_OBJECTBOLD_KEY, false);

        }

        //Boolean Value Params
        if (thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX) {
            thisObject.mBooleanValue = thisObjectJSONObject.getBoolean(Object.JSONOBJECT_OBJECTBOOLEANVALUE_KEY, false);

        }

        //EditText Params
        if (thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT) {
            thisObject.mTextValue = thisObjectJSONObject.getString(Object.JSONOBJECT_OBJECTSTRINGVALUE_KEY, null);
            thisObject.mHint = thisObjectJSONObject.getString(Object.JSONOBJECT_OBJECTHINT_KEY, null);
            thisObject.mUpperLimit = thisObjectJSONObject.getDouble(Object.JSONOBJECT_OBJECTUPPERKIMIT_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mLowerLimit = thisObjectJSONObject.getDouble(Object.JSONOBJECT_OBJECTLOWERLIMIT_KEY, OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mHaveLimits = thisObjectJSONObject.getBoolean(Object.JSONOBJECT_OBJECTHAVELIMITS_KEY, false);
            thisObject.mObjectNumbersKeyboard = thisObjectJSONObject.getBoolean(Object.JSONOBJECT_OBJECTNUMBERKEYBOARD_KEY, false);

            if(thisObject.mHint != null && thisObject.mObjectXSize == OBJECT_NODEFINEINTRGERVALUE_CODE){
                thisObject.mObjectXSize = 150;
            }

        }

        //Object ID
        if (thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT || thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX) {
            thisObject.mObejctName = thisObjectJSONObject.getString(Object.JSONOBJECT_OBJECTNAME_KEY);

            if(thisObject.mObejctName == null || thisObject.mObejctName.equals("")) {

                if (thisObject.mObjectText != Object.OBJECT_NODEFINESTRINGVALUE_CODE && thisObject.mObjectText != null) {
                    int length = thisObject.mObjectText.length();
                    thisObject.mObejctName = thisObject.mObjectText.substring(0, (length < 25 ? length : 25)) + "...";

                } else if (thisObject.mHint != Object.OBJECT_NODEFINESTRINGVALUE_CODE && thisObject.mHint != null) {
                    int length = thisObject.mHint.length();
                    thisObject.mObejctName = thisObject.mHint.substring(0, (length < 25 ? length : 25)) + "...";

                } else {
                    thisObject.mObejctName = "Object Number: " + Integer.toDateTimeString(objectCounter) + " Nameless";

                }
            }

            Integer has = mObjectsNames.get(thisObject.mObejctName);
            if(has != null){
                has++;
                mObjectsNames.put(thisObject.mObejctName, has);
                thisObject.mObejctName += " - " + Integer.toDateTimeString(has);
                thisObject.mJSONObject.put(Object.JSONOBJECT_OBJECTNAME_KEY, thisObject.mObejctName);
            }

            thisObject.mObejctName = thisObject.mObejctName.replace("\n", " ");

            mObjectsNames.put(thisObject.mObejctName, 0);
        }

        //ImageView Params
        if (thisObject.mObjectKind == Object.OBJECTKIND_IMAGEVIEW) {
            thisObject.mObjectImageBase64 = thisLineJSONObject.getString(Object.JSONOBJECT_OBJECTIMAGEBASE64_KEY);
        }

        return thisObject;
    }

    public synchronized void setLinesToLayout(final DocumentStructure DocumentStructure, LayoutView<Layout> LayoutView){
        setLinesToLayout(DocumentStructure, LayoutView, true);
    }

    public synchronized void setLinesToLayout(final DocumentStructure DocumentStructure, LayoutView<Layout> LayoutView, boolean Editable){
        setVertical(LayoutView);

        for(int i = 0; i < DocumentStructure.mLineArrayList.size(); i++){
            Line line = DocumentStructure.mLineArrayList.get(i);

            if(line == null){
                continue;
            }

            final com.visionix.vxqc.KAKA.Parser.Class.LayoutView layoutView = createLine(line);
            line.mViewGroup = layoutView;

            for(int j = 0; j < line.mInLineObjects.size(); j++){
                Object object = (Object) line.mInLineObjects.get(j);

                if(object == null){
                    continue;
                }

                object.mLineViewGroup = layoutView;

                switch (object.mObjectKind){
                    case Object.OBJECTKIND_TEXTVIEW:
                        createTextView(object);
                        break;

                    case Object.OBJECTKIND_CHECKBOX:
                        createCheckBox(object, Editable);
                        break;

                    case Object.OBJECTKIND_EDITTEXT:
                        createEditText(object, Editable);
                        break;

                    case Object.OBJECTKIND_IMAGEVIEW:
                        createImageView(object);
                        break;

                    default:
                        throw new RuntimeException("ObjectKind of object number: " + j + " In Line Number: " + i + " Has Undefine value");
                }

                addObjectToLayout(object, line);
            }

            addLineToLayout(LayoutView, line);
        }

    }

    public abstract void setVertical(LayoutView<Layout> Layout);

    public abstract LayoutView createLine(Line Line);

    public abstract void createTextView(Object Object);

    public abstract void createCheckBox(Object Object, boolean Editable);

    public abstract void createEditText(Object Object, boolean Editable);

    public abstract void createImageView(Object Object);

    public abstract void addObjectToLayout(Object Object, Line Line);

    public abstract void addLineToLayout(LayoutView<Layout> Layout, Line Line);

    */

}

package com.visionix.vxqc.KAKA.Parser.Objects;

import com.visionix.vxqc.KAKA.Parser.Class.LayoutView;

import java.util.ArrayList;

public class Line<E> {

    public static final int OBJECTORIENTATION_VERTICAL = 1;
    public static final int OBJECTORIENTATION_HORIZONTAL = 2;

    public static final int OBJECTLINEPOSITIONAUTO = -1;

    public static final String JSONOBJECT_LINE_LINEKIND_KEY = "LineKind";
    public static final String JSONOBJECT_LINE_PADDINGLEFT_KEY = "PaddingLeft";
    public static final String JSONOBJECT_LINE_OBJECTSARRAY_KEY = "ObjectsArray";
    public static final String JSONOBJECT_LINE_OBJECTORIENTATION_KEY = "Orientation";
    public static final String JSONOBJECT_LINE_OBJECTLINEPOSITIONAUTO_KEY = "Position";
    public static final String JSONOBJECT_LINE_OBJECTLINENOVISIBLEPRODUCTSLIST_KEY = "ShowOnProductArray";

    public static final int LINEKIND_LINEAR = 1;
    public static final int LINEKIND_TABLE = 2;
    public static final int LINEKIND_XY = 3;

    public LayoutView<E> mViewGroup = null;

    public ArrayList<Object> mInLineObjects = new ArrayList<Object>();

    public int mLineKind = LINEKIND_LINEAR;
    public int mPaddingLeft = 0;
    public int mLinePosition = OBJECTLINEPOSITIONAUTO;

    public ArrayList<String> mProductsNoVisibleList = new ArrayList<String>();

    public int mOrientation = OBJECTORIENTATION_VERTICAL;

    public boolean isThisLineNoVissable(String ProductName){
        for (String s : mProductsNoVisibleList) {
            if(s.equals(ProductName)){
                return true;
            }
        }

        return false;
    }

}
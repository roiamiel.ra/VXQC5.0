package com.visionix.vxqc.KAKA.Tools;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Encoding {

    public static Bitmap DecodeBase64ToBitmap(String Base64String){
        byte[] decodedString = Base64.decode(Base64String, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static String EncodeBitmapTo64Base(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static String EncodeFileTo64Base(File file){
        byte[] bytes;
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            InputStream inputStream = new FileInputStream(file);
            while((bytesRead = inputStream.read(buffer)) != -1){
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }
            bytes = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void WrtieToFile(File File, String Base64) throws IOException {
        if(File.exists()){
            File.delete();
        }

        File.createNewFile();

        FileOutputStream fileOutputStream = new FileOutputStream(File);

        fileOutputStream.write(android.util.Base64.decode(Base64, android.util.Base64.DEFAULT));

        fileOutputStream.flush();
        fileOutputStream.close();

        return;
    }

}

package com.visionix.vxqc.KAKA.Tools.Time;

import android.os.Handler;
import android.os.Looper;

import com.visionix.vxqc.utiles.TimeUtiles;

public abstract class SetTimeOnStartCountDowmTimer {

    private long mTickTime;
    private int mRepetTime;
    private boolean mNonStop = false;
    private int mReaptCounter;

    private boolean mContinue;

    public synchronized final void cancel() {
        mContinue = false;

        return;
    }

    public synchronized final void start(long millisInFuture, long countDownInterval) {
        mRepetTime = (int) (millisInFuture / countDownInterval);
        mTickTime = countDownInterval;

        mNonStop = millisInFuture == -1;

        mReaptCounter = 0;

        mContinue = true;

        Thread timeThraed = new Thread(new Runnable() {
            @Override
            public void run() {
                while((mContinue && mReaptCounter <= mRepetTime || mNonStop)){

                    long startTime = System.currentTimeMillis();

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            onTick();
                        }
                    });

                    mReaptCounter++;

                    TimeUtiles.wait((int) (mTickTime - (System.currentTimeMillis() - startTime)));
                }

                if(mReaptCounter == mRepetTime){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            onFinish();
                        }
                    });
                }
            }
        });
        timeThraed.start();

        return;
    }

    public abstract void onTick();

    public abstract void onFinish();

}

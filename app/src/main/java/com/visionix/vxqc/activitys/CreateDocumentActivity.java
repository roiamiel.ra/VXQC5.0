package com.visionix.vxqc.activitys;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.visionix.vxqc.KAKA.Activitysa.Dialogs.IdentificationDialog;
import com.visionix.vxqc.KAKA.Activitysa.DocumentEditorActivity;
import com.visionix.vxqc.KAKA.Activitysa.Report;
import com.visionix.vxqc.server.objects.Document;
import com.visionix.vxqc.server.objects.DocumentSource;
import com.visionix.vxqc.server.objects.User;
import com.visionix.vxqc.R;
import com.visionix.vxqc.activitys.models.FloatingLayoutActivity;
import com.visionix.vxqc.utiles.TimeUtiles;

import java.util.ArrayList;
import java.util.Arrays;

import static android.widget.AdapterView.OnItemClickListener;

public class CreateDocumentActivity extends FloatingLayoutActivity implements OnItemClickListener {

    //Start activity params
    private static String OpenOnProduct;
    private static String OpenOnType;

    //Views
    private GridView mDocumentsGridView;
    private EditText mSearchEditText;

    //Grid adapter
    private DocumentsGridAdapter mDocumentsGridAdapter;

    //Arrays
    private DocumentSource[] mDocumentSourceArray;

    //Identification dialog
    private IdentificationDialog mIdentificationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(
                R.layout.activity_create_document,
                getString(R.string.create_document_activity_title),
                getString(R.string.create_document_activity_sub_title)
        );

        //Set in animation
        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        //Find views
        this.mDocumentsGridView = findViewById(R.id.gridview);
        this.mSearchEditText = findViewById(R.id.editText);

        //Set listeners
        this.mDocumentsGridView.setOnItemClickListener(this);

        //Set adapter
        this.mDocumentsGridView.setAdapter(
                this.mDocumentsGridAdapter = new DocumentsGridAdapter(
                        this.mDocumentSourceArray =
                                DocumentSource.getDocumentsListArray(),
                        this.mSearchEditText
                )
        );

        //Check if need to open document (via quick create document param)
        if(OpenOnProduct != null && !OpenOnProduct.equals("") &&
                OpenOnType != null && !OpenOnType.equals("")) {

            //Search for the specific quick create doucment
            //Found flag and this object for the loop
            boolean found = false;
            DocumentSource thisDocumentSource;

            for(int i = 0; i < this.mDocumentSourceArray.length && !found; i++) {
                //Get the specifit document source
                thisDocumentSource = this.mDocumentSourceArray[i];

                //Check match
                if(thisDocumentSource.getProduct().equals(OpenOnProduct) && thisDocumentSource.getType().equals(OpenOnType)){
                    //If match change flag and fake a real click on the index
                    found = true;
                    onItemClick(null, null, i, 0);
                }
            }

            //If not found
            if(!found) {
                //Print a message to the user and report
                Toast.makeText(this, R.string.document_not_found, Toast.LENGTH_SHORT).show();
                Report.sendLog("CreateDocumentActivity: error while trying to create document via quick create params," +
                        "the document was not found in the list", null);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

        //Get the click item as doucment source, and install with it a new doucment object
        Document tempNewDocument = new Document(this.mDocumentsGridAdapter.getItem(position));

        //loadURLs the document source in background
        LoadDocumentContent loadDocumentContent = new LoadDocumentContent(tempNewDocument);
        loadDocumentContent.execute();

        //Create the identification dialog
        this.mIdentificationDialog = new IdentificationDialog(
                    this,                                     // Context
                    tempNewDocument,                                 // The doument source
                    true,                         // Is new document
                    true,                                   // Is edit able
                    loadDocumentContent::userReadyToCreateDocument  // When the user done, send ready status to the load class
        );

        //And show it
        this.mIdentificationDialog.show();
    }

    /**
     * Class to load doecument content to a specific doucment source object
     *
     */
    private class LoadDocumentContent extends AsyncTask<Void, Void, Void>{

        //The document to load the content
        private Document mDocument;

        //The progress dialog
        private ProgressDialog mProgressDialog;

        //Flags
        private boolean mSuccessStatus = false;
        private boolean mUserReadyToCreate = false;

        /**
         * Basic constructor
         * @param Document The document to load content
         *
         */
        public LoadDocumentContent(Document Document){
            //Check valid
            if(Document == null) {
                throw new RuntimeException("CreateDocumnentAvtivity -> LoadDocumentContent: error while installing the class, " +
                        "Document param can't be null");
            }

            //Set the object
            this.mDocument = Document;

            //syncTimeInstall the ProgressDialog
            this.mProgressDialog = new ProgressDialog(CreateDocumentActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            //loadURLs the document content
            this.mDocument.getDocumentContent();

            return null;
        }

        @Override
        protected void onPostExecute(Void Void) {
            super.onPostExecute(Void);

            //Cancel the ProgressDialog if needs to
            if(this.mProgressDialog.isShowing()) {
                this.mProgressDialog.cancel();
            }

            //Change the success flag
            this.mSuccessStatus = this.mDocument.isDocumentContentLoaded();

            //Check if faild
            if(!this.mSuccessStatus) {
                //Show a message and send a log
                Toast.makeText(CreateDocumentActivity.this, R.string.faild_to_download, Toast.LENGTH_LONG).show();
                Report.sendLog("CreateDocumentActivity -> LoadDocumentContent: error while loading the document content", null);

                //And finish the activity
                finish();

            } else {
                //Call the create document function
                createTheDocument();
            }
        }

        /**
         * Function to change the user ready status
         * To create the document the user must be ready and the document must be ready
         *
         */
        public void userReadyToCreateDocument () {
            //Change the flag
            this.mUserReadyToCreate = true;

            //If still runing
            if(getStatus() == Status.RUNNING) {
                //Show the ProgressDialog
                this.mProgressDialog.setMessage(getString(R.string.downloading));
                this.mProgressDialog.setCancelable(false);
                this.mProgressDialog.show();
            }

            //Call the create document function
            createTheDocument();
        }

        /**
         * Function to create the document if the user and the document are ready
         *
         */
        private void createTheDocument() {
            //Check if ready to create
            if(this.mUserReadyToCreate && this.mSuccessStatus) {
                //Create the document
                //Update in the recents edits list
                //new Thread(() -> RecentEdits.NewDocumentCreated(TimeUtiles.getTime(), this.mDocument)).start();

                //Set the doument params
                this.mDocument.setTester(User.getConnectUser().getUserName());
                this.mDocument.setStartTime(TimeUtiles.getDate());

                //Start the activity
                startActivity(DocumentEditorActivity.getIntent(
                        CreateDocumentActivity.this,
                        this.mDocument,
                        true,
                        true
                ));
            }
        }

    }

    @Override
    public void finish() {
        if(this.mIdentificationDialog != null) {
            if(this.mIdentificationDialog.isShowing()) {
                this.mIdentificationDialog.cancel();
            }
        }

        super.finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    /**
     * Class to hold a private adapter to the douments grid view list
     *
     */
    private class DocumentsGridAdapter extends BaseAdapter implements TextWatcher {

        //List array holders
        private DocumentSource[]          mDocumentsSourceArray      = null;
        private ArrayList<DocumentSource> mDisplayDocumentsArrayList = null;

        //The edit text to Search specific doucument
        private EditText mSearchEditText = null;

        /**
         * Basic counstroctur
         * @param DocumentsSourceArray The array of all the documents source
         * @param SearchEditText The edit text to Search doumcnet
         */
        public DocumentsGridAdapter(DocumentSource[] DocumentsSourceArray, EditText SearchEditText){
            //Check array list valid
            if(DocumentsSourceArray == null || DocumentsSourceArray.length <= 0) {
                //throw error
                throw new RuntimeException("CreateDocumentActivity -> DocumentsGridAdapter: error while installing the grid adapter," +
                        "the param 'DocumentSource', can't be null or empty");
            }

            //Check search edit text valid
            if(SearchEditText == null) {
                //throw error
                throw new RuntimeException("CreateDocumentActivity -> DocumentsGridAdapter: error while installing the grid adapter," +
                        "the param 'SearchEditText', can't be null");
            }

            //Set the params
            this.mDocumentsSourceArray = DocumentsSourceArray;
            this.mSearchEditText      = SearchEditText;

            //syncTimeInstall the display objects array
            this.mDisplayDocumentsArrayList = new ArrayList<>(DocumentsSourceArray.length);

            //Set text change listeners
            this.mSearchEditText.addTextChangedListener(this);

            //Update the list
            reloadRealList();
        }

        /**
         * Function to reload the real doucments source list
         *
         */
        public void reloadRealList(){
            //Clear the current list
            this.mDisplayDocumentsArrayList.clear();

            //Append all the documents from the source array
            this.mDisplayDocumentsArrayList.addAll(
                    Arrays.asList(this.mDocumentsSourceArray)
            );

            //Update the list
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return this.mDisplayDocumentsArrayList.size();
        }

        @Override
        public DocumentSource getItem(int i) {
            return this.mDisplayDocumentsArrayList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            //Get the object
            DocumentSource thisDocumentSource = mDisplayDocumentsArrayList.get(i);

            //Get the layout via LayoutInflater (xml file)
            View viewLayout = LayoutInflater.from(CreateDocumentActivity.this).inflate(
                    R.layout.item_create_document,
                    viewGroup,
                    false
            );

            //Dind the views by id
            TextView  documentNameTextView = viewLayout.findViewById(R.id.document_name_textview);
            ImageView documentImageImageView = viewLayout.findViewById(R.id.document_image_imageview);

            //Set the name text and the image
            documentNameTextView.setText(String.format("%s - %s", thisDocumentSource.getProduct(), thisDocumentSource.getType()));

            documentImageImageView.setImageBitmap(thisDocumentSource.getImage());

            return viewLayout;
        }

        public void reloadFilter(){
            //Get the text to filter (in upper case)
            String filterText = this.mSearchEditText.getText().toString().toUpperCase();

            //Check if there is no filter
            if(filterText.length() <= 0) {
                //If there is no, reload the real list
                reloadRealList();

            } else {
                //Apply the filter
                //Clear the display list to append the filter list
                this.mDisplayDocumentsArrayList.clear();

                //Search the specific doucmnet in the source list
                for (DocumentSource documentSource : mDocumentsSourceArray) {
                    //Check if the document match to the filter array
                    if(documentSource.isMatchTo(filterText)) {
                        //If true append it to the display list
                        this.mDisplayDocumentsArrayList.add(documentSource);
                    }
                }

                //Update the grid
                notifyDataSetChanged();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //When the text change, reload the filter
            reloadFilter();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    /**
     * Create a intent to start the class
     * @param Context The context
     * @param QuickOpenProduct the quick open product name
     * @param QuickOpenType the quick open document type
     * @return the intent
     */
    public static Intent getIntent(
            Context Context,
            String QuickOpenProduct,
            String QuickOpenType
    ) {
        //Check valid
        if(Context == null) {
            return null;
        }

        //syncTimeInstall the start activity params
        OpenOnProduct = QuickOpenProduct;
        OpenOnType = QuickOpenType;

        //Return the new activity
        return new Intent(Context, CreateDocumentActivity.class);
    }

    /**
     * Create a intent to start the class
     * @param Context The context
     * @return the intent
     */
    public static Intent getIntent(
            Context Context
    ) {
        //Return the new activity
        return getIntent(Context, null, null);
    }

}

package com.visionix.vxqc.activitys;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.visionix.vxqc.server.objects.User;
import com.visionix.vxqc.R;
import com.visionix.vxqc.activitys.models.FloatingLayoutActivity;
import com.visionix.vxqc.app.VXQCApplication;
import com.visionix.vxqc.utiles.UiUtiles;

/**
 * Activity - the login screen
 * The user will log in to his account
 */
public class LoginActivity extends FloatingLayoutActivity implements TextWatcher, OnClickListener {

    //Shared Preferences
    private static final String LAST_CONNECTED_USER_NAME_TAG = "LastConnectedUserName";

    //View params
    private AutoCompleteTextView mUserNameAutoComplete = null;
    private EditText mPasswordEditText = null;
    private FloatingActionButton mContinueFAB = null;

    private RelativeLayout.LayoutParams mUserNameAutoCompleteLayoutParams = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(
                R.layout.activity_login,
                getString(R.string.login_activity_title),
                getString(R.string.login_activity_sub_title)
        );

        //Find views by id
        this.mUserNameAutoComplete = findViewById(R.id.User_NameAutoComplete);
        this.mPasswordEditText = findViewById(R.id.Password_EditText);
        this.mContinueFAB = findViewById(R.id.Continue_FAB);

        //Set colors
        this.mUserNameAutoComplete.setTextColor(UiUtiles.getAppTheme().mPrimaryText);
        this.mPasswordEditText.setTextColor(UiUtiles.getAppTheme().mPrimaryText);

        this.mUserNameAutoComplete.setHintTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mPasswordEditText.setHintTextColor(UiUtiles.getAppTheme().mSecondaryText);

        this.mContinueFAB.setBackgroundTintList(ColorStateList.valueOf(UiUtiles.getAppTheme().mColorAccent));

        //Set listener
        this.mUserNameAutoComplete.addTextChangedListener(this);
        this.mContinueFAB.setOnClickListener(this);

        //Other options
        this.mPasswordEditText.setVisibility(View.GONE);
        this.mUserNameAutoComplete.setNextFocusDownId(mPasswordEditText.getId());

        this.mUserNameAutoCompleteLayoutParams = (RelativeLayout.LayoutParams) this.mUserNameAutoComplete.getLayoutParams();

        //loadURLs the last connected user name (if needs to)
        loadLastConnectUserName();

        //loadURLs the users names list to the auto complete text
        loadUsersNamesList();
    }

    /**
     * Function to load the last connect user name
     * To save typing time to the user
     */
    private void loadLastConnectUserName() {
        //Get the last connect user name
        String lastConnectedUserName = VXQCApplication.pref().getString(LAST_CONNECTED_USER_NAME_TAG, null);

        //Check valid
        if(lastConnectedUserName == null || lastConnectedUserName.equals("")) {
            return;
        }

        //Check if this user name exists
        if(!User.isUserExists(lastConnectedUserName)){
            return;
        }

        //Set the user name to this user name
        this.mUserNameAutoComplete.setText(lastConnectedUserName);

        //Add a click listener to delete this user name
        this.mUserNameAutoComplete.setOnClickListener(view -> {
            //Delete the listener
            mUserNameAutoComplete.setOnClickListener(null);

            //Clear the text
            mUserNameAutoComplete.setText("");
        });
    }

    /**
     * loadURLs the users names list to the auto complete text
     */
    private void loadUsersNamesList(){
        //Set the threshold param
        this.mUserNameAutoComplete.setThreshold(1);

        //Create and set the list adapter
        this.mUserNameAutoComplete.setAdapter(new ArrayAdapter<>(
                        this,
                        R.layout.support_simple_spinner_dropdown_item,
                        User.getUsersNames()
                )
        );
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Do nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Do nothing
    }

    @Override
    public void afterTextChanged(Editable s) {
        //Check if needs to open or close the password input

        //The text the the user input as user name
        String userName = mUserNameAutoComplete.getText().toString();

        setPasswordInputState(
                !userName.equals("") &&             //The user name not empty
                        User.isUserExists(userName) &&      //The user is exists
                        User.isUserHasPassword(userName)    //The user has a password
        );
    }

    /**
     * Function to open and close the password input place
     *
     * @param State the state (true -> open)
     */
    private void setPasswordInputState(boolean State) {
        //Clear password input
        this.mPasswordEditText.setText("");

        //Change the bottom margins
        this.mUserNameAutoCompleteLayoutParams.bottomMargin = (int) getResources().getDimension(State ? R.dimen.password_bar_open_margins : R.dimen.password_bar_close_margins);

        //Change the visibility
        this.mPasswordEditText.setVisibility(State ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.Continue_FAB:
                //Start login process
                connect();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        //Do nothing
        //To prevent the user exit this activity
    }

    /**
     * Function to connect as a specific user
     */
    private void connect() {
        //Clear errors
        this.mUserNameAutoComplete.setError(null);
        this.mPasswordEditText.setError(null);

        //Get user name and password
        String userName = this.mUserNameAutoComplete.getText().toString();
        String password = this.mPasswordEditText.getText().toString();

        //Check user name valid
        if(userName.equals("")){
            this.mUserNameAutoComplete.setError(getString(R.string.field_cant_stay_empty));

            return;
        }

        //Check password valid
        if(this.mPasswordEditText.getVisibility() == View.VISIBLE && password.equals("")){
            this.mPasswordEditText.setError(getString(R.string.field_cant_stay_empty));

            return;
        }

        if(User.connectAs(userName, password)) {
            //Save the new user name as last connected user
            VXQCApplication.pref()
                    .edit()
                    .putString(LAST_CONNECTED_USER_NAME_TAG, userName)
                    .apply();

            //Start the menu activity
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
            finish();

        } else {
            //Set wrong value error
            this.mUserNameAutoComplete.setError(getString(R.string.worng_field));
        }
    }
}

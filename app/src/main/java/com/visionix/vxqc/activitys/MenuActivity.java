package com.visionix.vxqc.activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.visionix.vxqc.KAKA.Activitysa.EditDocument;
import com.visionix.vxqc.KAKA.Activitysa.Report;
import com.visionix.vxqc.KAKA.Activitysa.SearchDocument;
import com.visionix.vxqc.server.objects.User;
import com.visionix.vxqc.gui.OnOffsetChangedAlpha;
import com.visionix.vxqc.gui.SwipyRefreshLayout.SwipeRefreshLayout;
import com.visionix.vxqc.R;
import com.visionix.vxqc.activitys.models.FloatingLayoutActivity;
import com.visionix.vxqc.utiles.TimeUtiles;
import com.visionix.vxqc.utiles.UiUtiles;

/**
 * Activity - the menu screen
 * The user will have a bunch of options
 *
 */
public class MenuActivity extends FloatingLayoutActivity implements OnClickListener {

    /* Views */
    private Button mCreateNewDocumentButton = null;
    private Button mOpenDocumentButton = null;
    private Button mSearchExistsDocumentButton = null;
    private Button mDisconnectButton = null;
    private Button mWriteLogButton = null;
    private Button mAdminSettingsButton = null;

    private TextView mTitleTextView = null;

    private AppBarLayout mAppBarLayout = null;
    private View mMenuGridView = null;
    private SwipeRefreshLayout mSwipeRefreshLayout = null;

    /* Time intent filter */
    private int mLastUpdateHour = -1;
    private BroadcastReceiver mTimeBroadcastReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(
                R.layout.activity_menu,
                "",
                "",
                false
        );

        //Set input animation
        overridePendingTransition(R.anim.left_in, R.anim.right_out);

        //Find views by ids
        this.mAppBarLayout = findViewById(R.id.AppBarLayout);

        this.mCreateNewDocumentButton = findViewById(R.id.NewDocumentButton);
        this.mOpenDocumentButton = findViewById(R.id.OpenFileButton);
        this.mSearchExistsDocumentButton = findViewById(R.id.ShowDocumentsButton);
        this.mDisconnectButton = findViewById(R.id.ExitButton);
        this.mWriteLogButton = findViewById(R.id.WriteLogButton);
        this.mAdminSettingsButton = findViewById(R.id.SettingsButton);

        this.mTitleTextView = findViewById(R.id.TitleTextView);

        this.mMenuGridView = findViewById(R.id.MenuGridView);
        this.mSwipeRefreshLayout = findViewById(R.id.SwipeRefreshLayout);

        //Set colors
        int primaryTextLighterColor =
                UiUtiles.colorLighter(UiUtiles.getAppTheme().mPrimaryText, 0.25f);

        this.mCreateNewDocumentButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mOpenDocumentButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mDisconnectButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mSearchExistsDocumentButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mWriteLogButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        this.mAdminSettingsButton.setTextColor(UiUtiles.getAppTheme().mSecondaryText);

        this.mCreateNewDocumentButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        this.mOpenDocumentButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        this.mSearchExistsDocumentButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        this.mDisconnectButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        this.mWriteLogButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        this.mAdminSettingsButton.getCompoundDrawables()[1]
                .setColorFilter(primaryTextLighterColor, PorterDuff.Mode.SRC_IN);

        //Set listeners
        this.mAppBarLayout.addOnOffsetChangedListener(
                new OnOffsetChangedAlpha((int) getResources().getDimension(R.dimen.offset_layout_max_point), this.mMenuGridView)
        );

        this.mOpenDocumentButton.setOnClickListener(this);
        this.mCreateNewDocumentButton.setOnClickListener(this);
        this.mWriteLogButton.setOnClickListener(this);
        this.mDisconnectButton.setOnClickListener(this);
        this.mSearchExistsDocumentButton.setOnClickListener(this);

        //Other options
        this.mSwipeRefreshLayout.setEnabled(false);

        //Set the recycler view to the recent created documents manager
        //RecentEdits.setRecyclerView(findViewById(R.id.RecentEditsRecyclerView));

        //syncTimeInstall the welcome message title
        updateTitleWithTime();

        //syncTimeInstall the time intent filter, to the welcome message title
        this.mTimeBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //This method will call when is there a time change event
                //When the time change call to update the welcome title
                updateTitleWithTime();
            }
        };

        //Set the broadcast receiver
        registerReceiver(this.mTimeBroadcastReceiver, new IntentFilter(Intent.ACTION_TIME_CHANGED));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.WriteLogButton:
                startActivity(new Intent(this, Report.class));
                break;

            case R.id.ExitButton:
                finish();
                break;

            case R.id.NewDocumentButton:
                startActivity(CreateDocumentActivity.getIntent(MenuActivity.this));
                break;

            case R.id.OpenFileButton:
                startActivity(new Intent(this, EditDocument.class));
                break;

            case R.id.ShowDocumentsButton:
                startActivity(new Intent(this, SearchDocument.class));
                break;
        }
    }

    /**
     * Update the title welcome message by time
     *
     */
    public void updateTitleWithTime() {
        //Get the current hour
        int hour = TimeUtiles.getDate().getHours();

        //Check if the hour change, if not there will be no change so return
        if(mLastUpdateHour == hour) return;

        this.mLastUpdateHour = hour;

        String text;

        //Find the current hour text
        if(hour <= 11) {
            text = getString(R.string.good_morning);

        } else if(hour <= 18){
            text = getString(R.string.good_after_non);

        } else if(hour <= 20) {
            text = getString(R.string.good_evening);

        } else {
            text = getString(R.string.good_night);
        }

        //Add the user name to the hour text
        text += ", " + User.getConnectUser().getUserName();

        //Change the text in the title
        this.mTitleTextView.setText(text);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Clear the time broadcast receiver
        unregisterReceiver(this.mTimeBroadcastReceiver);
    }

    @Override
    public void finish() {
        startActivity(new Intent(MenuActivity.this, LoginActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.left_out);

        super.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Update the welcome message
        updateTitleWithTime();

        //Update the recents edits list
        new UpdateRecentEditsList().execute();
    }

    /**
     * Class to update the recents edits list
     *
     */
    private class UpdateRecentEditsList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //If there is no items in the list, start the update animation
            /*
            if(RecentEdits.mItemList.size() == 0) {
                mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(true));
            }
            */
            //todo fix
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //Try to update the recents edits list
                //RecentEdits.LoadToList(MenuActivity.this);
                //todo fix
                int i = 0;
            } catch (Exception e) {
                //If there is error, send report
                Report.sendLog("MenuAnctivty, Error while tring to update the RecentEdits list", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void Void) {
            super.onPostExecute(Void);

            //Stop the update animation
            mSwipeRefreshLayout.post(() -> mSwipeRefreshLayout.setRefreshing(false));
        }
    }

}

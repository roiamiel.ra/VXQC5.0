package com.visionix.vxqc.activitys.helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RecentDocumentsHelper {

    /* Finals */
    private static final String RECENT_DOCS_REFRASH_REFERENCE = "RecentDocsRefrash";

    /**
     * Function to send update recent documents request to all devices
     *
     */
    public static void sendRecentDocsRefrashRequest() {
        DatabaseReference recentRefrashReference = FirebaseDatabase.getInstance()
                .getReference(RECENT_DOCS_REFRASH_REFERENCE);

        recentRefrashReference.setValue(true)
                .addOnCompleteListener(task -> {
                    recentRefrashReference.removeValue();
                });
    }

    /*
    public static class RecycleViewAdapter extends RecyclerView.Adapter<RecentDocumentHolder> {

        ArrayList<Item> list;

        public RecycleViewAdapter(ArrayList<Item> List){
            list = List;
        }

        @Override
        public RecentDocumentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(VXQCApplication.mBaseContext).inflate(R.layout.list_row, parent, false);
            holder viewHolder = new holder(view);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(holder holder, int position) {
            final Item item = list.get(position);
            ((SquareImageView) holder.itemView.findViewById(R.id.thumbnail)).setImageBitmap(item.mImage);
            ((TextView) holder.itemView.findViewById(R.id.title)).setText(item.mTitle);

            if(item.mClickable) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, CreateDocumentActivity.class);
                        CreateDocumentActivity.mOpenOnProduct = item.mProduct;
                        CreateDocumentActivity.mOpenOnType = item.mType;

                        mContext.startActivity(intent);
                    }
                });
            }

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

    }

    public static class RecentDocumentHolder extends RecyclerView.ViewHolder {

        public RecentDocumentHolder(View itemView) {
            super(itemView);
        }
    }

    */
}

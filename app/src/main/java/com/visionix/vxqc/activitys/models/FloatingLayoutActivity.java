package com.visionix.vxqc.activitys.models;

import android.annotation.SuppressLint;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

/**
 * Class to hold a basic layout with floating layout
 */
@SuppressLint("Registered")
public class FloatingLayoutActivity extends AppCompatActivity {

    //Layouts
    protected RelativeLayout mMainLayout = null;
    protected RelativeLayout mFloatLayout = null;

    //Views
    protected TextView mTitle = null;
    protected TextView mSubTitle = null;

    @Override
    @Deprecated
    public void setContentView(@LayoutRes int layoutResID) {
        //This function can't be call
        throw new RuntimeException("You can't use setContentView method in FloatingLayoutActivity class");
    }

    /**
     * Function to install the floating layout view
     *
     * @param layoutResID    The id of the layout
     * @param Title          The title of the layout
     * @param SubTitle       The sub title of the layout
     * @param HasFloatLayout If the layout ui have a float layout
     */
    public void setContentView(
            @LayoutRes int layoutResID,
            String Title,
            String SubTitle,
            boolean HasFloatLayout
    ) {
        //Check valid
        if (Title == null || SubTitle == null) {
            return;
        }

        //Set the layout as content view
        super.setContentView(layoutResID);

        //Find views
        this.mMainLayout = findViewById(R.id.MainLayout_RelativeLayout);

        if (HasFloatLayout) {
            this.mFloatLayout = findViewById(R.id.FloatingLayout_RelativeLayout);
        }

        if (HasFloatLayout) {
            this.mTitle = findViewById(R.id.Title_TextView);
            this.mSubTitle = findViewById(R.id.SubTitle_TextView);
        }

        //Set titles
        if (HasFloatLayout) {
            this.mTitle.setText(Title);
            this.mSubTitle.setText(SubTitle);
        }

        //Set background
        UiUtiles.setLayoutMaterialColor(this, this.mMainLayout);

        //Set colors
        if (HasFloatLayout) {
            this.mTitle.setTextColor(UiUtiles.getAppTheme().mPrimaryText);
            this.mSubTitle.setTextColor(UiUtiles.getAppTheme().mSecondaryText);
        }
    }

    /**
     * Function to install the floating layout view
     *
     * @param layoutResID The id of the layout
     * @param Title       The title of the layout
     * @param SubTitle    The sub title of the layout
     */
    public void setContentView(
            @LayoutRes int layoutResID,
            String Title,
            String SubTitle
    ) {
        setContentView(
                layoutResID,
                Title,
                SubTitle,
                false
        );
    }
}

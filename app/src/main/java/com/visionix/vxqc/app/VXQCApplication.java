package com.visionix.vxqc.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;

import com.visionix.vxqc.utiles.UiUtiles;

public class VXQCApplication extends Application {

    //Params
    private static final String SHARED_PREFERENCE_TAG = "VXQCApplication";

    //Self
    public static VXQCApplication mApplication;

    private static SharedPreferences mSharedPreferences;

    private static DisplayMetrics mDisplayMetrics;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplication = this;
        mSharedPreferences = getSharedPreferences(SHARED_PREFERENCE_TAG, MODE_PRIVATE);
        mDisplayMetrics = getResources().getDisplayMetrics();

        //Pick random theme
        UiUtiles.pickRandomTheme();
    }

    public static SharedPreferences pref() {
        return mSharedPreferences;
    }

    public static DisplayMetrics displayMetrics() {
        return mDisplayMetrics;
    }
}

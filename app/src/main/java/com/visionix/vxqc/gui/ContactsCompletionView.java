package com.visionix.vxqc.gui;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visionix.vxqc.KAKA.Activitysa.model.SmartSearchObject;
import com.visionix.vxqc.gui.TokenAutoComplete.TokenCompleteTextView;
import com.visionix.vxqc.R;
import com.visionix.vxqc.utiles.UiUtiles;

public class ContactsCompletionView extends TokenCompleteTextView {
    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(Object object) {
        SmartSearchObject smartSearchObject = (SmartSearchObject) object;

        Context context = getContext();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        Drawable backgroud = UiUtiles.getDrawable(context, R.drawable.token_background);
        backgroud.setColorFilter(UiUtiles.getAppTheme().mColorAccent, PorterDuff.Mode.SRC_IN);

        TextView textView = (TextView) layoutInflater.inflate(R.layout.contact_token, (ViewGroup) ContactsCompletionView.this.getParent(), false);
        textView.setText(smartSearchObject.getContent());
        textView.setBackground(backgroud);

        return textView;
    }

    @Override
    protected Object defaultObject(String completionText) {
        return new SmartSearchObject(completionText);
    }
}
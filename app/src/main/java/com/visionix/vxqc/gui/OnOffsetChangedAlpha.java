package com.visionix.vxqc.gui;

import android.support.design.widget.AppBarLayout;
import android.view.View;

public class OnOffsetChangedAlpha implements AppBarLayout.OnOffsetChangedListener {

    private int mHigh;
    private View mChildView;

    private float mOneUnit;

    public OnOffsetChangedAlpha(int High, View ChildView){
        mHigh = Math.abs(High);
        mChildView = ChildView;

        mOneUnit = (1.0f / mHigh);

        return;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        verticalOffset = Math.abs(verticalOffset);

        if(verticalOffset == 0){
            mChildView.setAlpha(1.0f);

        } else if(verticalOffset == mHigh) {
            mChildView.setAlpha(0.0f);

        } else {
            mChildView.setAlpha((mHigh - verticalOffset) * mOneUnit);

        }

        return;
    }
}

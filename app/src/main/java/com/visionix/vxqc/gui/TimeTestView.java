package com.visionix.vxqc.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.visionix.vxqc.KAKA.Tools.Time.SetTimeOnStartCountDowmTimer;
import com.visionix.vxqc.utiles.TimeUtiles;

public class TimeTestView extends TextView {

    private long mFromTime;
    private SetTimeOnStartCountDowmTimer mCountDownTimer = new SetTimeOnStartCountDowmTimer() {
        @Override
        public void onTick() {
            setText(GetTimePass());
        }

        @Override
        public void onFinish() {
            Run();
        }
    };

    public TimeTestView(Context Context) {
        super(Context);

    }

    public TimeTestView(Context Context, AttributeSet AttributeSet) {
        super(Context, AttributeSet);

    }

    public void Stop(){
        if(mCountDownTimer != null){
            mCountDownTimer.cancel();
        }

        return;
    }

    private void Run(){
        UpdateTimeUntilResetAndTick();

        mCountDownTimer.cancel();
        mCountDownTimer.start(mTimeUntilReset, mTickTime);

        setText(GetTimePass());

        return;
    }

    public void Start(long FromTime){
        mFromTime = FromTime;

        Run();

        return;
    }

    private long mTimeUntilReset = 0;
    private long mTickTime = 0;
    private void UpdateTimeUntilResetAndTick(){
        long nowTime = TimeUtiles.getTime();
        long timePass = Math.abs(nowTime - mFromTime);

        if(timePass < 59000){
            mTickTime = 1000;
            mTimeUntilReset = 60000;// - timePass;

        } else if(timePass < 3540000){
            mTickTime = 60000;
            mTimeUntilReset = 3600000;// - timePass;


        } else if(timePass < 82800000){
            mTickTime = 3600000;
            mTimeUntilReset = 86400000;// - timePass;

        } else {
            mTickTime = 86400000;
            mTimeUntilReset = -1;
        }
    }

    public String GetTimePass() {
        long nowTime = TimeUtiles.getTime();

        long timePass = Math.abs(nowTime - mFromTime);

        String timePassText;

        int timeCount;
        long subTime;
        if(timePass < 60000){
            timeCount = 1000;
            subTime = timePass / timeCount;
            if(subTime <= 1){
                timePassText = " שניה";

            } else {
                timePassText = Long.toString(subTime) + " שניות";

            }

        } else if(timePass < 3600000){
            timeCount = 60000;
            subTime = timePass / timeCount;
            if(subTime <= 1){
                timePassText = " דקה";

            } else {
                timePassText = Long.toString(subTime) + " דקות";

            }


        } else if(timePass < 86400000){
            timeCount = 3600000;
            subTime = timePass / timeCount;

            if(subTime <= 1){
                timePassText = " שעה";

            } else {
                timePassText = Long.toString(subTime) + " שעות";

            }

        } else {
            timeCount = 86400000;
            subTime = timePass / timeCount;

            if(subTime <= 1){
                timePassText = " יום";
            } else {
                timePassText = Long.toString(subTime) + " ימים";

            }
        }

        return "נוצר לפני " + timePassText;
    }
}

package com.visionix.vxqc.gui.TokenAutoComplete;

import android.content.res.ColorStateList;
import android.text.style.TextAppearanceSpan;

class HintSpan extends TextAppearanceSpan {
    public HintSpan(String family, int style, int size, ColorStateList color, ColorStateList linkColor) {
        super(family, style, size, color, linkColor);
    }
}

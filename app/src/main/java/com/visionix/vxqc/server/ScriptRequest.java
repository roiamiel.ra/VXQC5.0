package com.visionix.vxqc.server;

import com.visionix.vxqc.server.Scripts.ScriptURL;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public abstract class ScriptRequest<AnsType> {

    private String mURL = null;
    private Map<String, String> mParamsMap = new HashMap<>();

    private OnSuccessListener<AnsType> mOnSuccessListener = null;
    private OnFaildListener<AnsType> mOnFaildListener = null;

    public ScriptRequest(ScriptURL ScriptURL) {
        this.mURL = ScriptURL.getURL();
    }

    public String getURL() {
        return mURL;
    }

    public Map<String, String> getParamsMap() {
        return mParamsMap;
    }

    public ScriptRequest addParam(String Key, String Value) {
        mParamsMap.put(Key, Value);

        return this;
    }

    public ScriptRequest removeParam(String Key) {
        mParamsMap.remove(Key);

        return this;
    }

    public boolean isParamsValid() {
        String[] requiredParameters = getRequiredParameters();

        if(requiredParameters == null) {
            return false;
        }

        for (String parameter : requiredParameters) {
            if(!mParamsMap.containsKey(parameter)) return false;
        }

        return true;
    }

    public ScriptRequest setOnSuccessListener(OnSuccessListener<AnsType> OnSuccessListener) {
        this.mOnSuccessListener = OnSuccessListener;

        return this;
    }

    public ScriptRequest setOnFaildListener(OnFaildListener<AnsType> OnFaildListener) {
        this.mOnFaildListener = OnFaildListener;

        return this;
    }

    public OnSuccessListener getOnSuccessListener() {
        return mOnSuccessListener;
    }

    public OnFaildListener getOnFaildListener() {
        return mOnFaildListener;
    }

    public abstract String[] getRequiredParameters();

    public abstract AnsType getAnswerValue(JSONObject JSONObject);

    public interface OnSuccessListener<AnsType> {
        void onSuccess(JSONObject JSONObject, ScriptRequest<AnsType> ScriptRequest);
    }

    public interface  OnFaildListener<AnsType> {
        void onFaild(int ErrorCode, String ErrorMessage, ScriptRequest<AnsType> ScriptRequest);
    }

}

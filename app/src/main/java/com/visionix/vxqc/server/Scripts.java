package com.visionix.vxqc.server;

import com.visionix.vxqc.BuildConfig;
import com.visionix.vxqc.server.objects.DocumentSource;
import com.visionix.vxqc.server.objects.User;
import com.visionix.vxqc.utiles.JSONUtiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Scripts {

    public enum ScriptURL {
        GetDocumentsSources("documents_source/GetDocumentsSources.php"),
        GetUsers("users/GetUsers.php"),
        SyncTime("time/SyncTime.php"),
        Test("test.php");

        private String mURL;

        ScriptURL(String FileDirectory) {
            this.mURL = "http://" + BuildConfig.SERVER_IP + "/com.visionix.vxqc/" + FileDirectory;
        }

        public String getURL() {
            return mURL;
        }

    }

    /**
     * GetDocumentsSourcesScript
     * ScriptRequest to get all the documents source from the server.
     *
     */
    public static class GetDocumentsSourcesScript extends ScriptRequest<List<DocumentSource>> {

        private static final String ANSWER_DOCUMENTS_LIST_LIST_TAG = "DocumentSourceList";

        public GetDocumentsSourcesScript() {
            super(ScriptURL.GetDocumentsSources);
        }

        @Override
        public String[] getRequiredParameters() {
            return new String[]{}; //No Params;
        }

        @Override
        public List<DocumentSource> getAnswerValue(JSONObject JSONObject) {
            JSONArray documentsSourcesJSONArray = JSONUtiles.getJSONArray(JSONObject, ANSWER_DOCUMENTS_LIST_LIST_TAG);
            if(documentsSourcesJSONArray == null || documentsSourcesJSONArray.length() <= 0) {
                return new ArrayList<>();
            }

            List<DocumentSource> documentsSources = new ArrayList<>(documentsSourcesJSONArray.length());

            for (int i = 0; i < documentsSourcesJSONArray.length(); i++) {
                documentsSources.add(new DocumentSource(JSONUtiles.getJSONObject(documentsSourcesJSONArray, i)));
            }

            return documentsSources;
        }
    }

    /**
     * GetUsersScript
     * ScriptRequest to get all the users (name, password and premisions) from the server.
     *
     */
    public static class GetUsersScript extends ScriptRequest<List<User>> {

        private static final String ANSWER_USERS_LIST_TAG = "UsersList";

        public GetUsersScript() {
            super(ScriptURL.GetUsers);
        }

        @Override
        public String[] getRequiredParameters() {
            return new String[]{}; //No Params;
        }

        @Override
        public List<User> getAnswerValue(JSONObject JSONObject) {
            JSONArray usersJSONArray = JSONUtiles.getJSONArray(JSONObject, ANSWER_USERS_LIST_TAG);
            if(usersJSONArray == null || usersJSONArray.length() <= 0) {
                return new ArrayList<>();
            }

            List<User> usersList = new ArrayList<>(usersJSONArray.length());

            for (int i = 0; i < usersJSONArray.length(); i++) {
                usersList.add(new User(JSONUtiles.getJSONObject(usersJSONArray, i)));
            }

            return usersList;
        }
    }

    /**
     * SyncTimeScript
     * ScriptRequest to sync the device time with the server time,
     * by getting the time on the server and calculate the offset
     *
     */
    public static class SyncTimeScript extends ScriptRequest<Long> {

        private static final String ANSWER_LOCAL_TIME_TAG = "LocalTime";

        public SyncTimeScript() {
            super(ScriptURL.SyncTime);
        }

        @Override
        public String[] getRequiredParameters() {
            return new String[]{}; //No Params
        }

        @Override
        public Long getAnswerValue(JSONObject JSONObject) {
            return JSONUtiles.getLong(JSONObject, ANSWER_LOCAL_TIME_TAG, 0L);
        }
    }

    /**
     * TestScript
     * ScriptRequest to illustrate the request system
     *
     * NOT TO USE!!!!!!
     */
    public static class TestScript extends ScriptRequest<Void> {

        private static final String NAME_PARAM = "Name";
        private static final String AGE_PARAM = "Age";

        public TestScript(String Name, String Age) {
            super(ScriptURL.Test);

            addParam(NAME_PARAM, Name);
            addParam(AGE_PARAM, Age);
        }

        @Override
        public String[] getRequiredParameters() {
            return new String[]{NAME_PARAM, AGE_PARAM};
        }

        @Override
        public Void getAnswerValue(JSONObject JSONObject) {
            return null;
        }
    }

}

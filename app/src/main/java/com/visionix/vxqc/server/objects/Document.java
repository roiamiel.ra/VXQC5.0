package com.visionix.vxqc.server.objects;

import android.graphics.Bitmap;

import com.visionix.vxqc.KAKA.Activitysa.Report;
import com.visionix.vxqc.KAKA.Tools.Encoding;
import com.visionix.vxqc.utiles.JSONUtiles;
import com.visionix.vxqc.utiles.TimeUtiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Object to hold a document
 *
 */
public class Document extends DocumentSource {

    //Tag list
    protected static final String FAILURES_TAG     = "Failures";
    protected static final String FAILURES_QAL_TAG = "QALFailures";

    protected static final String DOCUMENT_ID_TAG = "DocumentID";

    protected static final String START_TIME_TAG = "StartTime";
    protected static final String END_TIME_TAG   = "EndTime";
    protected static final String TOTAL_TIME_TAG = "TotalTime";

    protected static final String TESTER_TAG    = "Tester";
    protected static final String SIGNATURE_TAG = "Signature";

    protected static final String EDIT_TEXT_QAL_TAG = "QALEditText";
    protected static final String CHECK_BOX_QAL_TAG = "QALCheckBox";
    protected static final String EDIT_TEXT_HM_TAG  = "HMEditText";
    protected static final String CHECK_BOX_HM_TAG  = "HMCheckBox";

    //Document id
    private int mDocumentID = -1;

    //TimeUtiles params
    private Date mStartTime   = null;
    private Date mEndTime     = null;
    private String mTotalTime = "";

    //Tester Data
    private String mTester    = "";
    private Bitmap mSignature = null;

    //Failures data
    private ArrayList<Failure> mFailuresList = new ArrayList<>();
    private JSONArray mFailuresJSONArray     = new JSONArray();
    private String mFailuresQAL              = "";

    //Statistic data
    private StringBuilder mQALEditTexts = new StringBuilder();
    private StringBuilder mQALCheckBoxs = new StringBuilder();
    private StringBuilder mHMEditTexts  = new StringBuilder();
    private StringBuilder mHMCheckBoxs  = new StringBuilder();

    /**
     * Basic constructor
     */
    public Document() {
        super();

        //Install the failures json array
        installJSONArrayFailuresList();
    }

    /**
     * Basic constructor from copy element
     * @param DocumentSource document to copy
     */
    public Document(DocumentSource DocumentSource){
        super(DocumentSource);

        //Install the failures json array
        installJSONArrayFailuresList();
    }

    /**
     * Basic constructor by json object
     * @param JSONObject the json object
     */
    public Document(JSONObject JSONObject) {
        super(JSONObject);

        //Install the params by the json object
        installBySONObject();
    }

    /**
     * Add a failure to the list
     * Add to the Array, JSONArray, and QAL
     * @param Failure the Failure to add
     */
    public void appendFailure(Failure Failure) {
        //Check valid
        if(Failure == null || !Failure.isValid()) {
            //If invalid, send a report, and return
            Report.sendLog("Document: error while append an failure, " +
                    "failure arg is null or not valid", null);

            return;
        }

        //Append the object
        this.mFailuresList.add(Failure);
        this.mFailuresJSONArray.put(Failure.getJSONObject());
    }

    /**
     * Return failure from the list
     * @param Index the index of the failure to return
     * @return The failure by the index
     */
    public Failure getFailure(int Index) {
        return this.mFailuresList.get(Index);
    }
    
    /**
     * Return the list of the failures list
     */
    public int getFailureListSize() {
        return this.mFailuresList.size();
    }

    /**
     * Return the json array of the failures
     */
    public JSONArray getFailureJSONArray() {
        return this.mFailuresJSONArray;
    }

    /**
     * Function to install the params by the json object
     */
    private void installBySONObject() {
        //Install params
        this.mDocumentID = JSONUtiles.getInt(this.mJSONObject, DOCUMENT_ID_TAG);

        this.mStartTime = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, START_TIME_TAG));
        this.mEndTime   = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, END_TIME_TAG));
        this.mTotalTime = JSONUtiles.getString(this.mJSONObject, TOTAL_TIME_TAG);

        this.mTester    = JSONUtiles.getString(this.mJSONObject, TESTER_TAG);
        this.mSignature = Encoding.DecodeBase64ToBitmap(JSONUtiles.getString(this.mJSONObject, SIGNATURE_TAG));

        this.mFailuresQAL = JSONUtiles.getString(this.mJSONObject, FAILURES_QAL_TAG);

        this.mQALEditTexts = new StringBuilder(JSONUtiles.getString(this.mJSONObject, EDIT_TEXT_QAL_TAG));
        this.mQALCheckBoxs = new StringBuilder(JSONUtiles.getString(this.mJSONObject, CHECK_BOX_QAL_TAG));
        this.mHMEditTexts  = new StringBuilder(JSONUtiles.getString(this.mJSONObject, EDIT_TEXT_HM_TAG));
        this.mHMCheckBoxs  = new StringBuilder(JSONUtiles.getString(this.mJSONObject, CHECK_BOX_HM_TAG));

        //Install the failures list
        JSONArray failuresJSONArray = JSONUtiles.getJSONArray(this.mJSONObject, FAILURES_TAG);
        if(failuresJSONArray != null) {
            for (int i = 0; i < failuresJSONArray.length(); i++) {
                this.mFailuresList.add(new Failure(JSONUtiles.getJSONObject(failuresJSONArray, i)));
            }
        }
    }

    /**
     * Function to update private param
     */
    public void setDocumentID(int DocumentID) {
        //Update value
        this.mDocumentID = DocumentID;

        //Update json object
        JSONUtiles.put(this.mJSONObject, DOCUMENT_ID_TAG, this.mDocumentID);
    }

    /**
     * Function to update private param
     */
    public void setStartTime(Date StartTime) {
        //Check valid
        if(StartTime == null || StartTime.getTime() == 0L) {
            return;
        }

        //Update value
        this.mStartTime = StartTime;

        //Update json object
        JSONUtiles.put(this.mJSONObject, START_TIME_TAG, TimeUtiles.toDateTimeString(this.mStartTime));
    }

    /**
     * Function to update private param
     */
    public void setEndTime(Date EndTime) {
        //Check valid
        if(EndTime == null || EndTime.getTime() == 0L) {
            return;
        }

        //Update value
        this.mEndTime = EndTime;

        //Update json object
        JSONUtiles.put(this.mJSONObject, END_TIME_TAG, TimeUtiles.toDateTimeString(this.mEndTime));
    }

    /**
     * Function to update private param
     */
    public void setTotalTime(String TotalTime) {
        //Check valid
        if(TotalTime == null || TotalTime.equals("")) {
            return;
        }

        //Update value
        this.mTotalTime = TotalTime;

        //Update json object
        JSONUtiles.put(this.mJSONObject, TOTAL_TIME_TAG, this.mTotalTime);
    }

    /**
     * Function to update private param
     */
    public void setTester(String Tester) {
        //Check valid
        if(Tester == null || Tester.equals("")) {
            return;
        }

        //Update value
        this.mTester = Tester;

        //Update json object
        JSONUtiles.put(this.mJSONObject, TESTER_TAG, this.mTester);
    }

    /**
     * Function to update private param
     */
    public void setSignature(Bitmap Signature) {
        //Check valid
        if(Signature == null) {
            return;
        }

        //Update value
        this.mSignature = Signature;

        //Update json object
        JSONUtiles.put(this.mJSONObject, IMAGE_TAG, Encoding.EncodeBitmapTo64Base(this.mSignature));
    }

    /**
     * Function to update private param
     */
    public void setFailuresQAL(String FailuresQAL) {
        //Check valid
        if(FailuresQAL == null || FailuresQAL.equals("")) {
            return;
        }

        //Update value
        this.mFailuresQAL = FailuresQAL;

        //Update json object
        JSONUtiles.put(this.mJSONObject, FAILURES_QAL_TAG, this.mFailuresQAL);
    }

    /**
     * Function to update private param
     */
    public void appendQALEditTexts(String QALEditTexts) {
        //Check valid
        if(QALEditTexts == null) {
            return;
        }

        //Update value
        this.mQALEditTexts.append(QALEditTexts);

        //Update json object
        JSONUtiles.put(this.mJSONObject, EDIT_TEXT_QAL_TAG, this.mQALEditTexts.toString());
    }

    /**
     * Function to update private param
     */
    public void appendQALCheckBoxs(String QALCheckBoxs) {
        //Check valid
        if(QALCheckBoxs == null) {
            return;
        }

        //Update value
        this.mQALCheckBoxs.append(QALCheckBoxs);

        //Update json object
        JSONUtiles.put(this.mJSONObject, CHECK_BOX_QAL_TAG, this.mQALCheckBoxs.toString());
    }

    /**
     * Function to update private param
     */
    public void appendHMEditTexts(String HMEditTexts) {
        //Check valid
        if(HMEditTexts == null) {
            return;
        }

        //Update value
        this.mHMEditTexts.append(HMEditTexts);

        //Update json object
        JSONUtiles.put(this.mJSONObject, EDIT_TEXT_HM_TAG, this.mHMEditTexts.toString());
    }

    /**
     * Function to update private param
     */
    public void appendHMCheckBoxs(String HMCheckBoxs) {
        //Check valid
        if(HMCheckBoxs == null) {
            return;
        }

        //Update value
        this.mHMCheckBoxs.append(HMCheckBoxs);

        //Update json object
        JSONUtiles.put(this.mJSONObject, CHECK_BOX_HM_TAG, this.mHMCheckBoxs.toString());
    }

    /**
     * Return the value of the param
     */
    public int getDocumentID() {
        return mDocumentID;
    }

    /**
     * Return the value of the param
     */
    public Date getStartTime() {
        return mStartTime;
    }

    /**
     * Return the value of the param
     */
    public Date getEndTime() {
        return mEndTime;
    }

    /**
     * Return the value of the param
     */
    public String getEndTimeStr() {
        return TimeUtiles.toDateTimeString(mEndTime);
    }

    /**
     * Return the value of the param
     */
    public String getStartTimeStr() {
        return TimeUtiles.toDateTimeString(mStartTime);
    }

    /**
     * Return the value of the param
     */
    public String getTotalTime() {
        return mTotalTime;
    }

    /**
     * Return the value of the param
     */
    public String getTester() {
        return mTester;
    }

    /**
     * Return the value of the param
     */
    public Bitmap getSignature() {
        return mSignature;
    }


    /**
     * Return the value of the param
     */
    public String getFailuresQAL() {
        return mFailuresQAL;
    }

    /**
     * Return the value of the param
     */
    public String getHMCheckBoxs() {
        return mHMCheckBoxs.toString();
    }

    /**
     * Return the value of the param
     */
    public String getQALEditTexts() {
        return mQALEditTexts.toString();
    }

    /**
     * Return the value of the param
     */
    public String getQALCheckBoxs() {
        return mQALCheckBoxs.toString();
    }

    /**
     * Return the value of the param
     */
    public String getHMEditTexts() {
        return mHMEditTexts.toString();
    }

    /**
     * Clear local string builder
     */
    public void clearHMCheckBoxs() {
        mHMCheckBoxs.setLength(0);
    }

    /**
     * Clear local string builder
     */
    public void clearQALEditTexts() {
        mQALEditTexts.setLength(0);
    }

    /**
     * Clear local string builder
     */
    public void clearQALCheckBoxs() {
        mQALCheckBoxs.setLength(0);
    }

    /**
     * Clear local string builder
     */
    public void clearHMEditTexts() {
        mHMEditTexts.setLength(0);
    }


    /**
     * Function th add the failure list reference to the json object
     */
    private void installJSONArrayFailuresList() {
        JSONUtiles.put(this.mJSONObject, FAILURES_TAG, this.mFailuresJSONArray);
    }
}

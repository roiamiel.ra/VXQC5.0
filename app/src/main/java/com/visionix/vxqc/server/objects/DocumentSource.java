package com.visionix.vxqc.server.objects;

import android.graphics.Bitmap;

import com.visionix.vxqc.server.ScriptRequest;
import com.visionix.vxqc.server.Scripts;
import com.visionix.vxqc.utiles.JSONUtiles;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Object to hold a document source params
 */
public class DocumentSource {

    //Tag list
    protected static final String ID_TAG = "Id";

    protected static final String PRODUCT_TAG = "Product";
    protected static final String TYPE_TAG = "Type";
    protected static final String REVISION_TAG = "Revision";

    protected static final String IMAGE_TAG = "Image";

    protected static final String PN_TAG = "PN";
    protected static final String DOCUMENT_NAME_TAG = "DocumentName";
    protected static final String PART_NUMBER_TAG = "PartNumber";
    protected static final String DESCRIPTION_TAG = "Description";
    protected static final String PURPOSE_TAG = "Purpose";
    protected static final String DATE_TAG = "UpdateAt";

    protected static final String DOCUMENT_CONTENT_TAG = "DocumentContent";

    protected static final String IDENTIFICATIONS_TAG = "Identifications";

    //Images cash list
    public static final HashMap<String, Bitmap> DocumentsImagesCashList = new HashMap<>();

    //Lists Management
    private static ArrayList<DocumentSource> DocumentsList;
    private static boolean DocumentsListAlreadyLoaded = false;

    //Document source id
    private int mId = -1;
    
    //Basic identification params
    private String mProduct = "";
    private String mType = "";
    private String mRevision = "";

    //Image
    private Bitmap mImage = null;

    //Document identification
    private ArrayList<Identification> mIdentificationList = new ArrayList<>();

    //Secondary document basic params 
    private String mPN = "";
    private String mDocumentName = "";
    private String mPartNumber = "";
    private String mDescription = "";
    private String mPurpose = "";
    private String mDate = "";
    
    //JSONObject
    protected JSONObject mJSONObject = new JSONObject();
    protected JSONArray mDocumentContentJSONArray = null;
    
    /**
     * Basic constructor
     */
    public DocumentSource() {
        //Do nothing
    }

    /**
     * Basic constructor by document source
     */
    public DocumentSource(DocumentSource DocumentSource) {
        //Check valid
        if(DocumentSource == null) {
            throw new RuntimeException("DocumentSource: error while installing the object by document source object, " +
                    "param can't be null");
        }

        //Copy the params
        setProduct(DocumentSource.mProduct);
        setType(DocumentSource.mType);
        setRevision(DocumentSource.mRevision);

        setPN(DocumentSource.mPN);
        setDocumentName(DocumentSource.mDocumentName);
        setPartNumber(DocumentSource.mPartNumber);
        setDescription(DocumentSource.mDescription);
        setPurpose(DocumentSource.mPurpose);
        setDate(DocumentSource.mDate);

        setDocument(DocumentSource.mDocumentContentJSONArray);

        setId(DocumentSource.mId);

        this.mImage = DocumentSource.mImage;
    }
    
    /**
     * Basic constructor by json object
     */
    public DocumentSource(JSONObject JSONObject) {
        //Check valid
        if(JSONObject == null) {
            throw new RuntimeException("DocumentSource: error while installing the object by json object, " +
                    "param can't be null");
        }
        
        //Install the json object
        this.mJSONObject = JSONUtiles.createFrom(JSONObject);

        //Install the object
        installByJSONArray();
    }

    /**
     * Function to update private param
     */
    public void setProduct(String Product) {
        //Check valid
        if(Product == null || Product.equals("")){
            return;
        }

        //Update value
        this.mProduct = Product;
        JSONUtiles.put(this.mJSONObject, PRODUCT_TAG, this.mProduct);
    }

    /**
     * Function to update private param
     */
    public void setType(String Type) {
        //Check valid
        if(Type == null || Type.equals("")){
            return;
        }

        //Update value
        this.mType = Type;
        JSONUtiles.put(this.mJSONObject, TYPE_TAG, this.mType);
    }

    /**
     * Function to update private param
     */
    public void setRevision(String Revision) {
        //Check valid
        if(Revision == null || Revision.equals("")){
            return;
        }

        //Update value
        this.mRevision = Revision;
        JSONUtiles.put(this.mJSONObject, REVISION_TAG, this.mRevision);
    }

    /**
     * Function to update private param
     */
    public void setPN(String PN) {
        //Check valid
        if(PN == null || PN.equals("")){
            return;
        }

        //Update value
        this.mPN = PN;
        JSONUtiles.put(this.mJSONObject, PN_TAG, this.mPN);
    }

    /**
     * Function to update private param
     */
    public void setDocumentName(String DocumentName) {
        //Check valid
        if(DocumentName == null || DocumentName.equals("")){
            return;
        }

        //Update value
        this.mDocumentName = DocumentName;
        JSONUtiles.put(this.mJSONObject, DOCUMENT_NAME_TAG, this.mDocumentName);
    }

    /**
     * Function to update private param
     */
    public void setPartNumber(String PartNumber) {
        //Check valid
        if(PartNumber == null || PartNumber.equals("")){
            return;
        }

        //Update value
        this.mPartNumber = PartNumber;
        JSONUtiles.put(this.mJSONObject, PART_NUMBER_TAG, this.mPartNumber);
    }

    /**
     * Function to update private param
     */
    public void setDescription(String Description) {
        //Check valid
        if(Description == null || Description.equals("")){
            return;
        }

        //Update value
        this.mDescription = Description;
        JSONUtiles.put(this.mJSONObject, DESCRIPTION_TAG, this.mDescription);
    }

    /**
     * Function to update private param
     */
    public void setPurpose(String Purpose) {
        //Check valid
        if(Purpose == null || Purpose.equals("")){
            return;
        }

        //Update value
        this.mPurpose = Purpose;
        JSONUtiles.put(this.mJSONObject, PURPOSE_TAG, this.mPurpose);
    }

    /**
     * Function to update private param
     */
    public void setDate(String Date) {
        //Check valid
        if(Date == null || Date.equals("")){
            return;
        }

        //Update value
        this.mDate = Date;
        JSONUtiles.put(this.mJSONObject, DATE_TAG, this.mDate);
    }

    private void setDocument(JSONArray JSONArray) {
        //Check valid
        if (JSONArray == null) {
            return;
        }

        //Update value
        this.mDocumentContentJSONArray = JSONUtiles.createFrom(JSONArray);
        JSONUtiles.put(this.mJSONObject, DOCUMENT_CONTENT_TAG, this.mDocumentContentJSONArray);
    }

    /**
     * Function to update private param
     */
    public void setId(int Id) {
        //Update value
        this.mId = Id;
        JSONUtiles.put(this.mJSONObject, ID_TAG, this.mId);
    }

    /**
     * Return the value of the param
     */
    public String getDate() {
        return mDate;
    }

    /**
     * Return the value of the param
     */
    public String getProduct() {
        return mProduct;
    }

    /**
     * Return the value of the param
     */
    public String getType() {
        return mType;
    }

    /**
     * Return the value of the param
     */
    public String getRevision() {
        return mRevision;
    }

    /**
     * Return the value of the param
     */
    public Bitmap getImage() {
        return mImage;
    }

    /**
     * Return the value of the param
     */
    public String getPN() {
        return mPN;
    }

    /**
     * Return the value of the param
     */
    public String getDocumentName() {
        return mDocumentName;
    }

    /**
     * Return the value of the param
     */
    public String getPartNumber() {
        return mPartNumber;
    }

    /**
     * Return the value of the param
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Return the value of the param
     */
    public String getPurpose() {
        return mPurpose;
    }

    /**
     * Return the value of the param
     */
    public Identification[] getIdentificationsList() {
        return this.mIdentificationList.toArray(new Identification[this.mIdentificationList.size()]);
    }

    /**
     * Return the value of the param, and load if needs to
     */
    public JSONArray getDocumentContent() {
        //Check if alredy has the document content
        if(this.mDocumentContentJSONArray == null || this.mDocumentContentJSONArray.length() <= 0) {
            //If not load it
            loadDocumentContent();
        }

        return this.mDocumentContentJSONArray;
    }

    /**
     * Function to check if the document content alredy loaded
     * @return true or false
     *
     */
    public boolean isDocumentContentLoaded() {
        return this.mDocumentContentJSONArray != null && this.mDocumentContentJSONArray.length() > 0;
    }

    /**
     * Function to load the document source content from the server
     */
    public void loadDocumentContent() {
        /*
        ArrayList<NameValuePair> requestParams = new ArrayList<>(1);

        //Set the is param
        requestParams.add(
                new BasicNameValuePair(
                        ID_TAG,
                        Integer.toDateTimeString(this.mId)
                )
        );

        //Call the request
        JSONObject jsonObject =
                App.mServerConnection.makeHttpRequest(URLs.mGetDocumentJSONArrayByID, requestParams);

        //Check valid
        if(jsonObject == null || jsonObject.has(DOCUMENT_CONTENT_TAG)){
            //If not valid send a report
            Report.sendLog("DocumentSource: error while tring to load the doucument content from the server.", null);
            this.mDocumentContentJSONArray = null;

        } else {
            //If valid
            this.mDocumentContentJSONArray = JSONUtiles.getJSONArray(jsonObject, DOCUMENT_CONTENT_TAG);
        }

        */
        //todo fix
    }

    /**
     * Return the json object
     */
    public JSONObject getJSONObject() {
        return this.mJSONObject;
    }

    /**
     * Install the object params by the json array
     */
    private void installByJSONArray() {
        //Copy params
        this.mProduct = JSONUtiles.getString(this.mJSONObject, PRODUCT_TAG);
        this.mType = JSONUtiles.getString(this.mJSONObject, TYPE_TAG);
        this.mRevision = JSONUtiles.getString(this.mJSONObject, REVISION_TAG);

        this.mPN = JSONUtiles.getString(this.mJSONObject, PN_TAG);
        this.mDocumentName = JSONUtiles.getString(this.mJSONObject, DOCUMENT_NAME_TAG);
        this.mPartNumber = JSONUtiles.getString(this.mJSONObject, PART_NUMBER_TAG);
        this.mDescription = JSONUtiles.getString(this.mJSONObject, DESCRIPTION_TAG);
        this.mPurpose = JSONUtiles.getString(this.mJSONObject, PURPOSE_TAG);
        this.mDate = JSONUtiles.getString(this.mJSONObject, DATE_TAG)
                .substring(0, 10); //Substring the 'update at' -> just 'date'

        this.mDocumentContentJSONArray = JSONUtiles.getJSONArray(this.mJSONObject, DOCUMENT_CONTENT_TAG);

        //Install the identifications array
        JSONArray identificationsJSONArray = JSONUtiles.getJSONArray(this.mJSONObject, IDENTIFICATIONS_TAG);
        for (int i = 0; i < identificationsJSONArray.length(); i++) {
            this.mIdentificationList.add(new Identification(JSONUtiles.getJSONObject(identificationsJSONArray, i)));
        }

        //Get the image
        this.mImage = getImage(JSONUtiles.getString(this.mJSONObject, IMAGE_TAG));
    }

    /**
     * Return image from the local cash, or from server
     * @param ImageName The name of the image
     *
     */
    public static Bitmap getImage(String ImageName) {
        //Check valid
        if(ImageName == null || ImageName.equals("")) {
            return null;
        }

        Bitmap image = DocumentsImagesCashList.get(ImageName);

        //Check image valid
        if(image == null) {
            //Get the image from the server
            /*
            image = Image.getImageFromServer(
                    URLs.mHtdocsFolder +
                    URLs.mImageFolder +
                    URLs.mSourceDocumentsFolder +
                    ImageName
            );
            */
            //todo fix

            //And add to the cash list
            DocumentsImagesCashList.put(ImageName, image);
        }

        return image;
    }

    /**
     * Check if some string is a fliter of this document source
     * @param FilterText the text to check
     * @return true or false
     *
     */
    public boolean isMatchTo(String FilterText) {
        //Check valid
        if(FilterText == null || FilterText.equals("")) {
            return true;
        }

        //Check all the match params
        boolean checkFlag = this.mProduct.toUpperCase().contains(FilterText);
        checkFlag |= this.mType        .toUpperCase().contains(FilterText);
        checkFlag |= this.mRevision    .toUpperCase().contains(FilterText);
        checkFlag |= this.mDocumentName.toUpperCase().contains(FilterText);
        checkFlag |= this.mPN          .toUpperCase().contains(FilterText);

        return checkFlag;
    }

    /**
     * Function to create userts get request
     *
     */
    public static ScriptRequest createGetDocumentsSourcesRequest() {
        return new Scripts.GetDocumentsSourcesScript()
                .setOnSuccessListener((JSONObject, ScriptRequest) -> {
                    List<DocumentSource> documentSources = ScriptRequest.getAnswerValue(JSONObject);

                    if(DocumentsList == null) {
                        DocumentsList = new ArrayList<>();

                    } else {
                        DocumentsList.clear();
                    }

                    //Check answer valid
                    if(documentSources == null || documentSources.size() <= 0) {
                        return;
                    }

                    DocumentsList.addAll(documentSources);
                })
                .setOnFaildListener((ErrorCode, ErrorMessage, ScriptRequest) -> {
                    throw new RuntimeException("DocumentSource -> createGetDocumentsSourcesRequest: Error while <sendRequest>: ErrorCode: " + Integer.toString(ErrorCode) + " ErrorMessage: " + ErrorMessage);
                });
    }


    //Function to get the doucmnets source list as array
    public static DocumentSource[] getDocumentsListArray() {
        //Convert the array list to array and return it
        return DocumentsList.toArray(new DocumentSource[DocumentsList.size()]);
    }

    /**
     * Class to hold the converter interface
     */
    public static class Converter implements JSONObjectConverter<DocumentSource> {
        @Override
        public DocumentSource convertTo(JSONObject JSONObject) {
            return new DocumentSource(JSONObject);
        }
    }
}

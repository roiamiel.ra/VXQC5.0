package com.visionix.vxqc.server.objects;

import com.visionix.vxqc.KAKA.Activitysa.Report;
import com.visionix.vxqc.utiles.JSONUtiles;
import com.visionix.vxqc.utiles.TimeUtiles;

import org.json.JSONObject;

import java.util.Date;

/**
 * Object to hold a failure
 */
public class Failure extends FailureSource {

    //Tags list
    protected static final String START_TIME_TAG = "StartTime";
    protected static final String END_TIME_TAG = "EndTime";

    protected static final String START_WORK_TIME_TAG = "StartWorkTime";
    protected static final String END_WORK_TIME_TAG = "EndWorkTime";

    protected static final String TOTAL_TIME_TAG = "TotalTime";
    protected static final String TOTAL_WORK_TIME_TAG = "TotalWorkTime";

    protected static final String DESCRIPTION_TAG = "Description";
    protected static final String CORRECTIVE_ACTION_TAG = "CorrectiveAction";
    protected static final String TECHNICIAN_TAG = "Technician";

    protected static final String APPROVAL_TAG = "Approval";

    //Open and close time
    private Date mStartTime = null;
    private Date mEndTime = null;

    //Start and end work time
    private Date mStartWorkTime = null;
    private Date mEndWorkTime = null;

    //Totals times
    private String mTotalTime = "";
    private String mTotalWorkTime = "";

    //Text params
    private String mDescription = "";
    private String mCorrectiveAction = "";
    private String mTechnician = "";

    //Approval status
    private boolean mIsApproval = false;

    /**
     * Basic constructor with params
     */
    public Failure(
            String Main,
            String Sub,
            String Expl
    ) {
        super();

        //Install the start time
        setStartTime(TimeUtiles.getDate());

        //Set params
        setMain(Main);
        setSub(Sub);
        setExpl(Expl);
        
        //Check valid
        if(!isValid()) {
            throw new RuntimeException("Failure: error while install the object, " +
                    "params can't be null or empty");
        }
    }

    /**
     * Basic constructor by JSONObject
     */
    public Failure(JSONObject JSONObject) {
        super(JSONObject);

        //install the values
        installByJSONObject();
    }

    /**
     * Function to update private param
     * @param StartTime The new value (not null or empty)
     */
    public void setStartTime(Date StartTime) {
        //Check valid
        if(StartTime == null || (StartTime.getTime() == 0 && this.mStartTime != null)) {
            return;
        }

        //Update the param
        this.mStartTime = StartTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, START_TIME_TAG, TimeUtiles.toDateTimeString(this.mStartTime));
    }

    /**
     * Function to update private param
     * @param EndTime The new value (not null or empty)
     */
    public void setEndTime(Date EndTime) {
        //Check valid
        if(EndTime == null || (EndTime.getTime() == 0 && this.mEndTime != null)) {
            return;
        }

        //Update the param
        this.mEndTime = EndTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, END_TIME_TAG, TimeUtiles.toDateTimeString(this.mEndTime));
    }

    /**
     * Function to update private param
     * @param StartWorkTime The new value (not null or empty)
     */
    public void setStartWorkTime(Date StartWorkTime) {
        //Check valid
        if(StartWorkTime == null || (StartWorkTime.getTime() == 0 && this.mStartWorkTime != null)) {
            return;
        }

        //Update the param
        this.mStartWorkTime = StartWorkTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, START_WORK_TIME_TAG, TimeUtiles.toDateTimeString(this.mStartWorkTime));
    }

    /**
     * Function to update private param
     * @param EndWorkTime The new value (not null or empty)
     */
    public void setEndWorkTime(Date EndWorkTime) {
        //Check valid
        if(EndWorkTime == null || (EndWorkTime.getTime() == 0 && this.mEndWorkTime != null)) {
            return;
        }

        //Update the param
        this.mEndWorkTime = EndWorkTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, END_WORK_TIME_TAG, TimeUtiles.toDateTimeString(this.mEndWorkTime));
    }

    /**
     * Function to update private param
     * @param TotalTime The new value (not null or empty)
     */
    public void setTotalTime(String TotalTime) {
        //Check valid
        if(TotalTime == null || (TotalTime.equals("") && this.mTotalTime != null)) {
            return;
        }

        //Update the param
        this.mTotalTime = TotalTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, TOTAL_TIME_TAG, this.mTotalTime);
    }

    /**
     * Function to update private param
     * @param TotalWorkTime The new value (not null or empty)
     */
    public void setTotalWorkTime(String TotalWorkTime) {
        //Check valid
        if(TotalWorkTime == null || (TotalWorkTime.equals("") && this.mTotalWorkTime != null)) {
            return;
        }

        //Update the param
        this.mTotalWorkTime = TotalWorkTime;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, TOTAL_WORK_TIME_TAG, this.mTotalWorkTime);
    }

    /**
     * Function to update private param
     * @param Description The new value (not null or empty)
     */
    public void setDescription(String Description) {
        //Check valid
        if(Description == null || (Description.equals("") && this.mDescription != null)) {
            return;
        }

        //Update the param
        this.mDescription = Description;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, DESCRIPTION_TAG, this.mDescription);
    }

    /**
     * Function to update private param
     * @param CorrectiveAction The new value (not null or empty)
     */
    public void setCorrectiveAction(String CorrectiveAction) {
        //Check valid
        if(CorrectiveAction == null || (CorrectiveAction.equals("") && this.mCorrectiveAction != null)) {
            return;
        }

        //Update the param
        this.mCorrectiveAction = CorrectiveAction;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, CORRECTIVE_ACTION_TAG, this.mCorrectiveAction);
    }

    /**
     * Function to update private param
     * @param Technician The new value (not null or empty)
     */
    public void setTechnician(String Technician) {
        //Check valid
        if(Technician == null || (Technician.equals("") && this.mTechnician != null)) {
            return;
        }

        //Update the param
        this.mTechnician = Technician;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, TECHNICIAN_TAG, this.mTechnician);
    }

    /**
     * Function to update private param
     * @param IsApproval The new value (not null or empty)
     */
    public void setIsApproval(boolean IsApproval) {
        //Update the param
        this.mIsApproval = IsApproval;

        //Update the json object
        JSONUtiles.put(this.mJSONObject, APPROVAL_TAG, this.mIsApproval);
    }

    /**
     * Return the value of the param
     */
    public Date getStartTime() {
        return mStartTime;
    }

    /**
     * Return the value of the param
     */
    public Date getEndTime() {
        return mEndTime;
    }

    /**
     * Return the value of the param
     */
    public Date getStartWorkTime() {
        return mStartWorkTime;
    }

    /**
     * Return the value of the param
     */
    public Date getEndWorkTime() {
        return mEndWorkTime;
    }

    /**
     * Return the value of the param
     */
    public String getTotalTime() {
        return mTotalTime;
    }

    /**
     * Return the value of the param
     */
    public String getTotalWorkTime() {
        return mTotalWorkTime;
    }

    /**
     * Return the value of the param
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Return the value of the param
     */
    public String getCorrectiveAction() {
        return mCorrectiveAction;
    }

    /**
     * Return the value of the param
     */
    public String getTechnician() {
        return mTechnician;
    }

    /**
     * Return the value of the param
     */
    public boolean isApproval() {
        return mIsApproval;
    }
    
    /**
     * Get the json object reference
     */
    @Override
    public JSONObject getJSONObject() {
        return this.mJSONObject;
    }
    
    /**
     * Install by JSONObject
     */
    private void installByJSONObject() {
        //Add time params
        this.mStartTime = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, START_TIME_TAG));
        this.mEndTime = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, END_TIME_TAG));
        this.mStartWorkTime = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, START_WORK_TIME_TAG));
        this.mEndWorkTime = TimeUtiles.fromDateTimeString(JSONUtiles.getString(this.mJSONObject, END_WORK_TIME_TAG));

        //Check start time valid
        if(this.mStartTime == null) {
            this.mStartTime = TimeUtiles.getDate();
        }

        //Total times params
        this.mTotalTime = JSONUtiles.getString(this.mJSONObject, TOTAL_TIME_TAG);
        this.mTotalWorkTime = JSONUtiles.getString(this.mJSONObject, TOTAL_WORK_TIME_TAG);

        //Text params
        this.mDescription = JSONUtiles.getString(this.mJSONObject, DESCRIPTION_TAG);
        this.mCorrectiveAction  = JSONUtiles.getString(this.mJSONObject, CORRECTIVE_ACTION_TAG);
        this.mTechnician = JSONUtiles.getString(this.mJSONObject, TECHNICIAN_TAG);

        //approval param
        this.mTotalWorkTime = JSONUtiles.getString(this.mJSONObject, APPROVAL_TAG);

        //Check if the object is valid
        if(!isValid()) {
            //If not throw an error and send report
            Report.sendLog("Failure: error while install the object from json object," +
                    "object are not valid", null);

            throw new RuntimeException("Failure: Error while install the object from json object");
        }
    }
}

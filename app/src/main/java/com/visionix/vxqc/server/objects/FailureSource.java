package com.visionix.vxqc.server.objects;

import com.visionix.vxqc.utiles.JSONUtiles;

import org.json.JSONObject;

/**
 * Class to hold the basic failure params
 * Created by Roi on 1/3/2017.
 */
public class FailureSource extends DynamicObject {

    //Tags list
    protected static final String MAIN_TAG = "Main";
    protected static final String SUB_TAG = "Sub";
    protected static final String EXPL_TAG = "Expl";

    //Basic params
    protected String mMain = "";
    protected String mSub  = "";
    protected String mExpl = "";

    //JSONObject
    protected JSONObject mJSONObject = new JSONObject();

    /**
     * Basic protected constructor
     */
    protected FailureSource() {
        //Do nothing
    }

    /**
     * Basic constructor with params
     */
    public FailureSource(
            String Main,
            String Sub,
            String Expl
    ) {
        //Set params
        setMain(Main);
        setSub(Sub);
        setExpl(Expl);
    }

    /**
     * Basic constructor by json object
     */
    public FailureSource(JSONObject JSONObject) {
        //Check valid
        if(JSONObject == null) {
            throw new RuntimeException("Failure Source: error while installing the class via json object, " +
                    "JSONObject params can't be null");
        }

        this.mJSONObject = JSONUtiles.createFrom(JSONObject);

        //Install the object
        installByJSONObject();

        //Check valid
        if(!isValid()) {
            throw new RuntimeException("Failure Source: error while installing the class via json object, " +
                    "params are not valid");
        }
    }

    /**
     * Function to update private param
     */
    public void setMain(String Main) {
        //Check valid
        if(Main == null || Main.equals("")) {
            return;
        }

        this.mMain = Main;
        JSONUtiles.put(this.mJSONObject, MAIN_TAG, this.mMain);
    }

    /**
     * Function to update private param
     */
    public void setSub(String Sub) {
        //Check valid
        if(Sub == null || Sub.equals("")) {
            return;
        }

        this.mSub = Sub;
        JSONUtiles.put(this.mJSONObject, SUB_TAG, this.mSub);
    }

    /**
     * Function to update private param
     */
    public void setExpl(String Expl) {
        //Check valid
        if(Expl == null || Expl.equals("")) {
            return;
        }

        this.mExpl = Expl;
        JSONUtiles.put(this.mJSONObject, EXPL_TAG, this.mExpl);
    }

    /**
     * Return the value of the param
     */
    public String getMain() {
        return mMain;
    }

    /**
     * Return the value of the param
     */
    public String getSub() {
        return mSub;
    }

    /**
     * Return the value of the param
     */
    public String getExpl() {
        return mExpl;
    }

    /**
     * Return the json object of the object
     */
    public JSONObject getJSONObject() {
        return this.mJSONObject;
    }

    /**
     * Install the object by the json object
     */
    private void installByJSONObject() {
        //Add Basic params
        this.mMain = JSONUtiles.getString(this.mJSONObject, MAIN_TAG);
        this.mSub  = JSONUtiles.getString(this.mJSONObject, SUB_TAG);
        this.mExpl = JSONUtiles.getString(this.mJSONObject, EXPL_TAG);
    }

    /**
     * Check if the basic params are valid
     * @return true or false
     */
    public boolean isValid() {
        return this.mMain != null && !mMain.equals("") &&
                this.mSub  != null && !mSub.equals("")  &&
                this.mExpl != null && !mExpl.equals("");
    }

    /**
     * Class to hold the converter interface
     */
    public static class Converter implements JSONObjectConverter<FailureSource> {
        @Override
        public FailureSource convertTo(JSONObject JSONObject) {
            return new FailureSource(JSONObject);
        }
    }

}

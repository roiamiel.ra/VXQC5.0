package com.visionix.vxqc.server.objects;

import com.visionix.vxqc.utiles.JSONUtiles;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Object to hold a identification params
 */
public class Identification {

    //Tags list
    private static final String NAME_TAG   = "Name";
    private static final String VALUE_TAG  = "Value";
    private static final String UNIQUE_TAG = "Unique";
    
    //The name and value
    private String mName  = "";
    private String mValue = "";

    //The unique flag
    private boolean mUnique = true;

    //JSONObject
    private JSONObject mJSONObject = new JSONObject();
    
    /**
     * Basic constructor
     */
    public Identification() {
        //Do nothing
    }

    /**
     * Basic constructor by json object
     * @param JSONObject the json object to install
     */
    public Identification(JSONObject JSONObject) {
        //Check valid
        if(JSONObject == null) {
            throw new RuntimeException("Identification: error while installing object by json object, " +
                                       "JSONObject can't being null");
        }
    
        //Install the json object
        try {
            this.mJSONObject = new JSONObject(JSONObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Install the object params
        installByJSONObject();

    }
    
    /**
     * Basic constructor by copy
     * @param Identification the object to copy
     */
    public Identification(Identification Identification) {
        //Copy values
        setName(getName());
        setValue(getValue());
        setUnique(isUnique());
    }

    /**
     * Basic constructor by params
     * @param Name the name
     * @param Value the value
     * @param Unique the unique flag
     */
    public Identification (
            String Name,
            String Value,
            boolean Unique) {

        //Set the values
        setName(Name);
        setValue(Value);
        setUnique(Unique);
    }

    /**
     * Set name function
     * @param Name the new name
     */
    public void setName(String Name) {
        //Check valid
        if(Name == null || Name.equals("")){
            return;
        }

        //Set value
        this.mName = Name;
        JSONUtiles.put(this.mJSONObject, NAME_TAG, this.mName);
    }

    /**
     * Set value function
     * @param Value the new value
     */
    public void setValue(String Value) {
        //Check valid
        if(Value == null || Value.equals("")){
            return;
        }

        //Set value
        this.mValue = Value;
        JSONUtiles.put(this.mJSONObject, VALUE_TAG, this.mValue);
    }

    /**
     * Set unique flag function
     * @param Unique the new unique flag
     */
    public void setUnique(boolean Unique) {
        //Set value
        this.mUnique = Unique;
        JSONUtiles.put(this.mJSONObject, UNIQUE_TAG, this.mUnique);
    }

    /**
     * Get name function
     * @return the name
     */
    public String getName() {
        return mName;
    }

    /**
     * Get value function
     * @return the value
     */
    public String getValue() {
        return mValue;
    }

    /**
     * Get unique function
     * @return the unique flag
     */
    public boolean isUnique() {
        return mUnique;
    }


    /**
     * Get the json object reference
     */
    public JSONObject getJSONObject() {
        return this.mJSONObject;
    }
    
    /**
     * Install by JSONObject
     */
    private void installByJSONObject() {
        this.mName = JSONUtiles.getString(this.mJSONObject, NAME_TAG);
        this.mValue = JSONUtiles.getString(this.mJSONObject, VALUE_TAG);
        this.mUnique = JSONUtiles.getBoolean(this.mJSONObject, UNIQUE_TAG);
    }
}

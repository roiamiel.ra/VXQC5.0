package com.visionix.vxqc.server.objects;

import com.visionix.vxqc.server.ScriptRequest;
import com.visionix.vxqc.server.Scripts;
import com.visionix.vxqc.utiles.JSONUtiles;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Object to hold a basic user params
 */
public class User {

    private static User ConnectUser = null;

    //Tag list
    private static final String USER_NAME_TAG = "UserName";
    private static final String PASSWORD_TAG  = "Password";

    private static final String TESTER_PERMISSION_TAG     = "TesterPermission";
    private static final String TECHNICIAN_PERMISSION_TAG = "TechnicianPermission";
    private static final String MANAGER_PERMISSION_TAG    = "ManagerPermission";

    //Lists Management
    private static HashMap<String, User> UsersList;         //<UserName, User> list

    //Personal information params
    private String mUserName = "";
    private String mPassword = "";

    //User Permissions
    private boolean mTesterPermission     = false;
    private boolean mTechnicianPermission = false;
    private boolean mManagerPermission    = false;

    /**
     * Basic constructor with user name
     */
    public User(String UserName) {
        //Set the user name
        setUserName(UserName);
    }

    /**
     * Basic constructor with user name and password
     */
    public User(String UserName, String Password) {
        //Set the user name and password
        setUserName(UserName);
        setPassword(Password);
    }

    /**
     * Basic constructor by json object
     */
    public User(JSONObject JSONObject) {
        //Check valid
        if(JSONObject == null) {
            throw new RuntimeException("User: error while installing the object by json object, " +
                    "JSONObject can't be null");
        }

        //Install the params by the json object
        setUserName(JSONUtiles.getString(JSONObject, USER_NAME_TAG));
        setPassword(JSONUtiles.getString(JSONObject, PASSWORD_TAG));

        setTesterPermission(JSONUtiles.getBoolean(JSONObject, TESTER_PERMISSION_TAG));
        setTechnicianPermission(JSONUtiles.getBoolean(JSONObject, TECHNICIAN_PERMISSION_TAG));
        setManagerPermission(JSONUtiles.getBoolean(JSONObject, MANAGER_PERMISSION_TAG));
    }

    /**
     * Function to update private param
     */
    public void setUserName(String UserName) {
        this.mUserName = UserName;
    }

    /**
     * Function to update private param
     */
    public void setPassword(String Password) {
        this.mPassword = Password;
    }

    /**
     * Function to update private param
     */
    public void setTesterPermission(boolean TesterPermission) {
        this.mTesterPermission = TesterPermission;
    }

    /**
     * Function to update private param
     */
    public void setTechnicianPermission(boolean TechnicianPermission) {
        this.mTechnicianPermission = TechnicianPermission;
    }

    /**
     * Function to update private param
     */
    public void setManagerPermission(boolean ManagerPermission) {
        this.mManagerPermission = ManagerPermission;
    }

    /**
     * Return the value of the param
     */
    public String getUserName() {
        return mUserName;
    }

    /**
     * Return the value of the param
     */
    public String getPassword() {
        return mPassword;
    }

    /**
     * Return the value of the param
     */
    public boolean isTesterPermission() {
        return mTesterPermission;
    }

    /**
     * Return the value of the param
     */
    public boolean isTechnicianPermission() {
        return mTechnicianPermission;
    }

    /**
     * Return the value of the param
     */
    public boolean isManagerPermission() {
        return mManagerPermission;
    }

    /**
     * Return the has password flag
     */
    public boolean hasPassword() {
        //Check the password param valid
        return this.mPassword != null && !this.mPassword.equals("");
    }

    /* Lists management functions */

    /**
     * Function to create userts get request
     *
     */
    public static ScriptRequest createGetUsersRequest() {
        return new Scripts.GetUsersScript()
                .setOnSuccessListener((JSONObject, ScriptRequest) -> {
                    List<User> usersList = ScriptRequest.getAnswerValue(JSONObject);

                    if(UsersList == null) {
                        UsersList = new HashMap<>();

                    } else {
                        UsersList.clear();
                    }

                    //Check answer valid
                    if(usersList == null || usersList.size() <= 0) {
                        return;
                    }

                    for (User user : usersList) {
                        UsersList.put(user.getUserName(), user);
                    }
                })
                .setOnFaildListener((ErrorCode, ErrorMessage, ScriptRequest) -> {
                    throw new RuntimeException("Users -> createGetUsersRequest: Error while <sendRequest>: ErrorCode: " + Integer.toString(ErrorCode) + " ErrorMessage: " + ErrorMessage);
                });
    }

    public static User getConnectUser() {
        return ConnectUser;
    }

    /**
     * Check if user exists in the list
     * @param UserName The user name to search
     */
    public static boolean isUserExists(String UserName) {
        //Check valid
        if(UserName == null || UserName.equals("")) {
            return false;
        }

        //Check if the user exists in the hm list
        return UsersList.get(UserName) != null;
    }

    /**
     * Check if some user has a password
     * @param UserName The user to check
     */
    public static boolean isUserHasPassword(String UserName) {
        //Check valid
        if(UserName == null || UserName.equals("")) {
            return false;
        }

        //Find the user
        User user = UsersList.get(UserName);

        //Check user valid
        if(user == null) {
            return false;
        }

        //Return the has password flag
        return user.hasPassword();
    }

    /**
     * Return the users names list
     */
    public static String[] getUsersNames() {
        //Convert the key set to array of strings
        return UsersList.keySet().toArray(new String[UsersList.size()]);
    }

    /**
     * Function to get user by user name and password
     * @param UserName The user name
     * @param Password The user password
     * @return The user if success (null if faild)
     */
    public static User getUser(String UserName, String Password) {
        //Check valid
        if(UserName == null || UserName.equals("")) {
            return null;
        }

        //Get the user from the list
        User user = UsersList.get(UserName);

        //Check user valid
        if(user == null) {
            return null;
        }

        //Check password
        if(user.hasPassword() && (Password == null || !user.getPassword().equals(Password))) {
            return null;
        }

        return user;
    }

    /**
     * Function to connect as user to the system
     * @param UserName The user name
     * @param Password The user password
     * @return success status (true or false)
     */
    public static boolean connectAs(String UserName, String Password){
        User user = getUser(UserName, Password);

        if(user == null) {
            return false;
        }

        //Connect as the user
        ConnectUser = user;

        return true;
    }

    /**
     * Function to disconnect from the connected user
     */
    public static void disconnect() {
        ConnectUser = null;
    }

    /**
     * Class to hold the converter interface
     */
    public static class Converter implements JSONObjectConverter<User> {
        @Override
        public User convertTo(JSONObject JSONObject) {
            return new User(JSONObject);
        }
    }
}

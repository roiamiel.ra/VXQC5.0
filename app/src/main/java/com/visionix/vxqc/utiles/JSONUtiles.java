package com.visionix.vxqc.utiles;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtiles {

    /* Create methods */
    public static JSONObject createFrom(String JSONObject) {
        try {
            return new JSONObject(JSONObject);

        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject createFrom(JSONObject JSONObject) {
        try {
            return new JSONObject(JSONObject.toString());

        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONArray createFrom(JSONArray JSONArray) {
        try {
            return new JSONArray(JSONArray.toString());

        } catch (JSONException e) {
            return null;
        }
    }

    /* Get methods */
    public static String getString(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getString(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static String getString(JSONObject JSONObject, String Key, String DefaultValue) {
        try {
            return JSONObject.getString(Key);

        } catch (JSONException e) {
            return DefaultValue;
        }
    }

    public static Boolean getBoolean(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getBoolean(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static Boolean getBoolean(JSONObject JSONObject, String Key, Boolean DefaultValue) {
        try {
            return JSONObject.getBoolean(Key);

        } catch (JSONException e) {
            return DefaultValue;
        }
    }

    public static Integer getInt(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getInt(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static Integer getInt(JSONObject JSONObject, String Key, Integer DefaultValue) {
        try {
            return JSONObject.getInt(Key);

        } catch (JSONException e) {
            return DefaultValue;
        }
    }

    public static Long getLong(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getLong(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static Long getLong(JSONObject JSONObject, String Key, Long DefaultValue) {
        try {
            return JSONObject.getLong(Key);

        } catch (JSONException e) {
            return DefaultValue;
        }
    }

    public static Double getDouble(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getDouble(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static Double getDouble(JSONObject JSONObject, String Key, Double DefaultValue) {
        try {
            return JSONObject.getDouble(Key);

        } catch (JSONException e) {
            return DefaultValue;
        }
    }

    public static JSONObject getJSONObject(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getJSONObject(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONArray getJSONArray(JSONObject JSONObject, String Key) {
        try {
            return JSONObject.getJSONArray(Key);

        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject getJSONObject(JSONArray JSONArray, int Index) {
        try {
            return JSONArray.getJSONObject(Index);

        } catch (JSONException e) {
            return null;
        }
    }

    /* Put methods */
    public static void put(JSONObject JSONObject, String Key, int Value) {
        try {
            JSONObject.put(Key, Value);

        } catch (JSONException ignored) {

        }
    }

    public static void put(JSONObject JSONObject, String Key, boolean Value) {
        try {
            JSONObject.put(Key, Value);

        } catch (JSONException ignored) {

        }
    }

    public static void put(JSONObject JSONObject, String Key, long Value) {
        try {
            JSONObject.put(Key, Value);

        } catch (JSONException ignored) {

        }
    }

    public static void put(JSONObject JSONObject, String Key, double Value) {
        try {
            JSONObject.put(Key, Value);

        } catch (JSONException ignored) {

        }
    }

    public static void put(JSONObject JSONObject, String Key, Object Value) {
        try {
            JSONObject.put(Key, Value);

        } catch (JSONException ignored) {

        }
    }

}

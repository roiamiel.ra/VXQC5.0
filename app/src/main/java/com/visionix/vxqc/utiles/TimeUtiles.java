package com.visionix.vxqc.utiles;

import com.google.firebase.crash.FirebaseCrash;
import com.visionix.vxqc.server.ScriptRequest;
import com.visionix.vxqc.server.Scripts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class TimeUtiles {

    /* Final params */
    private static final SimpleDateFormat mDateAndTimeFormat = new SimpleDateFormat("y-M-d H:m:s", Locale.getDefault());

    /* Sync time params */
    private static long mTimeOffset = 0;

    /**
     * Function to create sync time request
     * 
     */
    public static ScriptRequest createSyncTimeRequest() {
        return new Scripts.SyncTimeScript()
                .setOnSuccessListener((JSONObject, ScriptRequest) -> {
                    long serverTime = ScriptRequest.getAnswerValue(JSONObject);

                    //Check if error happend
                    if(serverTime == 0) {
                        //No Offset (because the error)
                        mTimeOffset = 0;

                        FirebaseCrash.report(new Exception("TimeUtiles -> syncTime: Server return time = 0"));
                    } else {
                        //Calculate offset
                        mTimeOffset = System.currentTimeMillis() - serverTime;
                    }
                })
                .setOnFaildListener((ErrorCode, ErrorMessage, ScriptRequest) -> {
                    throw new RuntimeException("TimeUtiles -> syncTime: Error while <sendRequest>: ErrorCode: " + Integer.toString(ErrorCode) + " ErrorMessage: " + ErrorMessage);
                });
    }

    /**
     * Function to put delay
     * @param ms the delay time is ms
     *
     */
    public static void wait(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calculate the different between two times
     * @param DateA First time
     * @param DateB Second time
     * @return the different
     */
    public synchronized static String getDateDifferent(Date DateA, Date DateB){
        float different;

        //Check valid
        if(DateA == null || DateB == null){
            //If error the different will be 0
            different = 0;

        } else {
            different = Math.abs(DateB.getTime() - DateA.getTime());

        }

        //TimeUtiles to String
        int H = (int) different / 3600000;
        different = different % 3600000;

        int M = (int) different / 60000;
        different = different % 60000;

        int S = (int) different / 1000;

        return H + ":" + M + ":" + S;
    }

    /**
     * Return the time in ms (now time)
     */
    public static long getTime(){
        return System.currentTimeMillis() + mTimeOffset;
    }

    /**
     * Return the date in string (now date)
     */
    public static String getStringDateTime(){
        return mDateAndTimeFormat.format(getDate());
    }

    /**
     * Return the date (now time)
     */
    public static Date getDate(){
        return new Date(getTime());
    }

    /**
     * Convert date to string by format
     * @param Date the date
     * @return string by format
     *
     */
    public static String toDateTimeString(Date Date) {
        //Check valid
        if(Date == null) {
            return null;
        }

        //Format the date and return as string by format
        return mDateAndTimeFormat.format(Date);
    }

    /**
     * Convert string to date by format
     * @param Date the date in string
     * @return date by format
     *
     */
    public static Date fromDateTimeString(String Date) {
        //Check valid
        if(Date == null || Date.equals("")) {
            return null;
        }

        //Format the date and return as string by format
        try {
            return mDateAndTimeFormat.parse(Date);
        } catch (ParseException e) {
            return null;
        }
    }
}

package com.visionix.vxqc.utiles;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;

import com.visionix.vxqc.app.VXQCApplication;

import java.util.Random;

public class UiUtiles {

    // App Color theme
    private static Theme mApplicationColorsTheme;

    /**
     * Function to check if some process run on the ui thread
     * @return true or false
     */
    public static boolean isRuningOnUiThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    /**
     * Function to create ligher color by factor
     * @param color The basic color
     * @param factor The lighter factor (0 -> 1)
     * @return The new ligher color
     */
    public static int colorLighter(int color, float factor) {
        int red = (int) ((android.graphics.Color.red(color) * (1 - factor) / 255 + factor) * 255);
        int green = (int) ((android.graphics.Color.green(color) * (1 - factor) / 255 + factor) * 255);
        int blue = (int) ((android.graphics.Color.blue(color) * (1 - factor) / 255 + factor) * 255);
        return android.graphics.Color.argb(android.graphics.Color.alpha(color), red, green, blue);
    }

    /**
     * Function to get drawable from the resurces
     * @param Context Context
     * @param Res Res of the drawable
     * @return the drawabe
     */
    public static Drawable getDrawable(Context Context, @DrawableRes int Res) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return Context.getDrawable(Res);

        } else {
            return Context.getResources().getDrawable(Res);
        }
    }

    /**
     * Function to apply the material design color to layout
     * @param Activity The activity
     * @param MainLayout The main layout
     */
    public static void setLayoutMaterialColor(Activity Activity, ViewGroup MainLayout){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity.getWindow().setStatusBarColor(mApplicationColorsTheme.mDarkPrimaryColor);
        }

        if(MainLayout != null) {
            MainLayout.setBackgroundColor(mApplicationColorsTheme.mPrimaryColor);
        }
    }

    /**
     * Convert mils to px units
     * @param Mils mils
     * @return px
     */
    public static float milsToPx(float Mils){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_IN, Mils / 1000, VXQCApplication.displayMetrics());
    }

    /**
     * Convert mm to px units
     * @param Mm mm
     * @return px
     */
    public static float mmToPx(float Mm){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, Mm, VXQCApplication.displayMetrics());
    }

    /**
     * Convert dp to px units
     * @param Dp dp
     * @return px
     */
    public static float dpToPx(int Dp) {
        return Dp * (VXQCApplication.displayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * Convert dpi to px units
     * @param Dpi dpi
     * @return px
     */
    public static float dpiToPx(float Dpi){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Dpi, VXQCApplication.displayMetrics());
    }

    public static Theme getAppTheme() {
        return mApplicationColorsTheme;
    }

    /**
     * Function to get a random color theme
     *
     */
    public static void pickRandomTheme() {
        Theme[] themesList = Theme.values();
        mApplicationColorsTheme = themesList[new Random().nextInt((themesList.length))];
    }

    public enum Theme {
        BLUEGREY_TEAL (0xFF455A64, 0xFF607D8B, 0xFFCFD8DC, 0xFFFFFFFF, 0xFF009688, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        INDIGO_BLUE (0xFF303F9F, 0xFF3F51B5, 0xFFC5CAE9, 0xFFFFFFFF, 0xFF448AFF, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        PINK_BLUE (0xFFC2185B, 0xFFE91E63, 0xFFF8BBD0, 0xFFFFFFFF, 0xFF448AFF, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        GREEN_BLUE (0xFF388E3C, 0xFF4CAF50, 0xFFC8E6C9, 0xFFFFFFFF, 0xFF448AFF, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        TEAL_LIGHTGREEN (0xFF00796B, 0xFF009688, 0xFFB2DFDB, 0xFFFFFFFF, 0xFF8BC34A, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        ORENGE_DEEPORENGE (0xFFF57C00, 0xFF7C4DFF, 0xFF7C4DFF, 0xFFFFFFFF, 0xFFFF5722, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        LIME_LIGHTBLUE (0xFFAFB42B, 0xFFCDDC39, 0xFFF0F4C3, 0xFFFFFFFF, 0xFF03A9F4, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        RED_LIGHTBLUE (0xFFD32F2F, 0xFFF44336, 0xFFFFCDD2, 0xFFFFFFFF, 0xFF03A9F4, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        INDIGO_GREEN (0xFF303F9F, 0xFF3F51B5, 0xFFC5CAE9, 0xFFFFFFFF, 0xFF4CAF50, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        ORENGE_INDIGO (0xFFF57C00, 0xFFFF9800, 0xFFFFE0B2, 0xFFFFFFFF, 0xFF536DFE, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        BLUE_DEEPPURPLE (0xFF1976D2, 0xFF2196F3, 0xFFBBDEFB, 0xFFFFFFFF, 0xFF7C4DFF, 0xFF212121, 0xFF212121, 0xFFB6B6B6),
        RED_DEEPPURPLE (0xFFD32F2F, 0xFFF44336, 0xFFFFCDD2, 0xFFFFFFFF, 0xFF7C4DFF, 0xFF212121, 0xFF212121, 0xFFB6B6B6),
        BLUE_LIGHTGREEN (0xFF1976D2, 0xFF2196F3, 0xFFBBDEFB, 0xFFFFFFFF, 0xFF8BC34A, 0xFF212121, 0xFF727272, 0xFFB6B6B6),
        TEAL_INDIGO (0xFF00796B, 0xFF009688, 0xFFB2DFDB, 0xFFFFFFFF, 0xFF536DFE, 0xFF212121, 0xFF727272, 0xFFB6B6B6);

        public final int mDarkPrimaryColor;
        public final int mPrimaryColor;
        public final int mLightPrimaryColor;
        public final int mText;
        public final int mColorAccent;
        public final int mPrimaryText;
        public final int mSecondaryText;
        public final int mDividerColor;

        Theme(int DarkPrimaryColor,
              int PrimaryColor,
              int LightPrimaryColor,
              int Text,
              int ColorAccent,
              int PrimaryText,
              int SecondaryText,
              int DividerColor){

            this.mDarkPrimaryColor = DarkPrimaryColor;
            this.mPrimaryColor = PrimaryColor;
            this.mLightPrimaryColor = LightPrimaryColor;
            this.mText = Text;
            this.mColorAccent = ColorAccent;
            this.mPrimaryText = PrimaryText;
            this.mSecondaryText = SecondaryText;
            this.mDividerColor = DividerColor;
        }
    }

}

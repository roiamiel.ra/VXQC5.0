package com.visionix.vxqc.utiles;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.visionix.vxqc.server.ScriptRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VolleyUtiles {

    /* Answers Tags */
    public static final String ANSWER_CODE_TAG = "AnswerCode";
    public static final String ANSWER_MESSAGE_TAG = "AnswerMessage";
    public static final String ANSWER_CONTENT_TAG = "AnswerContent";

    /* Errors */
    public static int UNKNOWN_ERROR_CODE = -1;
    public static int INVALID_ANSWER_ERROR_CODE = 0;

    public static int NO_ERRORS_CODE = 100;

    public static void sendRequest(Context Context, ScriptRequest ScriptRequest) {
        // Add the request to the RequestQueue.
        Volley.newRequestQueue(Context).add(createStringRequest(ScriptRequest));
    }

    public static void sendRequests(Context Context, OnRequestsFinishListener OnRequestsFinishListener, ScriptRequest... ScriptsRequests) {
        //Check list validate
        if(ScriptsRequests == null || ScriptsRequests.length <= 0) {
            throw new RuntimeException("VolleyUtiles -> sendRequests: ScriptsRequests is empty or null");
        }

        //Create requests queue
        RequestQueue requestQueue = Volley.newRequestQueue(Context);
        List<Request> requestList = new ArrayList<>(ScriptsRequests.length);

        requestQueue.addRequestFinishedListener(request -> {
            requestList.remove(request);

            //Check if no more requests
            if(requestList.size() == 0 && OnRequestsFinishListener != null) {
                OnRequestsFinishListener.onRequestsFinish();
            }
        });

        for (ScriptRequest scriptRequest : ScriptsRequests) {
            StringRequest stringRequest = createStringRequest(scriptRequest);

            requestQueue.add(stringRequest);
            requestList.add(stringRequest);
        }
    }

    private static StringRequest createStringRequest(ScriptRequest ScriptRequest) {
        //Check if script is valid
        if (!ScriptRequest.isParamsValid()) {
            throw new RuntimeException("ScriptRequest (" + ScriptRequest.getClass().getName() + ") missing params: " + ScriptRequest.getParamsMap());
        }

        // Request a string response from the provided URL with params
        return new StringRequest(
                Request.Method.POST,
                ScriptRequest.getURL(),
                answerString -> {
                    // Parse answer validate
                    JSONObject jsonObject = JSONUtiles.createFrom(answerString);
                    if(jsonObject == null) {
                        if(ScriptRequest.getOnFaildListener() != null) ScriptRequest.getOnFaildListener().onFaild(INVALID_ANSWER_ERROR_CODE, "Can't create jsonobject form request answer: " + answerString, ScriptRequest);

                        return;
                    }

                    // Get answer params
                    Integer answerCode = JSONUtiles.getInt(jsonObject, ANSWER_CODE_TAG);
                    String answerMessage = JSONUtiles.getString(jsonObject, ANSWER_MESSAGE_TAG);

                    if(answerCode == null || answerMessage == null) {
                        if(ScriptRequest.getOnFaildListener() != null) ScriptRequest.getOnFaildListener().onFaild(INVALID_ANSWER_ERROR_CODE, "Answer code or message missing form: " + answerString, ScriptRequest);

                        return;
                    }

                    // Check if answer has errors
                    if(answerCode != NO_ERRORS_CODE) {
                        if(ScriptRequest.getOnFaildListener() != null) ScriptRequest.getOnFaildListener().onFaild(answerCode, answerMessage, ScriptRequest);

                        return;
                    }

                    // Return answer content
                    if(ScriptRequest.getOnSuccessListener() != null) ScriptRequest.getOnSuccessListener().onSuccess(JSONUtiles.getJSONObject(jsonObject, ANSWER_CONTENT_TAG), ScriptRequest);
                },

                volleyError -> { if(ScriptRequest.getOnFaildListener() != null) ScriptRequest.getOnFaildListener().onFaild(UNKNOWN_ERROR_CODE, volleyError.getMessage(), ScriptRequest); })
        {
            @Override
            protected Map<String, String> getParams() {
                return ScriptRequest.getParamsMap();
            }
        };
    }

    public interface OnRequestsFinishListener {
        void onRequestsFinish();
    }

}
